<h1 align="center">三方组件资源汇总</h1> 

##### 本文收集了一些已经开源的三方组件资源，欢迎应用开发者参考和使用，同时也欢迎开发者贡献自己的开源组件库，可以提PR加入到列表当中


## 目录

- [工具](#工具)
- [三方组件JAVA](#三方组件JAVA)
    - [工具类](#工具类JAVA)
        - [图片加载](#图片加载JAVA)
        - [数据封装传递](#数据封装传递JAVA)
        - [日志](#日志JAVA)
        - [权限相关](#权限相关JAVA)
        - [相机-相册](#相机-相册JAVA)
        - [其他工具类](#其他工具类JAVA)
    - [网络类](#网络类-JAVA)
        - [网络类](#网络类JAVA)
    - [文件数据类](#文件数据类JAVA)
        - [数据库](#数据库JAVA)
        - [Preference](#preference-JAVA)
        - [数据存储](#数据存储JAVA)
    - [UI-自定义控件](#ui-自定义控件JAVA)
        - [Image](#image-JAVA)
        - [Text](#text-JAVA)
        - [Button](#button-JAVA)
        - [ListContainer](#listcontainer-JAVA)
        - [PageSlider](#pageslider-JAVA)
        - [ProgressBar](#progressbar-JAVA)
        - [Dialog](#dialog-JAVA)
        - [Layout](#layout-JAVA)
        - [Tab-菜单切换](#tab-菜单切换JAVA)
        - [Toast](#toast-JAVA)
        - [Time-Date](#time-date-JAVA)
        - [其他UI-自定义控件](#其他ui-自定义控件JAVA)
    - [框架类](#框架类-JAVA)
        - [框架类](#框架类JAVA)
    - [动画图形类](#动画图形类JAVA)
        - [动画](#动画JAVA)
        - [图片处理](#图片处理JAVA)
    - [音视频](#音视频JAVA)
    - [游戏](#游戏JAVA)
- [学习材料](#学习材料)
    - [组件介绍](#组件介绍)
    - [技术分享](#技术分享)
    - [教学专栏](#教学专栏)

## 工具

- [IDE官方下载地址](https://developer.harmonyos.com/cn/develop/deveco-studio) - DevEco Studio
- [HAPM官网地址](https://hpm.harmonyos.com/hapm/#/cn/home) - [HAPM介绍](https://hpm.harmonyos.com/hapm/#/cn/help/introduction)

[返回目录](#目录)

## <a name="三方组件JAVA"></a>三方组件JAVA

### <a name="工具类JAVA"></a>工具类

#### <a name="图片加载JAVA"></a>图片加载

- [glide](https://gitee.com/harmonyos-tpc/glide) - 最常用的图片加载工具
- [glide-transformations](https://gitee.com/harmonyos-tpc/glide-transformations) - 基于glide 的图片变化库
- [fresco](https://gitee.com/harmonyos-tpc/fresco) - facebook出品的一款图片加载工具
- [picasso](https://gitee.com/harmonyos-tpc/picasso) - 常用的图片加载工具之一
- [ohos-gif-drawable](https://gitee.com/harmonyos-tpc/ohos-gif-drawable) - gif图片加载工具
- [Keyframes](https://gitee.com/harmonyos-tpc/Keyframes) - gif图片加载工具
- [ion](https://gitee.com/harmonyos-tpc/ion) - 图片加载工具
- [coil](https://gitee.com/baijuncheng-open-source/coil) - 一款用于图片加载的库
- [ohos-smart-image-view](https://gitee.com/chinasoft_ohos/ohos-smart-image-view) - ohos-smart-image-view是一个从URL或用户的联系地址簿中加载图像。图像被高速缓存到内存和磁盘，以实现超快速加载
- [Cube-ImageLoader](https://gitee.com/hihopeorg/Cube-ImageLoader) - 这个框架致力于快速实现图片加载需求，解放生产力
- [Broccoli](https://gitee.com/hihopeorg/Broccoli) - 提供预加载占位效果
- [ImageBrowse](https://gitee.com/hihopeorg/ImageBrowse) - 图片预览组件
- [fresco-helper](https://gitee.com/chinasoft5_ohos/fresco-helper) - 让使用Fresco就像使用Glide、Picasso一样简洁的工具类库
- [sketch](https://gitee.com/chinasoft5_ohos/sketch) - 一款强大且全面的图片加载器
- [FrescoImageView](https://github.com/applibgroup/FrescoImageView) - FrescoImageView is a image loading library in HarmonyOS,which is similar to facebook's Fresco library that supports all methods and properties.

[返回目录](#目录)

#### <a name="数据封装传递JAVA"></a>数据封装传递

- [EventBus](https://gitee.com/harmonyos-tpc/EventBus) - 最常用的消息传递工具，发布/订阅事件总线
- [Rxohos](https://gitee.com/harmonyos-tpc/Rxohos) - RxJava3的openharmony特定绑定的反应性扩展。该模块向RxJava添加了最小的类，这些类使在openharmony应用程序中编写反应式组件变得容易且轻松。更具体地说，它提供了一个可在主线程或任何给定EventRunner上进行调度的Scheduler
- [RxBus](https://gitee.com/harmonyos-tpc/RxBus) - 基于Rxjava消息传递工具
- [otto](https://gitee.com/harmonyos-tpc/otto) - 基于Guava的消息传递工具
- [RxLifeCycle](https://gitee.com/harmonyos-tpc/RxLifeCycle) - 基于RxJava生命周期获取，此功能很有用，因为不完整的订阅可能会导致内存泄漏
- [RxBinding](https://gitee.com/harmonyos-tpc/RxBinding) - 以rxjava的形式来处理ohos中的ui事件
- [agera](https://gitee.com/harmonyos-tpc/agera) - Agera 是一组类和接口，用于帮助编写功能性、异步和反应式应用程序
- [Anadea_RxBus](https://gitee.com/harmonyos-tpc/Anadea_RxBus) - 支持注解和动态绑定的事件总线框架
- [LoadSir](https://gitee.com/harmonyos-tpc/LoadSir) - 注册事件进行回调操作
- [Aria](https://gitee.com/hihopeorg/Aria) - 文件下载上传框架
- [mobius](https://gitee.com/chinasoft5_ohos/mobius) - 是一个用于管理状态演变的函数式反应框架
- [LiveEventBus](https://gitee.com/hihopeorg/LiveEventBus) - 是一款消息总线，基于LiveData，具有生命周期感知能力，支持Sticky，支持跨进程，支持跨APP
- [ThirtyInch](https://gitee.com/archermind-ti/ThirtyInch) - 该库提供了为Ability和Fraction提供了Presenters, View仅仅发送用户事件， 接受来自Presenter的数据， 从不主动请求数据。这使得测试变得非常容易， 因为在View中没有逻辑代码。
- [EmailIntentBuilder](https://gitee.com/chinasoft4_ohos/EmailIntentBuilder) - 调用系统电子邮件应用发送邮件
- [OnAbilityResult](https://gitee.com/chinasoft4_ohos/OnAbilityResult) - 使用注解为Ability回调方法onAbilityResult()生成样板代码

[返回目录](#目录)

#### <a name="日志JAVA"></a>日志

- [Logger](https://gitee.com/harmonyos-tpc/logger) - log工具，简单，漂亮，功能强大的记录器
- [xLog](https://gitee.com/harmonyos-tpc/xLog) - 日志工具，可同时在多个通道打印日志，如 hilog、Console 和文件。如果你愿意，甚至可以打印到远程服务器（或其他任何地方）
- [KLog](https://gitee.com/chinasoft_ohos/KLog) - HiLog 工具类
- [tinylog](https://gitee.com/archermind-ti/tinylog) - 日志工具
- [Timber_ohos](https://gitee.com/isrc_ohos/timber_ohos) - 基于开源项目Timber进行鸿蒙化的移植和开发，增强鸿蒙输出日志的能力
- [LogUtils](https://gitee.com/chinasoft_ohos/LogUtils) - 日志管理器
- [hyperlog-ohos](https://gitee.com/openneusoft/hyperlog-ohos) - 日志记录工具，并将日志记录在数据库中
- [amuyuLogger](https://gitee.com/hihopeorg/amuyuLogger) - 日志打印库
- [LoggingInterceptor](https://gitee.com/hihopeorg/LoggingInterceptor) - OkHttp拦截器，它有漂亮的请求和响应日志程序
- [ohos-logback](https://gitee.com/hihopeorg/ohos-logback) - 控制台，文件，数据库输出日志
- [LogcatViewer](https://gitee.com/hihopeorg/LogcatViewer) - 获取本机日志，展示出来
- [Tinylog_ohos](https://gitee.com/archermind-ti/tinylog) - 简化日志打印的日志打印库
- [Log4a](https://gitee.com/archermind-ti/log4a) - Log4a 是一个基于 mmap, 高性能、高可用的日志收集框架



[返回目录](#目录)

#### <a name="权限相关JAVA"></a>权限相关

- [XXPermissions](https://gitee.com/harmonyos-tpc/XXPermissions) - 权限申请，一键式权限请求框架
- [PermissionsDispatcher](https://gitee.com/harmonyos-tpc/PermissionsDispatcher)	 - 权限申请，提供了一个简单的基于注解的API来处理运行时权限。该库减轻了编写一堆检查语句（无论是否已授予您权限）带来的负担，以保持您的代码干净安全
- [Dexter](https://gitee.com/harmonyos-tpc/Dexter) - 权限申请，简化在运行时请求权限的过程
- [RuntimePermission](https://gitee.com/archermind-ti/RuntimePermission) - 请求运行时权限的最简单方法，不需要扩展类或重写permissionResult方法
- [permission-helper](https://gitee.com/baijuncheng-open-source/permission-helper) - 权限管理请求库
- [HiPermission](https://gitee.com/chinasoft_ohos/HiPermission) - 一个简单易用的漂亮权限申请库
- [easypermissions](https://gitee.com/chinasoft_ohos/easypermissions) - 动态权限申请
- [EffortlessPermissions](https://gitee.com/archermind-ti/effortless-permissions) - 一个 OpenHarmony 权限库，通过方便的添加扩展了 OpenHarmony 权限
- [RuntimePermission](https://gitee.com/archermind-ti/RuntimePermission) - runtimePermission具有进行权限申请功能，可以对需要的不同权限进行申请
- [soul-permission](https://gitee.com/openneusoft/soul-permission) - 相关权限的存在校验及权限设定提示
- [RuntimePermission](https://gitee.com/harmonyos-tpc/RuntimePermission) - 支持同时申请多个权限
- [HeiPermission](https://gitee.com/archermind-ti/hei-permission) - 动态权限申请库
- [PermissionGrantor](https://gitee.com/archermind-ti/permission-grantor) - 一行代码搞定ohos动态权限授权、权限管理
- [ MPermissions](https://gitee.com/ts_ohos/mpermissions) -OHOS运行时权限的库
- [TedPermission](https://gitee.com/hihopeorg/TedPermission) - TedPermission 可以轻松检查和请求权限
- [AndPermission](https://gitee.com/hihopeorg/AndPermission) -  请求运行时权限
- [PermissionUtils](https://gitee.com/chinasoft4_ohos/PermissionUtils) - 轻松请求权限,链式调用,简介明了
- [ScrollGalleryView](https://gitee.com/chinasoft2_ohos/ScrollGalleryView) - 是一个灵活的库，可帮助您在应用程序中创建很棒的媒体库
- [RxImagePicker](https://gitee.com/chinasoft5_ohos/RxImagePicker) - 相册图片选择
- [Zoomy](https://gitee.com/chinasoft4_ohos/Zoomy) - 一个使用方便的捏合缩放库

[返回目录](#目录)

#### <a name="相机-相册JAVA"></a>相机-相册

- [BGAQRCode-ohos](https://gitee.com/harmonyos-tpc/BGAQRCode-ohos) - 基于ZXing的二维码扫描工具
- [Matisse](https://gitee.com/harmonyos-tpc/Matisse) - 选择图库图片
- [ImagePicker](https://gitee.com/harmonyos-tpc/ImagePicker) - 相册访问
- [CameraView](https://gitee.com/harmonyos-tpc/CameraView) - 相机使用组件
- [easyqrlibrary](https://gitee.com/archermind-ti/easyqrlibrary) - 二维码扫描器
- [zxing-embedded](https://gitee.com/baijuncheng-open-source/zxing-embedded) - 基于ZXING，二维码条形码扫描库
- [qrcode-reader-view](https://gitee.com/baijuncheng-open-source/qrcode-reader-view) - 一个简易的相机扫码工具
- [barcodescanner](https://gitee.com/baijuncheng-open-source/barcodescanner) - 基于zxing和zbar提供易于使用的二维码扫描功能
- [certificate-camera](https://gitee.com/baijuncheng-open-source/certificate-camera) - 一个拍摄证件照片的相机工具。
- [Zbar_ohos](https://gitee.com/isrc_ohos/ZBar_ohos) - 基于开源项目Zbar进行鸿蒙化的移植和开发，条形码阅读
- [ImageSelector](https://gitee.com/chinasoft2_ohos/ImageSelector) - 一个功能强大的图片选择器
- [zBarLibary](https://gitee.com/chinasoft2_ohos/zBarLibary) - zxing二维码生成、识别
- [QRCodeScanner](https://gitee.com/chinasoft_ohos/QRCodeScanner) - 二维码扫描器
- [CameraFragment](https://gitee.com/chinasoft_ohos/CameraFragment) - 一个简单的易于集成的相机Fragment
- [PixImagePicker](https://gitee.com/chinasoft3_ohos/PixImagePicker) - PixImagePicker是一个拍照、录像，照片和视频选择功能库
- [ContentManager](https://gitee.com/archermind-ti/content-manager) - 本库用于从图库、相机等设备获取图片、视频
- [MagicalCamera](https://gitee.com/archermind-ti/magical-camera) - 在 OpenHarmony 中拍照和选择图片的魔法库。 方法很简单，如果需要也可以把图片保存在设备中，获取真实的uri路径或者图片或者获取图片的私密信息
- [imagepicker](https://gitee.com/openneusoft/imagepicker) - 设备上获取照片（从相册、文件中选择）、压缩图片的开源工具库
- [code-scanner](https://gitee.com/openneusoft/code-scanner) - 实现了相机的一些功能和条形码扫描的功能
- [LongImageCamera](https://gitee.com/openneusoft/long-image-camera) - 通过相机视图捕获多图像进行合并拼接，形成长图像并进行预览或手势操作
- [titan-camera](https://gitee.com/openneusoft/titan-camera) - 实现一个应用内置相机，可以处理预览大小，预览拉伸
- [Louvre](https://gitee.com/harmonyos-tpc/Louvre) - 相册访问组件
- [flow-camera](https://gitee.com/ts_ohos/flow-camera) - 照相视频拍摄保存
- [IDCardCamera](https://gitee.com/hihopeorg/IDCardCamera) - 自定义身份证相机功能。
- [CameraModule](https://gitee.com/hihopeorg/CameraModule) - 简单相机模块。
- [Laevatein](https://gitee.com/hihopeorg/Laevatein) - 照片选择。
- [ SquareCamera](https://gitee.com/ts_ohos/SquareCamera) -方形拍照库
- [ohos-image-picker](https://gitee.com/chinasoft5_ohos/ohos-image-picker) - 图片选择器图片来源相册和相机拍摄
- [Ohos-ImagesPickers](https://gitee.com/chinasoft5_ohos/Ohos-ImagesPickers) - 图片选择（单选/多选）、拍照、裁剪、图片预览、图片显示容器的图片选择显示工具
- [ImageSelector](https://gitee.com/hihopeorg/ImageSelector) - 是给应用提供简化读取相册，照片单选和多选，以及照片裁剪等功能
- [EasyPhotos](https://gitee.com/hihopeorg/EasyPhotos) - 相机拍照，相册选择(单选/多选)，文件夹图片选择(单选/多选)，视频选择，视频图片多类型复杂选择，图片添加水印，拼图功能

[返回目录](#目录)

#### <a name="其他工具类JAVA"></a>其他工具类

- [Butterknife](https://gitee.com/harmonyos-tpc/butterknife) - 通过反射调用方法，使用注解处理为您生成样板代码
- [assertj-ohos](https://gitee.com/harmonyos-tpc/assertj-ohos) - 一组旨在测试ohos的断言库
- [ohos-utilset](https://gitee.com/harmonyos-tpc/ohos-utilset) - 工具集
- [xUtils3](https://gitee.com/harmonyos-tpc/xUtils3) - 包含了orm,http（s）,image, Component注解的工具集合，特性强大，方便拓展
- [device-year-class](https://gitee.com/harmonyos-tpc/device-year-class) - 获取手机年份
- [swipe](https://gitee.com/harmonyos-tpc/swipe) - 对于手势封装应用
- [TinyPinyin](https://gitee.com/harmonyos-tpc/TinyPinyin) - 低内存占用的汉字转拼音工具库
- [ohos-bluetooth-kit](https://gitee.com/hihopeorg/ohos-bluetooth-kit) - 蓝牙设备通信
- [ohos-IMSI-Catcher-Detector](https://gitee.com/hihopeorg/ohos-IMSI-Catcher-Detector) - IMSI探测器
- [Battery_Metrics](https://gitee.com/hihopeorg/Battery-Metrics) - 检测电池相关系统指标的库
- [CheckVersionLib](https://gitee.com/hihopeorg/CheckVersionLib) - 版本检测更新库
- [ErrorProne](https://gitee.com/hihopeorg/error-prone) - 将常见的Java语法错误捕获为编译错误显示出来
- [FastBle](https://gitee.com/hihopeorg/FastBle) - 蓝牙设备通信
- [RxOhosBle](https://gitee.com/hihopeorg/rxohosble) - 蓝牙设备通信
- [ohos-BLE](https://gitee.com/hihopeorg/ohos-BLE) - 蓝牙框架,提供了扫描、连接、使能/除能通知、发送/读取数据、接收数据,读取rssi,
- [RxTool](https://gitee.com/baijuncheng-open-source/RxTool) - 工具类合集
- [truth](https://gitee.com/hihopeorg/truth) - 代码断言工具
- [KeyboardVisibilityEvent](https://gitee.com/hihopeorg/KeyboardVisibilityEvent) - 键盘显示隐藏监听工具
- [StatusBarUtil](https://gitee.com/hihopeorg/StatusBarUtil) - 状态栏管理工具
- [Router](https://gitee.com/chinasoft_ohos/Router) - 通过一行url去指定打开指定页面Ability的工具
- [Once](https://gitee.com/chinasoft_ohos/Once) - 提供一个简单的API来跟踪应用程序是否已经在给定的范围内执行了操作
- [libphonenumber-ohos](https://gitee.com/chinasoft_ohos/libphonenumber-ohos) - 电话归属地查询
- [ohos-gesture-detectors](https://gitee.com/chinasoft_ohos/ohos-gesture-detectors) - 实现各种手势检测功能
- [Commonmark-java](https://gitee.com/chinasoft_ohos/Commonmark-java) - 自定义表扩展名
- [LocationManager](https://gitee.com/chinasoft_ohos/LocationManager) - 简化用户位置的获取
- [phrase](https://gitee.com/chinasoft_ohos/phrase) - 字符串处理工具
- [JsonLube](https://gitee.com/chinasoft_ohos/JsonLube) - Json高效解析工具
- [Notify-ohos](https://gitee.com/chinasoft_ohos/Notify-ohos) - 一个统一通知管理的功能库
- [objenesis_ohos](https://gitee.com/archermind-ti/objenesis_ohos) - Objenesis是一个轻量级的Java库，作用是绕过构造器创建一个实例
- [update-checker-lib](https://gitee.com/baijuncheng-open-source/update-checker-lib) - 目前仅酷安网的更新检查检查
- [Parceler_ohos](https://gitee.com/isrc_ohos/parceler_ohos) - 序列化与反序列化封装实现
- [JodaTime_ohos](https://gitee.com/isrc_ohos/joda-time_ohos) - 日期和时间处理库
- [ANR-WatchDog-ohos](https://gitee.com/isrc_ohos/anr-watch-dog-ohos) - 检测ANR错误并引发有意义的异常工具
- [ViewServer_ohos](https://gitee.com/isrc_ohos/view-server_ohos) - 可视化界面显示布局调试支持工具
- [libyuv](https://gitee.com/harmonyos-tpc/libyuv) - 将ARGB图像转换为RGBA
- [ReLinker](https://gitee.com/harmonyos-tpc/ReLinker) - native库加载器
- [FastBle](https://gitee.com/harmonyos-tpc/FastBle) - 蓝牙快速开发框架
- [LoganSquare](https://gitee.com/harmonyos-tpc/LoganSquare) - JSON解析和序列化库
- [CustomActivityOnCrash](https://gitee.com/harmonyos-tpc/CustomActivityOnCrash) - 崩溃时启动自定义页面
- [RxScreenshotDetector](https://gitee.com/harmonyos-tpc/RxScreenshotDetector) - 截屏检测器
- [seismic](https://gitee.com/harmonyos-tpc/seismic) - 设备抖动检测
- [AutoDispose](https://gitee.com/harmonyos-tpc/AutoDispose) - RxJava工具库
- [webp-ohos](https://gitee.com/harmonyos-tpc/webp-ohos) - 节省内存空间的图片形式
- [Encryption](https://gitee.com/archermind-ti/Encryption) - 字符串加密解密工具
- [Ohos-Intent-Library](https://gitee.com/archermind-ti/Ohos-Intent-Library) - Intent跳转封装库
- [Armadillo](https://gitee.com/archermind-ti/armadillo) - 加密Preferences数据
- [java-aes-crypto](https://gitee.com/chinasoft_ohos/java-aes-crypto) - 用于简单加密解密的类
- [TrustKit-ohos](https://gitee.com/chinasoft_ohos/TrustKit-ohos) - 提供在任何Ohos应用程序中轻松部署ssl公钥锁定和报告功能的库
- [ohos-weak-handler](https://gitee.com/chinasoft_ohos/ohos-weak-handler) - 弱引用内存安全的 Handler
- [EasyProtector](https://gitee.com/chinasoft_ohos/EasyProtector) - ohos上提供的安全功能： 1、安全防护 2、检查root 3、检查Xposed 4、反调试 5、应用多开 6、模拟器检测
- [easydeviceinfo](https://gitee.com/chinasoft_ohos/easydeviceinfo) - 方便的获取手机设备的各种数据信息的库
- [countly-sdk-ohos](https://gitee.com/chinasoft2_ohos/countly-sdk-ohos) - 行为日志收集和性能分析
- [duktape-ohos](https://gitee.com/chinasoft_ohos/duktape-ohos) - 用于Duktape嵌入式JavaScript引擎
- [shortbread](https://gitee.com/chinasoft_ohos/shortbread) - 一个通过注解快捷创建shortcut的工具库
- [ohos-multipicker-library](https://gitee.com/chinasoft_ohos/ohos-multipicker-library) - 文件选择工具
- [OhosScreenAdaptation](https://gitee.com/chinasoft_ohos/OhosScreenAdaptation) - 屏幕分辨率适配
- [NettyChat](https://gitee.com/chinasoft_ohos/NettyChat) - 即时聊天功能
- [ActivityRouter](https://gitee.com/chinasoft_ohos/ActivityRouter) - 支持给Ability定义 URL，这样可以通过 URL 跳转到Ability，支持在浏览器以及 app 中跳入
- [matomo-sdk-ohos](https://gitee.com/chinasoft_ohos/matomo-sdk-ohos) - 可以解析每个按钮的点击事件所上报的数据
- [merlin](https://gitee.com/chinasoft_ohos/merlin) - 手机，wifi网络状态监听
- [Recovery](https://gitee.com/chinasoft2_ohos/Recovery) - 捕获应用崩溃框架，并能恢复崩溃页面
- [SensorManager](https://gitee.com/chinasoft2_ohos/SensorManager) - 这个一个关于传感器相关的功能用法，里面包含多种传感器的使用方法以及测试Demo
- [okble](https://gitee.com/chinasoft3_ohos/okble) - 简单易用的BLE library
- [okbinder](https://gitee.com/chinasoft3_ohos/okbinder) - 一个轻量级的跨进程通信方案，可以用来替代 AIDL
- [FileTransfer](https://gitee.com/chinasoft3_ohos/FileTransfer) - FileTransfer web端与app端文件传输
- [Ohos-Scanner-Compat-Library](https://gitee.com/chinasoft2_ohos/Ohos-Scanner-Compat-Library) - 蓝牙的操作库
- [version-compare](https://gitee.com/chinasoft_ohos/version-compare) - 软件版本号比较工具
- [MagicaSakura](https://gitee.com/chinasoft3_ohos/MagicaSakura) - MagicaSakura是一个openharmony多主题库，支持每日色彩主题和夜间主题
- [sensey](https://gitee.com/chinasoft_ohos/sensey) - 传感器封装
- [Bluetooth-LE-Library---ohos](https://gitee.com/chinasoft3_ohos/Bluetooth-LE-Library---ohos) - 该库可轻松访问Bluetooth LE设备的AdRecord和RSSI值。它为iBeacons提供了其他功能。差异点因为openharmony目前暂不支持系统分享原因，通过intent分享功能没有实现
- [GlideBitmapPool](https://gitee.com/chinasoft2_ohos/GlideBitmapPool) - 用于重用位图内存的内存管理库
- [PickiT](https://gitee.com/chinasoft3_ohos/PickiT) - 该库可通过文件的Uri获取到文件的path功能
- [ohos-visualizer](https://gitee.com/chinasoft3_ohos/ohos-visualizer) - 一个显示频谱的控件
- [ChinaMapView](https://gitee.com/chinasoft3_ohos/ChinaMapView) - 实现通过绘制map的方式进行统计，通过着色器来修改地图上各个组件颜色的操作(由于鸿蒙不支持事件分发机制不完善，导致滑动事件冲突未实现)
- [colorpicker](https://gitee.com/chinasoft2_ohos/colorpicker) - 一套新颖好用的颜色选择器，可以通过弹出框的形式显示，可以随意选择颜色并且生成对应的颜色值，自定义圆形按钮，通过选择颜色改变按钮显示效果，多界面显示，可以收拾滑动，显示多个颜色选择器在不同界面
- [librtmp](https://gitee.com/archermind-ti/librtmp) - Librtmp是用于RTMP流的工具包。 支持所有形式的RTMP，包括rtmp://，rtmpt://，rtmpe://，rtmpte://和rtmps://
- [hwcpipe](https://gitee.com/archermind-ti/hwcpipe) - HWCPipe是一个arm平台获取CPU和GPU硬件计数器的项目
- [xCrash](https://gitee.com/archermind-ti/xcrash) - xCrash为 app 提供捕获 java 崩溃，native 崩溃和 ANR 的能力。不需要 root 权限或任何系统权限
- [bugshaker](https://gitee.com/archermind-ti/bugshaker) - BugShaker允许你的QA团队和/或最终用户通过晃动他们的设备来轻松提交bug报告
- [AppUpdate](https://gitee.com/archermind-ti/appupdate) - 一个简单、轻量、可随意定制的OpenHarmony版本更新库
- [LifecycleModel](https://gitee.com/archermind-ti/lifecycle-model) - LifecycleModel 实现了 Fraction 与 Fraction 之间, Ability 与 Fraction 之间的通讯以及共享数据
- [Share2](https://gitee.com/archermind-ti/share2) - Share2 利用了 OpenHarmony 的原生 API 实现了分享功能，支持文本信息、图片、音视频等其他类型文件的分享
- [DroidAssist](https://gitee.com/archermind-ti/DroidAssist) - `DroidAssist` 是一个轻量级的字节码编辑插件，基于 `Javassist` 对字节码操作，根据 xml 配置处理 class 文件，以达到对 class 文件进行动态修改的效果
- [TaskManager](https://gitee.com/archermind-ti/taskmanager) - TaskManager任务管理器
- [NcAppFeedback](https://gitee.com/archermind-ti/NcAppFeedback) - 让用户使用电话电子邮件客户端或匿名使用 SparkPost 电子邮件服务进行反馈
- [stunning-signature](https://gitee.com/openneusoft/stunning-signature) - 防止篡改APK文件的签名库
- [markdown](https://gitee.com/openneusoft/markdown) - 读取Markdown文件,将Markdown格式转换为Html格式
- [AndLinker](https://gitee.com/openneusoft/and-linker) - AndLinker是 IPC (进程间通信) 库，结合了AIDL和Retrofit的诸多特性，且可以与RxJava和RxJava2的Call Adapters无缝结合使用
- [HarmonyOSRate](https://gitee.com/baijuncheng-open-source/harmony-osrate) - 应用评分
- [StatusBarUtil](https://gitee.com/baijuncheng-open-source/statusbarutil) - 状态栏工具类
- [Animewallpaper](https://gitee.com/baijuncheng-open-source/animewallpaper) - 高清动画壁纸
- [FishBun](https://gitee.com/ts_ohos/FishBun) - 主要涉及功能为读取手机图片，并进行选择操作。以及部分关于开发相关的配置。
- [ohosFilePicker](https://gitee.com/ts_ohos/file-picker) - 文件选择器
- [DevUtils](https://gitee.com/ts_ohos/dev_utils) - 封装快捷使用的工具类及 API 方法调用 该项目尽可能的便于开发人员，快捷、高效开发安全可靠的项目。
- [ohos-fest](https://gitee.com/hihopeorg/ohos-fest) - 常见容器、控件、方法类封装对应断言方法，并支持扩展。
- [ohos-Common](https://gitee.com/hihopeorg/ohos-Common) - 该组件为工具类组件，主要包含常用工具类以及下拉刷新等控件
- [PercentSmoothHandler](https://gitee.com/harmonyos-tpc/PercentSmoothHandler) - 自定义Handler
- [Pdfiumohos](https://gitee.com/harmonyos-tpc/Pdfiumohos) - pdf浏览控件
- [HoverTouchView](https://gitee.com/harmonyos-tpc/HoverTouchView) - 图片长按放大浏览工具
- [foregroundappchecker](https://gitee.com/harmonyos-tpc/foregroundappchecker) - 当前运行app包名获取工具
- [RxBackoff](https://gitee.com/harmonyos-tpc/RxBackoff) - 基于rxjava实现捕获异常进行重试操作
- [EasyFonts](https://gitee.com/harmonyos-tpc/EasyFonts) - 导入直接使用的字体库
- [MobileInfo](https://gitee.com/harmonyos-tpc/MobileInfo) - 获取一些手机常规信息
- [ohos-utils](https://gitee.com/hihopeorg/ohos-utils) -  提供部分应用开发过程当中常用的工具类
- [CrawlerForReader](https://gitee.com/hihopeorg/CrawlerForReader) -  一个本地网络小说爬虫，基于jsoup与xpath，通过模版解析网页。
- [Utils-Everywhere](https://gitee.com/hihopeorg/Utils-Everywhere) -  各种工具类集合。
- [AnotherMonitor](https://gitee.com/hihopeorg/AnotherMonitor) -  监控ohos设备的内存使用情况
- [CrashReporter](https://gitee.com/hihopeorg/CrashReporter) -  异常捕获工具
- [PhoneNumber](https://gitee.com/hihopeorg/PhoneNumber) - 一个获取号码归属地和其他信息（诈骗、骚扰等）的开源库。
- [ohos-db-commons](https://gitee.com/hihopeorg/ohos-db-commons) - 用于DataAbilityHelper的工具类。
- [ohosProcess](https://gitee.com/hihopeorg/ohosProcess) - 判断应用处于前后台。
- [ColorPicker](https://gitee.com/archermind-ti/colorpicker) - 颜色选择器，可以通过弹出框的形式显示
- [HideKeyboard](https://gitee.com/archermind-ti/hide-keyboard-ohos) - 仿iOS输入法，点击输入框以外区域自动隐藏软键盘的轻量级库
- [OHOSSlideBack](https://gitee.com/archermind-ti/ohos-slide-back) - 给页面加入侧滑返回功能 (类似“小米MIX”和新版“即刻”滑动返回)
- [ShadowHelper](https://gitee.com/archermind-ti/shadowhelper) - 一个可以很简单的给ohos component添加自然的阴影的库
- [Dynamic-load-view](https://gitee.com/archermind-ti/dynamic-load-view) - Dynamic-load-view能够动态加载外部hap中的Component，能够热修复线上Component，以及模块化更新
- [BLE](https://gitee.com/archermind-ti/ble) - 一个为了简化蓝牙设备接入的流程的基础操作框架，基于回调，操作简单。包含扫描、广播包解析、服务读写等功能
- [adapter-kit](https://gitee.com/chinasoft4_ohos/adapter-kit) - 一组有用的适配器套件，该套件当前包括，即时适配器，简单部分适配器，循环列表适配器
- [Rajawali](https://gitee.com/archermind-ti/rajawali) - 3D引擎库
- [NotifyUtil](https://gitee.com/chinasoft4_ohos/NotifyUtil) - 高仿淘宝，网易新闻，微信，应用宝，环聊等等热门应用的通知视图，并且完通知工具类的封装，提供多达8种最常见的通知接口
- [EasyCountDownTextureView](https://gitee.com/chinasoft4_ohos/EasyCountDownTextureView) - 可以开始/停止，并且可以自定义时间的定时器
- [MathView](https://gitee.com/chinasoft4_ohos/MathView) - 使用Web的形式展示数学公式
- [DrawView](https://gitee.com/chinasoft4_ohos/DrawView) - 画板组件，允许用户创建绘图，从简单视图中绘制任何你喜欢的图案
- [auto-value-parcel](https://gitee.com/ts_ohos/auto-value-parcel) -自动值的生成工具，支持简单序列化处理
- [WhiteBoard](https://gitee.com/chinasoft4_ohos/WhiteBoard) -一个可涂鸦、绘图、添加文字、图像（可旋转缩放）、背景的Fragment
- [MultiThreadDownload](https://gitee.com/ts_ohos/multi-thread-download) -一个多功能的多线程下载库，支持开发者自定义下载，包括图片下载，视频下载，app下载
- [ohos-saripaar](https://gitee.com/hihopeorg/ohos-saripaar) - 表单内容合法性的验证,例如账号、密码、手机号、邮箱等验证合法性
- [Cockroach](https://gitee.com/hihopeorg/Cockroach) - 用户在使用应用的过程中，防止应用发生非必要的crash，降低方发生crash的概率
- [Slidr](https://gitee.com/hihopeorg/Slidr) - 滑动退出Ability页面
- [ohos-Bolts](https://gitee.com/hihopeorg/ohos-Bolts) - 使复杂的异步代码的组织更易于管理；提供了一种跨平台的机制，允许其他应用程序直接链接到针对其所运行的设备进行了优化
- [truetime-ohos](https://gitee.com/hihopeorg/ohos-truetime) - 同步NTP服务器时间,可以实时获取最新时间
- [jdeferred](https://gitee.com/hihopeorg/jdeferred) - 异步编程
- [pandora](https://gitee.com/hihopeorg/pandora) - 可直接在应用程序中检查和修改包括网络、数据库、UI 等在内的内容工具集
- [CustomAbilityOnCrash](https://gitee.com/hihopeorg/CustomActivityOnCrash) - 监听崩溃，自定义崩溃页面，设置重启的的目标页面，复制异常信息到剪贴板
- [status-bar-compat](https://gitee.com/hihopeorg/status-bar-compat) - 设置状态栏颜色或透明
- [ColorfulStatusBar](https://gitee.com/hihopeorg/colorfulstatusbar) - 实现设置状态栏颜色
- [Lens](https://gitee.com/hihopeorg/Lens) - 界面分析，对象监控, DataDump，数据存储更新等等功能
- [TinyTask](https://gitee.com/hihopeorg/TinyTask) - 一个创建异步后台任务
- [YCNotification](https://gitee.com/hihopeorg/YCNotification) - 通知封装库务
- [objenesis](https://gitee.com/archermind-ti/objenesis) - 一个专用于在创建对象时绕过构造函数的库
- [Cognitive-Face-OHOS](https://gitee.com/archermind-ti/Cognitive-Face-OHOS) - 演示微软人脸识别功能
- [ohos-TopScrollHelper](https://gitee.com/archermind-ti/topscrollsample) - AutoScroll工具类，点击或者双击顶部标题栏置顶列表
- [SkinSprite](https://gitee.com/archermind-ti/skinsprite) - SkinSprite是一种无需重新启动页面即可更改昼夜模式的一种解决方案
- [Rudeness](https://gitee.com/archermind-ti/rudeness) - 一种粗暴快速的全屏幕适配方案
- [OhosKeyboardWatcher](https://gitee.com/archermind-ti/OhosKeyboardWatcher) - 一个监听软键盘开启/关闭组件
- [ShakeDetector](https://gitee.com/chinasoft4_ohos/ShakeDetector) - 该库提供了一种简单的方法，可以使用内置的加速度计来检测震动，并在每次发生时在UI线程上触发回调
- [data-binding-validator](https://gitee.com/chinasoft3_ohos/data-binding-validator) - 基于数据绑定适配器的字段验证库
- [Ohos-Goldfinger](https://gitee.com/chinasoft5_ohos/Ohos-Goldfinger) - 简化身份人脸验证实现
- [chooseasy](https://gitee.com/chinasoft4_ohos/chooseasy) - 具有分组选择器的库，使选择变得容易
- [colorpicker](https://gitee.com/chinasoft4_ohos/colorpicker) - 实现设置颜色列表和选择颜色
- [hawk](https://gitee.com/chinasoft4_ohos/hawk) - 安全、简单的键值存储
- [decoro](https://gitee.com/chinasoft3_ohos/decoro) - 库设计的自动格式化文本输入自定义规则
- [declex](https://gitee.com/chinasoft5_ohos/declex) - declex是一个强大的框架，它减少了Java语言的冗长，增强了依赖/视图注入，从而允许以创纪录的速度创建Ohos应用程序，它完全基于ohosannotation
- [Keyboard](https://gitee.com/chinasoft4_ohos/Keyboard) - 仿京东，支付宝密码键盘和密码输入框
- [BoofCV](https://gitee.com/chinasoft3_ohos/BoofCV) - 一个开源实时计算机视觉库
- [Better-Link-Movement-Method](https://gitee.com/chinasoft4_ohos/Better-Link-Movement-Method) - 实现链接跳转系统应用
- [PolygonDrawingUtil](https://gitee.com/chinasoft3_ohos/PolygonDrawingUtil) - 多边形绘制工具类
- [secure-storage-ohos](https://gitee.com/chinasoft2_ohos/secure-storage-ohos) - 多边形绘制工具类
- [sentry-java](https://gitee.com/chinasoft5_ohos/sentry-java) - 一款用于统计用户行为、自定义事件、crash log上报到sentry服务端的SDK
- [bugsnag-ohos](https://gitee.com/chinasoft5_ohos/bugsnag-ohos) - 监视crash事件以及生成报告的工具,适用于ohos app
- [LicenseAdapter](https://gitee.com/chinasoft4_ohos/LicenseAdapter) - 一个易于使用的adapter库
- [SystemUiHelper](https://gitee.com/chinasoft4_ohos/SystemUiHelper) - ohos系统UI状态栏和标题栏显示/隐藏的动画效果

[返回目录](#目录)

### <a name="网络类-JAVA"></a>网络类

#### <a name="网络类JAVA"></a>网络类

- [PersistentCookieJar](https://gitee.com/harmonyos-tpc/PersistentCookieJar) - 基于okhttp3实现的cookie网络优化
- [chuck](https://gitee.com/harmonyos-tpc/chuck) - okhttp本地client
- [google-http-java-client](https://gitee.com/harmonyos-tpc/google-http-java-client) - google http Client库
- [ohos-async-http](https://gitee.com/harmonyos-tpc/ohos-async-http) - 基于Apache的HttpClient库构建的Http Client
- [okhttp-OkGo](https://gitee.com/harmonyos-tpc/okhttp-OkGo) - 基于okhttp 封装的库
- [ohosAsync](https://gitee.com/harmonyos-tpc/ohosAsync) - 异步网络请求
- [Fast-ohos-Networking](https://gitee.com/harmonyos-tpc/Fast-ohos-Networking) - 快速访问
- [FileDownloader](https://gitee.com/harmonyos-tpc/FileDownloader) - 文件下载库
- [PRDownloader](https://gitee.com/harmonyos-tpc/PRDownloader) - 文件下载库
- [ohosDownloader](https://gitee.com/openneusoft/ohosdownloader) - 一个面向ohos的开源多线程和多任务下载框架
- [network-connection-class](https://gitee.com/harmonyos-tpc/network-connection-class) - 获取网络状态库
- [ThinDownloadManager](https://gitee.com/harmonyos-tpc/ThinDownloadManager) - 文件下载库
- [AndServer](https://gitee.com/hihopeorg/AndServer) - 网络部署与反向代理设置
- [autobahn-java](https://gitee.com/hihopeorg/autobahn-java) - WebSocket协议和Web应用程序消息传递协
- [Smack](https://gitee.com/hihopeorg/Smack) - 用于与XMPP服务器进行通信，以执行实时通信，包括即时消息和群聊
- [RxEasyHttp](https://gitee.com/hihopeorg/RxEasyHttp) - 基于RxJava2+Retrofit2实现简单易用的网络请求框架
- [retrofit-cache](https://gitee.com/archermind-ti/retrofit-cache) - 通过注解配置，可以针对每一个接口灵活配置缓存策略
- [okdownload](https://gitee.com/harmonyos-tpc/okdownload) - 下载引擎
- [NoHttp](https://gitee.com/harmonyos-tpc/NoHttp) - 实现Http标准协议框架，支持多种缓存模式，底层可动态切换OkHttp,URLConnection
- [ReactiveNetwork](https://gitee.com/harmonyos-tpc/ReactiveNetwork) - 监听网络连接状态以及与RxJava Observables的Internet连接
- [okhttputils](https://gitee.com/harmonyos-tpc/okhttputils) - okhttp的封装辅助工具
- [okhttp](https://gitee.com/chinasoft_ohos/okhttp) - PUT，DELETE，POST，GET等请求、文件的上传下载、加载图片(内部会图片大小自动压缩)、支持请求回调，直接返回对象、对象集合、支持session的保持
- [BaseOkHttpV3](https://gitee.com/chinasoft_ohos/BaseOkHttpV3) - OkHttp的二次封装库，提供各种快速使用方法以及更为方便的扩展功能。提供更高效的Json请求和解析工具以及文件上传下载封装，HTTPS和Cookie操作也更得心应手
- [OhosNetworkTools](https://gitee.com/chinasoft_ohos/OhosNetworkTools) - 这是一个 networkTools网络工具类，端口扫描，子网设备查找（本地网络上发现设备）
- [safe-java-js-webview-bridge](https://gitee.com/chinasoft2_ohos/safe-java-js-webview-bridge) - 抛弃使用高风险的WebView addJavascriptInterface方法，通过对js层调用函数及回调函数的包装，支持异步回调，方法参数支持js所有已知的类型，包括number、string、boolean、object、function
- [ZWebView](https://gitee.com/chinasoft2_ohos/ZWebView) - 建立移动端和Web的JS桥接框架，实现通过容器WebView实现移动端与js的互调功能
- [HtmlBuilder](https://gitee.com/chinasoft2_ohos/html-builder) - html页面的加载
- [OkSocket](https://gitee.com/chinasoft2_ohos/OkSocket) - 是一款基于Tcp协议的Socket通讯（长连接）
- [StompProtocolOhos](https://gitee.com/chinasoft3_ohos/StompProtocolOhos) - 对STOMP协议支持长连接 收发消息
- [bizsocket](https://gitee.com/chinasoft2_ohos/bizsocket) - 断线重连、一对一请求、通知、粘性通知、串行请求合并、包分片处理(AbstractFragmentRequestQueue)、缓存、拦截器、支持rxjava，提供类似于retrofit的支持、提供rxjava和rxjava2两种使用方式
- [OkHttpFinal](https://gitee.com/hihopeorg/OkHttpFinal) - 一个对OkHttp封装的简单易用型HTTP请求和文件下载管理框架
- [ok2curl](https://gitee.com/openneusoft/ok2curl) - 将OkHttp请求转换为curl日志
- [RetrofitUrlManager](https://gitee.com/archermind-ti/retrofit-url-manager) - 以最简洁的 Api 让 Retrofit 同时支持多个 BaseUrl 以及动态改变 BaseUrl
- [TrebleShot_ohos](https://gitee.com/archermind-ti/treble-shot-ohos) - 通过可用连接，发送和接收文件
- [multi-thread-downloader](https://gitee.com/openneusoft/multi-thread-downloader) - 轻量级支持断点续传的多线程下载器
- [RxRetroJsoup](https://gitee.com/openneusoft/rx-retro-jsoup) - 响应式的请求网络框架
- [RxWebSocket](https://gitee.com/openneusoft/rx-web-socket) - 基于okhttp和RxJava封装的WebSocket客户端
- [Kalle](https://gitee.com/openneusoft/kalle) - HttpClient，遵循Http标准协议，支持同步请求和异步请求
- [ohos_lite_http](https://gitee.com/openneusoft/ohos_lite_http) - 只需一行代码就可以发出HTTP请求！它可以将java模型转换为参数，并智能地将响应JSON命名为java模型
- [ohos-upload-service](https://gitee.com/hihopeorg/ohos-upload-service) - 在带有进度通知的后台轻松上传文件。支持持久上传请求、自定义和自定义插件。
- [volley](https://gitee.com/harmonyos-tpc/volley) - 轻量级网络请求
- [okhttp-json-mock](https://gitee.com/hihopeorg/okhttp-json-mock) - 为Okhttp和Retrofit模拟json格式的数据，可以请求转发到本地json文件并返回其中存储的数据。
- [HappyDnsOhos](https://gitee.com/archermind-ti/happydnsohos) - 方便使用DNS的库
- [reark](https://gitee.com/chinasoft4_ohos/reark) - 请求网络数据显示页面中，可对数据进行缓存处理
- [DownloadHelper](https://gitee.com/hihopeorg/DownloadHelper) - 一个支持断点续传和多线程的下载帮助类，它可靠稳定，无其他依赖，体积很小并且接入方式简单
- [RetrofitUrlManager](https://gitee.com/archermind-ti/retrofit-url-manager) - 以最简洁的 Api 让 Retrofit 同时支持多个 BaseUrl 以及动态改变 BaseUrl
- [PRDownloader](https://gitee.com/chinasoft_ohos/PRDownloader) - 支持暂停与恢复的文件下载组件
- [fuel](https://gitee.com/chinasoft5_ohos/fuel) - 最简单的 HTTP 网络库
- [Fetch](https://gitee.com/chinasoft5_ohos/Fetch) - 简单灵活强大的下载管理库

[返回目录](#目录)

### <a name="文件数据类JAVA"></a>文件数据类

#### <a name="数据库JAVA"></a>数据库

- [greenDAO](https://gitee.com/harmonyos-tpc/greenDAO) - 最常用的数据库组件
- [Activeohos](https://gitee.com/harmonyos-tpc/Activeohos) - 数据库sqlite封装
- [RushOrm](https://gitee.com/harmonyos-tpc/RushOrm) - 通过将Java类映射到SQL表来替代对SQL的需求，封装为易于操作的数据库
- [LitePal](https://gitee.com/harmonyos-tpc/LitePal) - 数据库sqlite封装，简化sqlite操作
- [debug-database](https://gitee.com/baijuncheng-open-source/debug-database) - 封装原生数据库的增删改查操作， ORM方式操作对象对应数据库中的数据
- [ohos-database-sqlcipher](https://gitee.com/harmonyos-tpc/ohos-database-sqlcipher) - 数据库加密
- [ohos-NoSql](https://gitee.com/harmonyos-tpc/ohos-NoSql) - 轻量数据库
- [ormlite-ohos](https://gitee.com/harmonyos-tpc/ormlite-ohos) - 数据库
- [nitrite-java](https://gitee.com/openneusoft/nitrite-java) - Java嵌入式nosql文档库
- [EasiestSqlLibrary](https://gitee.com/openneusoft/easiest-sql-library) - 最简单的对数据库进行增删改查
- [rdb-explorer](https://gitee.com/openneusoft/rdb-explorer) - 适用于鸿蒙数据库的 快速简单的查看和管理
- [ohos_dbinspector](https://gitee.com/ts_ohos/ohos_dbinspector) - OpenHarmonyOS 实现的数据库实时显示效果应用
- [requery](https://gitee.com/harmonyos-tpc/requery) - 一个轻巧但功能强大的对象映射和SQL生成器，支持RxJava和Java8。轻松映射到或创建数据库，从任何使用Java的平台执行查询和更新。
- [sqlite-ohos](https://gitee.com/harmonyos-tpc/sqlite-ohos) - Sqlite包含openharmony数据库包装器API本机代码的优化版本，与更新的优化Sqlite版本一起，通常在所有情况下都比当前数据库API提供更好的性能。
- [GreenDaoUpgradeHelper](https://gitee.com/hihopeorg/GreenDaoUpgradeHelper) - 数据库升级
- [EasiestSqlLibrary](https://gitee.com/openneusoft/easiest-sql-library) - 一个SQL数据库的最简单和最懒惰的方法
- [RdbExplorer](https://gitee.com/openneusoft/easiest-sql-library) - 鸿蒙数据库可视化查看器
- [LiteOrm](https://gitee.com/hihopeorg/ohos-lite-orm) -  LiteOrm是一个小巧、强大、比系统自带数据库操作性能快1倍的 ORM 框架类库，开发者一行代码实现数据库的增删改查操作，以及实体关系的持久化和自动映射
- [DBFlow](https://gitee.com/chinasoft4_ohos/DBFlow) - DBFlow是一个快速、高效、功能丰富数据库组件

[返回目录](#目录)

#### <a name="preference-JAVA"></a>Preference

- [rx-preferences](https://gitee.com/harmonyos-tpc/rx-preferences) - 以rxjava的形式来保存和获取配置文件中的参数
- [preferencebinder](https://gitee.com/harmonyos-tpc/preferencebinder) - 基于Preferences封装存储工具
- [PreferenceRoom](https://gitee.com/chinasoft_ohos/PreferenceRoom) - 一个高效且结构化管理Preference的功能库
- [tray](https://gitee.com/baijuncheng-open-source/tray) - Preference 替代库
- [Secured-Preference-Store](https://gitee.com/archermind-ti/secured-preference-store) - openharmony Preferences的包装器，使用256位AES加密对内容进行加密
- [PrefCompat](https://gitee.com/harmonyos-tpc/PrefCompat) - Preferences类的便捷工具，同时支持通过RxJava
- [EasyPrefs](https://gitee.com/harmonyos-tpc/EasyPrefs) - 基于Preferences封装存储工具
- [binaryprefs](https://gitee.com/hihopeorg/binaryprefs) - Preferences 的快速轻量级重新实现，将每个首选项分别存储在文件中，通过 NIO 使用内存映射字节缓冲区执行磁盘操作，并在进程之间运行 IPC.
- [VNTNumberPickerPreference](https://gitee.com/chinasoft4_ohos/VNTNumberPickerPreference) - 数字选择器值自动保存

[返回目录](#目录)

#### <a name="数据存储JAVA"></a>数据存储

- [DiskLruCache](https://gitee.com/hihopeorg/DiskLruCache) - 磁盘Lru存储
- [MMKV](https://gitee.com/hihopeorg/MMKV) - 数据持久化键值对存储
- [hawk](https://gitee.com/harmonyos-tpc/hawk) - 安全，简单的键值存储
- [tray](https://gitee.com/harmonyos-tpc/tray) - 跨进程数据管理方法
- [Parceler](https://gitee.com/harmonyos-tpc/Parceler) - 任何类型的数据传输
- [RxCache](https://gitee.com/harmonyos-tpc/RxCache) - 这是一个专用于 RxJava，解决 OpenHarmony 中对任何 Observable 发出的结果做缓存处理的框架。
- [Secured-Preference-Store](https://gitee.com/archermind-ti/secured-preference-store) - 使用256位AES加密对Preferences内容进行加密。加密密钥安全地存储在设备的KeyStore中。您还可以使用EncryptionManager该类对开箱即用的数据进行加密和解密。
- [FilePicker](https://gitee.com/chinasoft4_ohos/FilePicker) - 从设备存储中选择文件/目录
- [Store](https://gitee.com/chinasoft4_ohos/Store) - 是一个Java库，用于轻松、反应式的数据加载

[返回目录](#目录)

### <a name="ui-自定义控件JAVA"></a>UI-自定义控件

#### <a name="image-JAVA"></a>Image

- [PhotoView](https://gitee.com/harmonyos-tpc/PhotoView) - 图片缩放查看
- [CircleImageView](https://gitee.com/harmonyos-tpc/CircleImageView) - 圆形图片
- [RoundedImageView](https://gitee.com/harmonyos-tpc/RoundedImageView) - 圆角图片
- [subsampling-scale-image-view](https://gitee.com/harmonyos-tpc/subsampling-scale-image-view) - 一个图片浏览工具，利用局部剪裁的算法支持超高清图片浏览且不卡顿，支持缩放平移等功能。
- [ContinuousScrollableImageView](https://gitee.com/harmonyos-tpc/ContinuousScrollableImageView) - 带动画播放的Image
- [AvatarImageView](https://gitee.com/harmonyos-tpc/AvatarImageView) - 头像显示库
- [PhotoDraweeView](https://gitee.com/chinasoft_ohos/PhotoDraweeView) - 多场景图片缩放移动处理
- [SuperImageView](https://gitee.com/chinasoft2_ohos/SuperImageView) - 无论图像大小如何，我们都需要在某些地方裁剪图像，支持网络图片裁剪
- [PaletteImageView](https://gitee.com/archermind-ti/palette-image-view) - PaletteImageView是一个可以解析图片中颜色，同时还可以为图片设置多彩阴影的控件
- [path-view](https://gitee.com/archermind-ti/path-view) - 读取web版svg文件（根节点为svg）并通过path measure对path路径加载设置动画
- [collageview](https://gitee.com/openneusoft/collageview) - 用于在应用程序中创建简单的照片拼贴。例如，在个人资料页
- [EffectiveShapeView](https://gitee.com/baijuncheng-open-source/effective-shape-view) - 一个根据输入的数值，绘制多边形，更改多边形边界宽度，设置附着三角形位置的库
- [BlurImageView](https://gitee.com/harmonyos-tpc/BlurImageView) - 高斯模糊图片
- [NineGridImageView](https://gitee.com/harmonyos-tpc/NineGridImageView) - 九宫格图片展示
- [ohos-combination-avatar](https://gitee.com/harmonyos-tpc/ohos-combination-avatar) - 仿QQ讨论组的多头像结合
- [gif-movie-view](https://gitee.com/ts_ohos/gif-movie-view) - 支持gif动画播放的控件。
- [ScratchTextView](https://gitee.com/hihopeorg/ScratchView) - 刮刮卡效果控件
- [SuperImageView](https://gitee.com/archermind-ti/SuperImageView) - 以模块化的方式为ImageView提供额外的功能（例如裁剪）
- [avatar-view](https://gitee.com/archermind-ti/avatar-view) - 以圆形的方式显示用户头像或者其名字首字的自定义Image组件
- [CaptchaImageView](https://gitee.com/chinasoft5_ohos/CaptchaImageView) - 自定义ImageView以生成验证码图像
- [SlideImageView](https://gitee.com/chinasoft5_ohos/SlideImageView) - 在视图内可滑动的ImageView
- [driveimageview](https://gitee.com/chinasoft5_ohos/driveimageview) - 可以添加一些漂亮多彩的文本或描述的ImageView
- [PolygonImageView](https://gitee.com/archermind-ti/polygon-image-view) - 自定义的多边形ImageView
- [PuzzleView](https://gitee.com/chinasoft4_ohos/PuzzleView) - 自定义拼图组件
- [StfalconImageViewer](https://gitee.com/chinasoft5_ohos/StfalconImageViewer) - 全屏图片查看器
- [ZoomPreviewPicture](https://gitee.com/chinasoft5_ohos/ZoomPreviewPicture) - 本项目受Zooming a View 启发，实现了点击小图放大至全屏预览，退出全屏恢复至原来位置这两个过程的动画过渡。 常见应用场景如微信朋友圈照片九宫格和微信聊天图片,视频,gif预览，某些手机系统相册等viewpager图片查看 缩放 拖拽下拉缩小退出（效果同微信图片浏览）
- [GifView](https://gitee.com/hihopeorg/GifView) - 显示gif图片
- [GifImageView](https://gitee.com/hihopeorg/GifImageView) - 加载处理GIF图像
- [CustomShapeImageView](https://gitee.com/hihopeorg/CustomShapeImageView) - 使用SVG和绘制形状的自定义形状图像视图的库
- [BigImageViewPager](https://gitee.com/hihopeorg/BigImageViewPager) - 一个图片浏览器，支持超大图、超长图、支持手势放大、支持查看原图、下载、加载百分比进度显示
- [Oblique](https://gitee.com/hihopeorg/Oblique) - 用Oblique探索新的图像显示方式
- [ImageTransition](https://gitee.com/hihopeorg/ImageTransition) - 用于从一个活动的圆形ImageView转换为已启动活动中的矩形ImageView
- [ImageGallery](https://gitee.com/hihopeorg/ImageGallery) - 提供一套图片浏览框架，可适配到用户应用中
- [LikeStarAnimation](https://gitee.com/chinasoft_ohos/LikeStarAnimation) - 带动画特效的点赞image
- [ImageLetterIcon](https://gitee.com/chinasoft4_ohos/ImageLetterIcon) - 实现类似用户联系人通讯录展示功能，支持带边框和字母的圆形、矩形、圆角矩形、图片背景效果功能
- [zoomage](https://gitee.com/chinasoft4_ohos/zoomage) - 简单缩放 Image 库，强调流畅自然的感觉
- [FAImageView](https://github.com/applibgroup/FAImageView) - HMOS Library called Frame Animation ImageView. You can animate multiple image

[返回目录](#目录)

#### <a name="text-JAVA"></a>Text

- [drawee-text-view](https://gitee.com/harmonyos-tpc/drawee-text-view) - 富文本组件
- [ReadMoreTextView](https://gitee.com/harmonyos-tpc/ReadMoreTextView) - 点击展开的Text控件
- [MaterialEditText](https://gitee.com/harmonyos-tpc/MaterialEditText) - 基于MaterialDesign设计的自定义输入框，可以支持多种风格，不同样式颜色的设置。并且拥有验证判断等功能同时支持正则计算
- [XEditText](https://gitee.com/harmonyos-tpc/XEditText) - 自定义特殊效果输入
- [lygttpod_SuperTextView](https://gitee.com/hihopeorg/lygttpod_SuperTextView) - 各种样式的自定义Text控件
- [TagView](https://gitee.com/chinasoft_ohos/TagView) - 实现文本可操作标签
- [BankCardFormat](https://gitee.com/chinasoft_ohos/BankCardFormat) - 自定义银行卡号输入框
- [AutoVerticalTextview](https://gitee.com/chinasoft_ohos/AutoVerticalTextview) - 纵向自动滚动的text
- [RTextView](https://gitee.com/chinasoft_ohos/RTextView) - 自定义Text控件，支持多种形状效果
- [JustifiedTextView](https://gitee.com/chinasoft_ohos/JustifiedTextView) - 文本对齐的Text控件
- [TextBannerView](https://gitee.com/harmonyos-tpc/TextBannerView) - 文字轮播图
- [ohos-viewbadger](https://gitee.com/harmonyos-tpc/ohos-viewbadger) - 文本标签View
- [ticker](https://gitee.com/harmonyos-tpc/ticker) - 显示滚动文本
- [stefanjauker_BadgeView](https://gitee.com/harmonyos-tpc/stefanjauker_BadgeView) - 仿iOS Springboard
- [CountAnimationTextView](https://gitee.com/harmonyos-tpc/CountAnimationTextView) - Text动画计数
- [ExpandableTextView](https://gitee.com/chinasoft_ohos/ExpandableTextView) - 实现可以展开/折叠的Text控件
- [Pinview](https://gitee.com/chinasoft_ohos/Pinview) - TextField光标设置图片背景未实现
- [ohos-materialshadowninepatch](https://gitee.com/chinasoft_ohos/ohos-materialshadowninepatch) - 实现可以给文本设置阴影效果
- [edittext-mask](https://gitee.com/chinasoft_ohos/edittext-mask) - 一个输入框控件，支持输入内容遮罩(掩码）
- [Badge](https://gitee.com/chinasoft2_ohos/Badge) - 个性化文字与图片tag
- [material-icons](https://gitee.com/openneusoft/material-icons) - 这是一个自定义控件，图标是无限可伸缩的，并且可以使用阴影以及您可以在文本上执行的所有操作进行自定义
- [MaterialTextField](https://gitee.com/harmonyos-tpc/MaterialTextField) - 一个漂亮带动画的输入框控件
- [MixtureTextView](https://gitee.com/harmonyos-tpc/MixtureTextView) - 支持图文混排，文字环绕图片的Text控件
- [DateTimeSeer](https://gitee.com/harmonyos-tpc/DateTimeSeer) - 实用的时间联想输入，能估计输入内容联想到准备的日期时间
- [TimeSinceTextView](https://gitee.com/harmonyos-tpc/TimeSinceTextView) - 根据设置是具体时间，转换成一个相对时间
- [floatlabelededittext](https://gitee.com/harmonyos-tpc/floatlabelededittext) - 带悬浮提示的输入框
- [CreditCardEntry](https://gitee.com/harmonyos-tpc/CreditCardEntry) - 适配银行卡号的输入框
- [VerificationCodeInput](https://gitee.com/ts_ohos/VerificationCodeInput) - 验证码输入框
- [GSYRichText](https://gitee.com/ts_ohos/richtext_ohos) - 支持类似微博的文本效果，表情、@某人、话题、url链接等。 DEMO同时演示了不同输入框输入效果的使用。
- [ExpandableTextView](https://gitee.com/ts_ohos/ExpandableTextView) - 多行文本缩略组件
- [TooltipView](https://gitee.com/ts_ohos/tooltip-view) - 自定义Text，向应用程序提供非常简单的方法添加提示框。
- [typerEditText](https://gitee.com/ts_ohos/typerEditText) - 仿打字机效果的自定义组件。
- [FloatingSearchView](https://gitee.com/ts_ohos/floating-search-view) - 搜索输入框控件。
- [RotatingText](https://gitee.com/hihopeorg/RotatingText) - 滚动切换文本控件。
- [AwesomeValidation](https://gitee.com/hihopeorg/AwesomeValidation) - 各种格式限制输入。
- [currency-edittext](https://gitee.com/hihopeorg/currency-edittext) - 自定义TextField实现，允许对基于货币的数字输入进行格式化。
- [FadingTextView](https://gitee.com/hihopeorg/fading-text-view) - 提供一套自定义文本框控件，支持复合文本内容交替显示。
- [AutoLinkTextView](https://gitee.com/hihopeorg/AutoLinkTextView) - 自动检测(#)标签，（@）标签，URL（http：//），电话和电子邮件以及处理点击事件的功能。
- [AnimRichEditor](https://gitee.com/ts_ohos/anim-rich-editor) - 富文本编辑器，使用户能够插入/删除位图和文本到编辑视图与动画
- [typerEditText](https://gitee.com/ts_ohos/typerEditText) - 仿打字机效果的自定义组件
- [CreditsRoll](https://gitee.com/archermind-ti/CreditsRoll) - 仿星球大战片尾字母特效
- [NoticeView](https://gitee.com/archermind-ti/noticeview) - 滚动播放文字的公告控件
- [nachos](https://gitee.com/archermind-ti/nachos) - 一个文本标签控件
- [CarbsExpandableTextView](https://gitee.com/hihopeorg/CarbsExpandableTextView) - 一个可展开、收起的Text控件
- [MarkdownView](https://gitee.com/chinasoft5_ohos/MarkdownView) - markdown格式的文本库
- [ExpandableTextView](https://gitee.com/hihopeorg/ExpandableTextView) - 可伸缩的Text
- [ScrollTextView](https://gitee.com/hihopeorg/ScrollTextView) - ScrollTextView是一个可以使文字实现跑马灯效果的组件
- [PinchZoomTextView](https://gitee.com/archermind-ti/pinch-zoom-text-view) - 手指手势来增加/减小字体大小的文本库
- [BabushkaText](https://gitee.com/archermind-ti/BabushkaText) - 自定义富文本组件，提供一种简单方法样式化Text，实现自定义文本各部分样式功能
- [BiuEditText](https://gitee.com/chinasoft4_ohos/BiuEditText) - 输入或删除字符时会有弹出效果
- [justified](https://gitee.com/chinasoft4_ohos/justified) - Text和TextField文本对齐
- [ColorTextView](https://gitee.com/chinasoft4_ohos/ColorTextView) - 给指定文字设置指定颜色
- [ExpandTextView](https://gitee.com/chinasoft4_ohos/ExpandTextView) - 一个可折叠和展开的Text控件
- [WeatherIconView](https://gitee.com/chinasoft5_ohos/WeatherIconView) - Weather Icon View-ohos提供显示天气图标的自定义视图
- [ohos-rich-text-editor](https://gitee.com/chinasoft5_ohos/ohos-rich-text-editor) - Span实现富文本多种样式的编辑
- [socialview](https://gitee.com/chinasoft5_ohos/socialview) - 富文本输入框 支持hashtag、notify和超链接
- [SimplifySpan](https://gitee.com/chinasoft5_ohos/SimplifySpan) - 简化的Spanner库
- [Hijckr](https://gitee.com/chinasoft4_ohos/Hijckr) - 实现动态加载
- [XRichText](https://gitee.com/chinasoft4_ohos/XRichText) - 富文本类库，支持图文混排，支持编辑和预览，支持插入和删除图片
- [text-decorator](https://gitee.com/chinasoft3_ohos/text-decorator) - 实现可以针对文本设置不同的样式
- [OhosEdit](https://gitee.com/chinasoft4_ohos/OhosEdit) - EditText的撤销和恢复撤销操作
- [ReadMoreTextView](https://gitee.com/chinasoft4_ohos/ReadMoreTextView) - 带有修剪文本的自定义文本视图
- [customedittext](https://gitee.com/chinasoft5_ohos/customedittext) - 一个非常简单和轻量级的编辑框
- [FlexibleSearchBar](https://github.com/applibgroup/FlexibleSearchBar) - Scalable search bar, imitating Huawei's application market
- [SpannableTextView](https://github.com/applibgroup/SpannableTextView) - SpannableTextView is a custom TextView which lets you customize the
  styling of SpannableStyles of your text or statment via Spannables, but without the hassle of having to deal directly with Spannable themselves.
- [TextViewOverflowing](https://github.com/applibgroup/TextViewOverflowing) - A simple library to add indicators for your Carousel or PageSlider.
- [SizeAdjustingTextView](https://github.com/applibgroup/SizeAdjustingTextView) - This is based on an open source autosizing textview for HMOS

[返回目录](#目录)

#### <a name="button-JAVA"></a>Button

- [FloatingActionButton](https://gitee.com/harmonyos-tpc/FloatingActionButton) - 悬浮button
- [circular-progress-button](https://gitee.com/harmonyos-tpc/circular-progress-button) - 一个带进度条的自定义按钮，支持多种不能样式多种状态跳转
- [progressbutton](https://gitee.com/harmonyos-tpc/progressbutton) - 带进度的自定义按钮
- [SwitchButton](https://gitee.com/harmonyos-tpc/SwitchButton) - 仿ios的开关按钮
- [SlideSwitch](https://gitee.com/harmonyos-tpc/SlideSwitch) - 多种样式的开关按钮
- [iOS-SwitchView](https://gitee.com/chinasoft_ohos/iOS-SwitchView) - 仿ios的开关按钮
- [Highlight](https://gitee.com/harmonyos-tpc/Highlight) - 指向性功能高亮
- [SwitchButton](https://gitee.com/harmonyos-tpc/SwitchButton) - 开关按钮
- [slideview](https://gitee.com/harmonyos-tpc/slideview) - 自定义滑动按钮
- [ohos-process-button](https://gitee.com/chinasoft_ohos/ohos-process-button) - 显示Button各种加载状态
- [Fancybuttons](https://gitee.com/chinasoft_ohos/Fancybuttons) - 可制作带icon、边框的按钮
- [StateButton](https://gitee.com/chinasoft2_ohos/StateButton) - button点击效果
- [AwesomeSwitch](https://gitee.com/hihopeorg/AwesomeSwitch) - AwesomeSwitch替代了标准Switch，并且比标准开关组件提供了更多的自定义功能
- [LikeButton](https://gitee.com/baijuncheng-open-source/LikeButton) - 一点类似twitter的点赞按钮
- [SubmitButton](https://gitee.com/harmonyos-tpc/SubmitButton) - 带提交动画的按钮(tpc)
- [TriStateToggleButton](https://gitee.com/harmonyos-tpc/TriStateToggleButton) - 拥有三种状态的开关按钮
- [CompoundButtonGroup](https://gitee.com/harmonyos-tpc/CompoundButtonGroup) - 一个简单的多选按钮控件
- [LikeButton](https://gitee.com/harmonyos-tpc/LikeButton) - 类似社区按钮集合，点赞分享评论等
- [MaterialFavoriteButton](https://gitee.com/ts_ohos/MaterialFavoriteButton) - 一个适用于OHOS的自定义点赞/标记button
- [material-animated-switch](https://gitee.com/ts_ohos/material-animated-switch) - 一个添加了icon动画和颜色变化的切换按钮
- [AnimShopButton](https://gitee.com/ts_ohos/anim-shop-button) - 一个仿饿了么 带伸缩位移旋转动画的购物车按钮
- [ElegantNumberButton](https://gitee.com/hihopeorg/ElegantNumberButton) - 可以增减的自定义控件
- [morphing-button](https://gitee.com/hihopeorg/ohos-morphing-button) - 变形按钮控件
- [PlayPauseView](https://gitee.com/archermind-ti/playpauseview) - 一个播放、暂停可以优雅的过渡的按钮
- [HoldingButton](https://gitee.com/archermind-ti/holdingbutton) - 一个当用户按住按钮时可见的按钮
- [ProgressRoundButton](https://gitee.com/chinasoft5_ohos/ProgressRoundButton) - 实现一个平滑的下载按钮
- [ohos-flat-button](https://gitee.com/hihopeorg/ohos-flat-button) - 按钮点击，切换当前背景色
- [swipe-button](https://gitee.com/hihopeorg/swipe-button) - 滑动按钮
- [roundbutton](https://gitee.com/archermind-ti/roundbutton) - 可设置圆角背景边框的的按钮 通过调节色彩明度自动计算按下(pressed)状态颜色
- [ MovingButton](https://gitee.com/archermind-ti/MovingButton) - 向八个方向移动的按钮
- [ KTLoadingButton](https://gitee.com/archermind-ti/KTLoadingButton) - 一个简单的Loading按钮
- [SparkButton](https://gitee.com/chinasoft4_ohos/SparkButton) - 一个拥有与Twitter心跳动画类似效果的按钮
- [IconSwitch](https://gitee.com/chinasoft5_ohos/IconSwitch) - 自定义Switch小部件
- [custom-signin-button](https://gitee.com/chinasoft5_ohos/custom-signin-button) - 带图标样式的按钮

[返回目录](#目录)

#### <a name="listcontainer-JAVA"></a>ListContainer

- [FloatingGroupExpandableListView](https://gitee.com/harmonyos-tpc/FloatingGroupExpandableListView) - 自定义list组件，支持分类带标题
- [XRecyclerView](https://gitee.com/harmonyos-tpc/XRecyclerView) - 基于ListContainer 一个简单的下拉刷新上来加载的控件
- [PullToZoomInListView](https://gitee.com/harmonyos-tpc/PullToZoomInListView) - 顶部放大List
- [WaveSideBar](https://gitee.com/harmonyos-tpc/WaveSideBar) - 类似于通讯录带字母选择的列表组件
- [SwipeActionAdapter](https://gitee.com/harmonyos-tpc/SwipeActionAdapter) - list侧滑菜单
- [ToDoList](https://gitee.com/hihopeorg/ToDoList) - 支持多样性自定义化的list控件
- [SectionedRecyclerViewAdapter](https://gitee.com/hihopeorg/SectionedRecyclerViewAdapter) - 支持多样性自定义化的list控件
- [ARecyclerView](https://gitee.com/chinasoft_ohos/ARecyclerView) - 自定义listContainer控件
- [StickyHeadersib](https://gitee.com/chinasoft_ohos/StickyHeaders) - 支持列表分组标题
- [RoundedLetterView](https://gitee.com/chinasoft_ohos/RoundedLetterView) - 简单的通讯录ui库
- [AStickyHeader_ohos](https://gitee.com/isrc_ohos/asticky-header_ohos) - 分组标题栏滑动时置顶效果
- [CalendarListview](https://gitee.com/harmonyos-tpc/CalendarListview) - 日历选择器
- [SlideAndDragListView](https://gitee.com/harmonyos-tpc/SlideAndDragListView) - 自定义ListContaner控件, 实现左右滑动,上下拖动更换item的位置
- [pinned-section-listview](https://gitee.com/harmonyos-tpc/pinned-section-listview) - 支持列表分组标题
- [HeaderAndFooterRecyclerView](https://gitee.com/harmonyos-tpc/HeaderAndFooterRecyclerView) - 支持addHeaderView，addFooterView到ListContainer
- [MultiType](https://gitee.com/harmonyos-tpc/MultiType) - 为简便ListContainer创建多种类型
- [StickyListHeaders](https://gitee.com/harmonyos-tpc/StickyListHeaders) - 支持列表分组标题
- [MaterialSpinner](https://gitee.com/chinasoft_ohos/MaterialSpinner) - 实现Material风格的可下拉列表控件
- [PinnedSectionItemDecoration](https://gitee.com/chinasoft_ohos/PinnedSectionItemDecoration) - 实现ListContainer滑动可悬停的标题栏
- [RecyclerViewSwipeDismiss](https://gitee.com/chinasoft2_ohos/RecyclerViewSwipeDismiss) - 水平、垂直方向滑动删除，设置不同状态背景
- [header-decor](https://gitee.com/chinasoft2_ohos/header-decor) - RecyclerView的粘性头部装饰器
- [recycler-fast-scroll](https://gitee.com/chinasoft_ohos/recycler-fast-scroll) - ListContainer 的快速滑动和分区显示
- [MaterialList](https://gitee.com/chinasoft2_ohos/MaterialList) - MaterialList是一个帮助开发者展示漂亮Card视图的功能库
- [DragListView](https://gitee.com/chinasoft_ohos/DragListView) - 实现ListContainer多级滑动及自动居中
- [BGASwipeItemLayout-ohos](https://gitee.com/chinasoft_ohos/BGASwipeItemLayout-ohos) - 带弹簧效果的左右滑动控件，可作为ListContainer的item
- [ohos-GridViewWithHeaderAndFooter](https://gitee.com/chinasoft_ohos/ohos-GridViewWithHeaderAndFooter) - 支持给网格布局的ListContainer添加头布局、尾布局
- [Slice](https://gitee.com/chinasoft3_ohos/Slice) - 类似CardView效果的自定义控件
- [AsymmetricGridView](https://gitee.com/chinasoft_ohos/AsymmetricGridView) - 支持跨列的网格组件
- [ExpansionPanel](https://gitee.com/chinasoft_ohos/ExpansionPanel) - 一个效果上类似ListView的控件，支持对子控件的独立拉伸、编辑操作
- [greedo-layout-for-ohos](https://gitee.com/chinasoft2_ohos/greedo-layout-for-ohos) - 根据图片比例展示图片流，固定高度展示图片流
- [drag-select-recyclerview](https://gitee.com/chinasoft3_ohos/drag-select-recyclerview) - 简单的多选列表功能库
- [SnappingSwipingRecyclerView](https://gitee.com/chinasoft2_ohos/SnappingSwipingRecyclerView) - ListContainer类似于viewpager的实现,长按即可滑动删除
- [turn-layout-manager](https://gitee.com/chinasoft2_ohos/turn-layout-manager) - 支持四个方向切换，设置半径、偏移量、文字方向等功能
- [OpenHarmonyTreeView](https://gitee.com/baijuncheng-open-source/OpenHarmonyTreeView) - 树状列表
- [TreeView](https://gitee.com/baijuncheng-open-source/tree-view) - 树状列表
- [RecycleView](https://gitee.com/baijuncheng-open-source/recycle-view) - 这是一个功能丰富而灵活的数据列表操作组件
- [DoubleStickyHeadersList](https://gitee.com/baijuncheng-open-source/DoubleStickyHeadersList) - 一个OpenHarmony库，用于粘贴到列表顶部的双层节头。OpenHarmony小部件，特别用于显示具有多级分类的项目
- [ohos-parallax-recyclerview](https://gitee.com/baijuncheng-open-source/ohos-parallax-recyclerview) - 一个条目滑动点击，点击按钮切换adpater,条目内容更换的库
- [Ohos-InfiniteCards](https://gitee.com/chinasoft2_ohos/Ohos-InfiniteCards) - 叠加式卡片列表
- [ohosSwipeLayout](https://gitee.com/harmonyos-tpc/ohosSwipeLayout) - 滑动删除
- [epoxy](https://gitee.com/harmonyos-tpc/epoxy) - 自定义组件RecyclerComponent以及扩展功能
- [TableFixHeaders](https://gitee.com/harmonyos-tpc/TableFixHeaders) - 自定义表格控件
- [FocusLayoutManager](https://gitee.com/harmonyos-tpc/FocusLayoutManager) - 自定义横向列表RecyclerView控件
- [SimplifiedRecyclerview](https://gitee.com/harmonyos-tpc/SimplifiedRecyclerview) - 支持水平和垂直平移图像的openharmony库。
- [EasyAdapter](https://gitee.com/harmonyos-tpc/EasyAdapter) - 基于BaseItemProvider优化的鸿蒙版适配器。
- [AdapterDelegates](https://gitee.com/hihopeorg/AdapterDelegates) - AdapterDelegates使用组合模式构建列表适配器，方便组件重用。
- [FeaturedRecyclerView](https://gitee.com/hihopeorg/FeaturedRecyclerView) - 自定义视图组扩展ListContainer使得位于顶部的第一个项为最大项（通过将其高度设置为featuredItemHeight）。
- [excelPanel](https://gitee.com/hihopeorg/excelPanel) - 使用ListContainer实现Excel表格功能
- [RecyclerItemDecoration](https://gitee.com/archermind-ti/recycleritemdecoration) - 一个允许开发人员使用ItemDecoration轻松创建RecyclerView的库
- [OHOSFastScroll](https://gitee.com/archermind-ti/ohosfastscroll) - 一款完全可定制覆盖轨道，拇指，弹出，动画和滚动，易于使用的快速滚动的列表控件
- [async-expandable-list-master](https://gitee.com/archermind-ti/async-expandable-list-master) - 一个异步展开的多级列表组件
- [shimmerrecyclerview](https://gitee.com/archermind-ti/shimmerrecyclerview) - 一个的可以单列或者多列的列表组件
- [recyclerviewhelper](https://gitee.com/archermind-ti/recyclerviewhelper) - 为RecyclerView提供常见的功能，如拖拽、左右滑动删除、分割线、点击事件等
- [ListItemView](https://gitee.com/archermind-ti/listitemview) - Material Design中 List Item 的实现
- [RecyclerView-FastScroll](https://gitee.com/chinasoft4_ohos/RecyclerView-FastScroll) - 简单的快速滚动列表控件
- [Taurus](https://gitee.com/archermind-ti/taurus) - 具有小飞机动画的下拉刷新组件
- [PullRefreshLayout](https://gitee.com/archermind-ti/pullrefreshlayout) - 一个下拉刷新、上拉加载自定义组件
- [pull-to-make-soup](https://gitee.com/archermind-ti/persistentsearchview) - 可以设置自定义动画的下拉刷新库
- [BeautifulRefreshLayout](https://gitee.com/chinasoft5_ohos/BeautifulRefreshLayout) - 好看的下拉刷新组件
- [Ohos-MaterialRefreshLayout](Ohos-MaterialRefreshLayout) - Material风格下拉刷新组件
- [AnimRefreshRecyclerView](Ohos-MaterialRefreshLayout) - 为应用提供列表、宫格布局和3种下拉刷新效果
- [BeerSwipeRefreshLayout](https://gitee.com/archermind-ti/beerswiperefresh) - 具有倒啤酒动画的下拉刷新组件
- [expandable-recycler-view](https://gitee.com/chinasoft5_ohos/expandable-recycler-view) - 可折叠的列表菜单
- [FlexibleAdapter](https://gitee.com/hihopeorg/FlexibleAdapter) - FlexibleAdapter是一个灵活的ListContainer适配器，可以帮助开发人员简化ListContainer的使用。它很容易使用，并且是可配置的。
- [SuperRecyclerView](https://gitee.com/hihopeorg/SuperRecyclerView) - 集成上拉加载下拉刷新侧滑删除的ListContainer
- [BGABaseAdapter_ohos](https://gitee.com/hihopeorg/BGABaseAdapter-ohos) - 简化ListContainer适配器的编写
- [cwac-merge](https://gitee.com/hihopeorg/cwac-merge) - 支持多个adapter和component统一设置给ListContainer显示
- [SectionedRecyclerView](https://gitee.com/archermind-ti/sectionedrecyclerview) - SectionedRecyclerView是一个用于创建带有部分的 ListContainer 的适配器，提供headers 和 footers
- [PopupListView](https://gitee.com/archermind-ti/PopupListView) - 强大的ListContainer，可以点击lListContainer中的哪个item，在item view下弹出显示内部视图
- [RecyclerCoverFlow](https://gitee.com/chinasoft5_ohos/RecyclerCoverFlow) - 使用ListContainer实现旋转木马相册效果
- [FamiliarRecyclerView](https://gitee.com/chinasoft5_ohos/FamiliarRecyclerView) - ListContainer实现横竖列表,横竖网格视图以及下拉刷新和加载更多
- [CarouselView](https://gitee.com/chinasoft4_ohos/CarouselView) - ListContainer横向轮播展示
- [Linkage-RecyclerView](https://gitee.com/chinasoft4_ohos/Linkage-RecyclerView) - 实现二级联动列表
- [FullRecyclerView](https://gitee.com/chinasoft5_ohos/FullRecyclerView) - 实现recyclerView中不同种类和操作的效果
- [ParallaxRecyclerView](https://gitee.com/chinasoft4_ohos/ParallaxRecyclerView) - 一个视差效果的ListContainer控件
- [expandable-recycler-view](https://github.com/applibgroup/expandable-recycler-view) - Expandable-recycler-view is an HarmonyOS library with Custom BaseItemProvider for expanding and collapsing groups
- [SharpView-Lib](https://github.com/applibgroup/SharpView-Lib) - It is a HMOS Library which provides a Custom TextView,LinearLayout,RelativeLayout with sharpView
- [PullDownView](https://github.com/applibgroup/PullDownView) - TextViewOverflowing is a HMOS library that provides custom view and allows reflowing from one TextView to another.

[返回目录](#目录)

#### <a name="pageslider-JAVA"></a>PageSlider

- [ViewPagerIndicator](https://gitee.com/harmonyos-tpc/ViewPagerIndicator) - 星级最高的Slider组件
- [PageIndicatorView](https://gitee.com/harmonyos-tpc/PageIndicatorView) - 自定义适配器组件
- [UltraViewPager](https://gitee.com/harmonyos-tpc/UltraViewPager) - 多种样式的Slider自定义控件
- [SlidingDrawer](https://gitee.com/harmonyos-tpc/SlidingDrawer) - 自定义Slider组件
- [AppIntro](https://gitee.com/harmonyos-tpc/AppIntro) - 为应用程序构建一个很酷的轮播介绍
- [ParallaxViewPager](https://gitee.com/harmonyos-tpc/ParallaxViewPager) - 自定义Slider组件
- [MZBannerView](https://gitee.com/harmonyos-tpc/MZBannerView) - 一个简单的图片轮播控件
- [FlycoPageIndicator](https://gitee.com/harmonyos-tpc/FlycoPageIndicator) - 多种样式的页面指示器
- [SCViewPager](https://gitee.com/harmonyos-tpc/SCViewPager) - 具有转场动画的PageSlider自定义控件
- [imagecoverflow](https://gitee.com/harmonyos-tpc/ImageCoverFlow) - 3D视角适配器
- [ohos-ConvenientBanner](https://gitee.com/hihopeorg/ohos-ConvenientBanner) - 自定义banner组件
- [Banner](https://gitee.com/chinasoft_ohos/Banner) - Banner图片轮播控件
- [Material-ViewPagerIndicator](https://gitee.com/chinasoft_ohos/Material-ViewPagerIndicator) - 页面指示器，实现平移，显隐组合动画效果
- [Banner_ohos](https://gitee.com/isrc_ohos/banner_ohos) - 广告图片轮播控件
- [BGABanner-ohos](https://gitee.com/chinasoft_ohos/BGABanner-ohos) - 广告轮播，循环轮播
- [ViewPagerHelper](https://gitee.com/chinasoft_ohos/ViewPagerHelper) - 能够帮你快速实现导航栏轮播图，app引导页，内置多种tab指示器，让你告别 PageSlider 的繁琐操作，专注逻辑功能
- [VerticalViewPager](https://gitee.com/chinasoft2_ohos/VerticalViewPager) - ViewPager垂直方向页面滑动
- [ViewPagerTransforms](https://gitee.com/chinasoft_ohos/ViewPagerTransforms) - 提供一个更易于使用和扩展PageSlide动画的实现
- [BannerViewPager](https://gitee.com/chinasoft_ohos/BannerViewPager) - ViewPager轮播图
- [ohos-Coverflow](https://gitee.com/chinasoft_ohos/ohos-Coverflow) - 轮播图自定义组件
- [RollViewPager](https://gitee.com/chinasoft_ohos/RollViewPager) - 轮播图自定义组件
- [InkPageIndicator](https://gitee.com/chinasoft_ohos/InkPageIndicator) - ViewPager指示器控件
- [AdvancedPagerSlidingTabStrip](https://gitee.com/baijuncheng-open-source/advanced-pager-sliding-tab-strip) - AdvancedPagerSlidingTabStrip是一种HarmonyOS平台的导航控件，完美兼容HarmonyOS自带库和兼容库的PageSlider组件
- [WoWoViewPager](https://gitee.com/baijuncheng-open-source/WoWoViewPager) - 结合ViewPager和动画
- [HorizontalPicker](https://gitee.com/harmonyos-tpc/HorizontalPicker) -一个简单的横向菜单选择器控件
- [SmartTabLayout](https://gitee.com/hihopeorg/SmartTabLayout) - 自定义TabLayout组件
- [StatefulLayout](https://gitee.com/harmonyos-tpc/StatefulLayout) - 可以左右切换布局有点类似PageSlider，显示最常见的布局状态模板，如加载、空、错误布局等
- [NavigationTabStrip](https://gitee.com/hihopeorg/NavigationTabStrip) - Viewpager导航指示器,提供多种样式,支持自定义
- [FlycoTabLayout](https://gitee.com/harmonyos-tpc/FlycoTabLayout) - 自定义TabLayout组件，支持三种模式多种状态设置。
- [ViewPagerIndicator](https://gitee.com/baijuncheng-open-source/ViewPagerIndicator) - ViewPager指示器
- [Banner-Slider](https://gitee.com/baijuncheng-open-source/banner-slider) - 一个简易的图片滑动的库
- [JazzyViewPager](https://gitee.com/harmonyos-tpc/JazzyViewPager) - 自定义撞击效果动画的ViewPager控件
- [welcome-ohos](https://gitee.com/harmonyos-tpc/welcome-ohos) - 一个简单好用的初始欢迎界面
- [ProgressPageIndicator](https://gitee.com/harmonyos-tpc/ProgressPageIndicator) - pageSlider指示器组件
- [StackOverView](https://gitee.com/harmonyos-tpc/StackOverView) - 卡片式翻页组件
- [DecentBanner](https://gitee.com/ts_ohos/DecentBanner) - 自定义PageSlider控件，支持自动轮播
- [ahoy-onboarding](https://gitee.com/ts_ohos/ahoy-onboarding) - 一套三方UI库，可以方便地设置背景
- [welcome-coordinator](https://gitee.com/hihopeorg/welcome-coordinator) - 自定义动态欢迎引导页面
- [IndicatorBox](https://gitee.com/hihopeorg/IndicatorBox) - 一个自定义指示框视图
- [CoolViewPager](https://gitee.com/hihopeorg/CoolViewPager) - PagerSlider页面切换，滑动方向设置，自动轮播以及轮播间隔时长等
- [dotsindicator](https://gitee.com/archermind-ti/dotsindicator) - 不同样式的页面指示器
- [imageslider](https://gitee.com/archermind-ti/imageslider) - 神奇的轮播图组件
- [GuideBackgroundColorAnimation](https://gitee.com/archermind-ti/guidebackgroundcoloranimation) -  在滑动PageSlider的时候，给PageSlider增加背景动态变化的效果的库
- [StickyHeaderViewPager](https://gitee.com/archermind-ti/stickyheaderviewpager) - 当ItemView在PageSlider中滚动时，该库支持将导航器贴在顶部
- [QingtingBannerView](https://gitee.com/archermind-ti/qingtingbannerview) - 仿蜻蜓FM轮播banner
- [ParallaxPager](https://gitee.com/chinasoft5_ohos/ParallaxPager) - 使用视差效果为你的PageSlider滚动添加一些深度
- [carouselview](https://gitee.com/chinasoft5_ohos/carouselview) - 自定义轮播图组件
- [SwipeStack](https://gitee.com/chinasoft4_ohos/SwipeStack) - 可滑动视图堆栈
- [ExpandablePager](https://gitee.com/chinasoft5_ohos/ExpandablePager) - 包含PageSlider的布局，可以在两种状态(展开和折叠)之间垂直滑动
- [GalleryLayoutManager](https://github.com/lawloretienne/DiscreteSlider) - 画廊和轮播图效果的列表
- [banner](https://gitee.com/chinasoft4_ohos/banner) - 基于PageSlider实现无限轮播功能。可以自定义indicator，需自定义实现 Indicator 接口
- [ViewPagerCardTransformer](https://gitee.com/chinasoft5_ohos/ViewPagerCardTransformer) - 各种方向的ViewPager层叠卡片
- [AutoScrollViewPager](https://gitee.com/chinasoft5_ohos/AutoScrollViewPager) - 可以自动滚动的 ViewPager，与 ViewPagerIndicator 兼容
- [ViewPagerIndicator](https://gitee.com/hihopeorg/ViewPagerIndicator) - 实现网易顶部tab，新浪微博主页底部tab，引导页，无限轮播banner等效果，高度自定义tab和特效
- [XBanner](https://gitee.com/hihopeorg/XBanner) - 广告轮播，循环轮播
- [rtl-viewpager](https://gitee.com/hihopeorg/rtl-viewpager) - RtlPageSlider 从右向左，PageSlider展示
- [InfiniteViewPager](https://gitee.com/hihopeorg/InfiniteViewPager) - 提供一套自定义PageSlider控件，可提供等效的无限滑动功能
- [MagicViewPager](https://gitee.com/hihopeorg/MagicViewPager) - 提供PageSlider炫酷切换效果，适用于Banner等
- [cardslider-ohos](https://gitee.com/hihopeorg/ohos-cardslider) - UI控制器,卡片之间滑动切换
- [CircleIndicator](https://gitee.com/archermind-ti/CircleIndicator) - 一个轻量级的pageslider指示器！可以跟随屏幕滑动而滑动
- [viewpagerindicator](https://gitee.com/chinasoft_ohos/ViewPagerIndicator) - 多种样式slider
- [ViewPagerIndicator](https://gitee.com/chinasoft_ohos/ViewPagerIndicator) - 灵活自定义功能丰富的Slider组件
- [ohos-slideshow-widget](https://gitee.com/chinasoft4_ohos/ohos-slideshow-widget) - 一个可以实现幻灯片放映的小部件
- [RevealBanner](https://gitee.com/chinasoft4_ohos/RevealBanner) - 滑动特效banner
- [PullToDismissPager](https://gitee.com/chinasoft5_ohos/PullToDismissPager) - 可以下拉消失的PageSlider
- [ohos-auto-scroll-view-pager](https://gitee.com/chinasoft4_ohos/ohos-auto-scroll-view-pager) - 一个可设置间隔的自动滑动pagerslider组件
- [freepager](https://gitee.com/chinasoft4_ohos/freepager) - 视图分页器
- [IndicatorView](https://github.com/applibgroup/IndicatorView) - A simple library to add indicators for your Carousel or PageSlider.
- [SlideActionView](https://github.com/applibgroup/SlideActionView) - HMOS view which provides a nice slide-to-left/right interaction.


[返回目录](#目录)

#### <a name="progressbar-JAVA"></a>ProgressBar

- [MaterialProgressBar](https://gitee.com/harmonyos-tpc/MaterialProgressBar) - 多种样式自定义progressbar
- [MaterialRatingBar](https://gitee.com/harmonyos-tpc/MaterialRatingBar) - Material样式的Rating
- [discreteSeekBar](https://gitee.com/harmonyos-tpc/discreteSeekBar) - 基于populdialog控件实现动画冒泡式显示进度的一个自定义seekbar控件
- [ohos-HoloCircularProgressBar](https://gitee.com/harmonyos-tpc/ohos-HoloCircularProgressBar) - 自定义progressBar
- [circular-music-progressbar](https://gitee.com/harmonyos-tpc/circular-music-progressbar) - 类似于音乐播放器的圆形progressbar
- [SectorProgressView](https://gitee.com/harmonyos-tpc/SectorProgressView) - 自定义圆形progressBar
- [LikeSinaSportProgress](https://gitee.com/harmonyos-tpc/LikeSinaSportProgress) - 仿新浪体育客户端的点赞进度条
- [ArcSeekBar](https://gitee.com/hihopeorg/ArcSeekBar) - 带有弧度的seekbar
- [MaterialishProgress](https://gitee.com/hihopeorg/materialish-progress) - Materia风格的Progress控件
- [RoundCornerProgressBar](https://gitee.com/hihopeorg/RoundCornerProgressBar) - 进度条效果设置库
- [BoxedVerticalSeekBar](https://gitee.com/chinasoft_ohos/BoxedVerticalSeekBar) - 自定义纵向seekbar
- [ProgressWheel_ohos](https://gitee.com/isrc_ohos/progress-wheel_ohos) - 开源进度轮
- [MagicProgressWidget](https://gitee.com/harmonyos-tpc/MagicProgressWidget) - 颜色渐变的圆形进度条和纯色轻量横向进度条
- [NumberProgressBar](https://gitee.com/harmonyos-tpc/NumberProgressBar) - 一款可显示数字，可设置进度条颜色，文字大小等属性自定义数字进度条
- [ArcProgressStackView](https://gitee.com/harmonyos-tpc/ArcProgressStackView) - 弧形模式下显示进度条
- [ProgressPieView](https://gitee.com/harmonyos-tpc/ProgressPieView) - 自定义进度饼
- [CoreProgress](https://gitee.com/harmonyos-tpc/CoreProgress) - 上传加载进度框架
- [CircularProgressView](https://gitee.com/harmonyos-tpc/CircularProgressView) - Material圆形进度条
- [ButtonProgressBar](https://gitee.com/harmonyos-tpc/ButtonProgressBar) - 自定义按钮进度条
- [ProgressView](https://gitee.com/harmonyos-tpc/ProgressView) - 自定义ProgressView
- [CircleProgress](https://gitee.com/harmonyos-tpc/CircleProgress) - 自定义圆形进度条
- [CProgressButton](https://gitee.com/harmonyos-tpc/CProgressButton) - 自定义进度条按钮
- [WhorlView](https://gitee.com/harmonyos-tpc/WhorlView) - 带螺纹样式的进度条
- [ACProgressLite](https://gitee.com/chinasoft2_ohos/ACProgressLite) - openharmony 加载控件库，简洁、易用、可定制性强。用于快速实现类似 iOS 的 “加载中” 等弹出框。
- [IndicatorSeekBar](https://gitee.com/chinasoft_ohos/IndicatorSeekBar) - 自定义可滑动进度条库
- [Zloading](https://gitee.com/chinasoft_ohos/Zloading) - 一款自定义的炫酷的加载动画类库
- [AndRatingBar](https://gitee.com/chinasoft_ohos/AndRatingBar) - 继承自原生Rating，具有原生的滑动选择等特性，并且可以自定义大小，间距，颜色，图标，支持从右当左
- [BubbleSeekBar](https://gitee.com/chinasoft2_ohos/BubbleSeekBar) - 自定义SeekBar，进度变化由可视化气泡样式呈现，定制化程度较高，适合大部分需求
- [SeekBarCompat](https://gitee.com/hihopeorg/SeekBarCompat) - SeekBarCompat是一个Slider的封装库
- [FABProgressCircle](https://gitee.com/baijuncheng-open-source/fabprogress-circle) - 圆形进度条
- [TheGlowingLoader](https://gitee.com/baijuncheng-open-source/the-glowing-loader) - TheGlowingLoader组件是一个易于定制的自定义动画加载视图
- [square-progressbar](https://gitee.com/baijuncheng-open-source/square-progressbar) - 图片边缘进度条
- [AdhesiveLoadingView](https://gitee.com/baijuncheng-open-source/adhesive-loading-view) - 具有粘性的滑动小球，跌落反弹形成loading的效果
- [CircleProgressBar](https://gitee.com/harmonyos-tpc/CircleProgressBar) - 圆形进度条
- [SmoothProgressBar](https://gitee.com/harmonyos-tpc/SmoothProgressBar) - 自定义进度条控件，loading动画
- [CircularProgressDrawable](https://gitee.com/harmonyos-tpc/CircularProgressDrawable) - 自定义样式圆形进度条控件
- [Circle-Progress-View](https://gitee.com/harmonyos-tpc/Circle-Progress-View) - 自定义圆形进度条
- [dotted-progress-bar](https://gitee.com/harmonyos-tpc/dotted-progress-bar) - 自定义圆点进度条
- [BndrsntchTimer](https://gitee.com/harmonyos-tpc/BndrsntchTimer) - 随时间收缩的水平进度条
- [materialish-progress](https://gitee.com/harmonyos-tpc/materialish-progress) - 一个简单的圆形进度条，支持有数值和没数值两种模式的进度设置
- [material-range-bar](https://gitee.com/harmonyos-tpc/material-range-bar) - 一个带有中间节点的自定义进度条
- [SnailBar](https://gitee.com/ts_ohos/SnailBar) - 一个蜗牛样式的进度条控件
- [StateProgressBar](https://gitee.com/ts_ohos/StateProgressBar) - 带节点的进度条
- [MultiSlider](https://gitee.com/hihopeorg/MultiSlider) - 多指和多功能组件。它可以作为一个普通的Slider，一个范围栏和多个拇指条
- [SeekArc](https://gitee.com/archermind-ti/seekarc) - 不同样式的拖动条
- [ProSwipeButton](https://gitee.com/archermind-ti/proswipebutton) - 滑动按钮，带有用于异步操作的圆形进度栏
- [SpecialProgressBar](https://gitee.com/archermind-ti/special-progress-bar) - 一个特殊的下载进度条组件
- [CircularProgressIndicator](https://gitee.com/archermind-ti/circularprogressindicator) - 环形进度条和滑动指示器及不同样式功能
- [ProgressView](https://gitee.com/archermind-ti/progressview) - 实现一个进度视图，目前实现了带数字进度的水平进度条以及圆形进度条，圆形进度条包括三种风格：普通环形进度，内部垂直填充进度以及内部环形填充进度
- [SwagPoints](https://gitee.com/archermind-ti/swagpoints) - 自定义圆形 SeekBar
- [WheelIndicatorView](https://gitee.com/archermind-ti/wheel-indicator-view) - 一个类似’google fit‘的ohos进度指示器
- [MaterialProgressBar](https://gitee.com/baijuncheng-open-source/MaterialProgressBar) - 长条和圆形进度条
- [progresshint](https://gitee.com/chinasoft5_ohos/progresshint) - 用代理去显示浮动进度样式
- [Ohos-PercentProgressBar-lib](https://gitee.com/chinasoft5_ohos/Ohos-PercentProgressBar-lib) - 百分比圆形进度条和百分比直线进度条
- [ PercentageChartView](https://gitee.com/archermind-ti/percentagechartview) - 一个多样式百分比进度条实现库，包括环，饼，填充式
- [Dashed Circular Progress](https://gitee.com/chinasoft5_ohos/progresshint) - 一个可在其中放置任意控件的环形动画进度条
- [RingProgressBar](https://gitee.com/chinasoft5_ohos/RingProgressBar) - 一个material design圆圈进度条，使用时可以在图片加载和文件上传中下载
- [ProgressManager](https://gitee.com/hihopeorg/ProgressManager) - 支持Glide 图片加载，文件下载，上传等进度显示
- [RangeSeekBar](https://gitee.com/hihopeorg/RangeSeekBar) - 一款美观强大的支持单向、双向范围选择、分步、垂直、高度自定义的SeekBar
- [KProgressHUD](https://gitee.com/hihopeorg/KProgressHUD) - 提供多种加载效果的库
- [RangeBar](https://gitee.com/hihopeorg/range-bar) - 类似于增强的SeekBar小部件，提供了一系列值的选择，而不是单个值
- [DilatingDotsProgressBar](https://gitee.com/hihopeorg/DilatingDotsProgressBar) - 可自定义的不确定进度条
- [FlickerProgressBar](https://gitee.com/hihopeorg/FlickerProgressBar) - 仿应用宝下载进度条
- [EasySignSeekBar](https://gitee.com/hihopeorg/EasySignSeekBar) - 一个漂亮而强大的自定义SeekBar
- [RingProgress](https://gitee.com/hihopeorg/RingProgress) - 支持自定义颜色，背景色，旋转角度，圆环角度等
- [CircularSeekBar](https://gitee.com/hihopeorg/circularseekbar) - 可拖拽圆形进度条CircleSeekBar
- [CircleProgressView](https://gitee.com/archermind-ti/circle-progress-view) - CircleProgressView是一个圆形渐变的进度动画控件（支持外环显示刻度，内环随之变化，配置参数完全可配），动画效果纵享丝滑
- [PreviewSeekBar](https://gitee.com/archermind-ti/previewseekbar) - 进度条绑定预览框组件，并且预览框显示有变形动画和渐隐渐出动画效果选择；可修改进度条颜色
- [CircleProgress](https://gitee.com/chinasoft2_ohos/CircleProgress) - 实现自定义弧形与圆形进度条，以及水波纹圆形进度
- [Progressbar](https://gitee.com/chinasoft4_ohos/Progressbar) - 实现圆形进度，水波浪进度框
- [chart-progress-bar-ohos](https://gitee.com/chinasoft4_ohos/chart-progress-bar-ohos) - 使用进度条样式绘制图表
- [Ohos-CircleProgressIndicator](https://gitee.com/chinasoft5_ohos/Ohos-CircleProgressIndicator) - 可渐变色的自定义圆环直角progressBar
- [RopeProgressBar](https://gitee.com/chinasoft5_ohos/RopeProgressBar) - 进度条在自身重量下”弯曲“
- [StoriesProgressView](https://gitee.com/chinasoft3_ohos/StoriesProgressView) - 像Instagram stories那样展示横向进度的库
- [ButtonProgressBar](https://gitee.com/chinasoft4_ohos/ButtonProgressBar) - 下载进度条，能显示下载进度以及下载完成的状态
- [ProgressStatusBar](https://gitee.com/chinasoft4_ohos/ProgressStatusBar) - 系统状态栏的进度视图，显示在系统状态栏的进度条
- [MagicProgressWidget](https://gitee.com/chinasoft4_ohos/MagicProgressWidget) - 渐变的圆形进度条与轻量横向进度条
- [unifiedcircularprogress](https://gitee.com/chinasoft4_ohos/unifiedcircularprogress) - 圆形动态进度条
- [CircularProgressBar](https://gitee.com/chinasoft5_ohos/CircularProgressBar) - 以最简单的方式实现一个圆形的进度条
- [ZzHorizontalProgressBar](https://github.com/applibgroup/ZzHorizontalProgressBar) - Highly customized horizontal progress bar control.
- [TickSeekBar](https://github.com/applibgroup/TickSeekBar) - This is a custom SeekBar library on HarmonyOS.
- [CircleSeekbar](https://github.com/applibgroup/CircleSeekbar) - An HMOS circle seekbar library
- [CircularProgressbar](https://github.com/applibgroup/CircularProgressbar) - CircularProgressbar project let you create circular progressbar in HMOS

[返回目录](#目录)

#### <a name="dialog-JAVA"></a>Dialog

- [sweet-alert-dialog](https://gitee.com/harmonyos-tpc/sweet-alert-dialog) - 一个漂亮而灵动的提醒对话框，支持succeed，error，warning等多种状态模式提示
- [LovelyDialog](https://gitee.com/harmonyos-tpc/LovelyDialog) - 自定义样式的Dialog，一组简单的对话框包装类库，旨在帮助您轻松创建精美对话框
- [CookieBar](https://gitee.com/harmonyos-tpc/CookieBar) - 顶部底部弹出的自定义对话框
- [Alerter](https://gitee.com/harmonyos-tpc/Alerter) - 带有动画效果的顶部提示弹窗
- [StatusView](https://gitee.com/harmonyos-tpc/StatusView) - 顶部弹出的状态视图
- [ohos-styled-dialogs](https://gitee.com/hihopeorg/ohos-styled-dialogs) - 自定义风格化Dialog
- [NiceDialog](https://gitee.com/chinasoft_ohos/NiceDialog) - NiceDialog基于CommonDialog的扩展，让dialog的使用更方便
- [BlurDialogFragment](https://gitee.com/baijuncheng-open-source/blur-dialog-fragment) - 模糊效果对话框
- [SnackBar_ohos](https://gitee.com/isrc_ohos/SnackBar_ohos) - 开源SnackBar消息弹框
- [michaelbel_BottomSheet](https://gitee.com/harmonyos-tpc/michaelbel_BottomSheet) - material design弹框
- [search-dialog](https://gitee.com/harmonyos-tpc/search-dialog) - 搜索Dialog
- [material-dialogs](https://gitee.com/harmonyos-tpc/material-dialogs) - Material风格Dialog
- [BottomDialog](https://gitee.com/harmonyos-tpc/BottomDialog) - 通过CommonDialog实现的底部弹窗布局，支持任意布局
- [XPopup](https://gitee.com/harmonyos-tpc/XPopup) - 功能强大，交互优雅，动画丝滑的通用弹窗
- [DialogUtil](https://gitee.com/openneusoft/dialog-util) - 各种功能样式的对话弹窗工具
- [Hover](https://gitee.com/baijuncheng-open-source/Hover) - 一个自定义的悬浮球库
- [StyledDialogs](https://gitee.com/baijuncheng-open-source/StyledDialogs) - 不同样式的Dialog
- [EasyFloat](https://gitee.com/ts_ohos/easy-float) - 浮窗
- [PowerMenu](https://gitee.com/harmonyos-tpc/PowerMenu) - 实现material弹出菜单的最强大，最简单的方法。 PowerMenu可以完全自定义，并用于弹出对话框
- [DialogV3](https://gitee.com/harmonyos-tpc/DialogV3) - 各种样式的自定义弹出框组件
- [easy-rating-dialog](https://gitee.com/harmonyos-tpc/easy-rating-dialog) - 一个简单样式的弹框组件
- [MNProgressHUD](https://gitee.com/ts_ohos/mnprogress-hud) - 一个炫酷的自定义dialog UI显示组件，支持各种自定义效果的dialog显示，包括水平进度，圆形进度，toast显示等
- [bottomsheets](https://gitee.com/hihopeorg/bottomsheets) - 底部弹出对话框
- [PopupLayout](https://gitee.com/hihopeorg/PopupLayout) - 支持从顶部，底部，顶部，顶部和中心这五个位置切换到自己指定的视图。
- [StandOut](https://gitee.com/hihopeorg/StandOut) - 应用中创建浮动窗口
- [smart-app-rate](https://gitee.com/archermind-ti/smart-app-rate) - 对应用的评分对话框，如果用户对应用程序的评级低于定义的阈值评级，则该对话框将变为反馈窗体，否则，它会将用户带到指定网页地址
- [BubblePopupWindow](https://gitee.com/archermind-ti/bubble-popup-window) - 气泡弹窗，可控制气泡尖角偏移量
- [EasyDialog](https://gitee.com/archermind-ti/easydialog) - 一个Dialog的封装库，提供了builder的方式进行调用，代码中没有任何自定义，轻量稳定
- [HintPopupWindow](https://gitee.com/archermind-ti/hintpopupwindow) - 仿QQ的选项弹窗
- [ohos-simple-tooltip](https://gitee.com/archermind-ti/ohos-simple-tooltip) - 基于PopupDialog的创建tooltip的一个简单库
- [FloatUtil](https://gitee.com/chinasoft4_ohos/FloatUtil) - 一个简单的浮窗工具，封装了浮窗的使用方法
- [Ohos-FloatWindow](https://gitee.com/chinasoft5_ohos/Ohos-FloatWindow) - 自定义悬浮窗
- [PopupMenuView](https://gitee.com/archermind-ti/popupmenuview) - 类似IOS中的圆角气泡弹窗
- [Ohos-CircleDialog](https://gitee.com/chinasoft4_ohos/Ohos-CircleDialog) - 仿IOS圆角对话框、进度条、列表框、输入框，ad广告框，支持横竖屏切换
- [DropDownMenu](https://gitee.com/hihopeorg/DropDownMenu) - 筛选器，适配各种数据model
- [BottomSheet](https://gitee.com/hihopeorg/BottomSheet) - BottomSheet组件主要展示的是底部弹框的效果，即从屏幕底部边缘向上滑动的弹框。底页提供了无需解释的清晰和简单动作的显示弹框。
- [BottomDialog](https://gitee.com/hihopeorg/BottomDialog) - 实现底部对话框布局，支持任意布局
- [RelativePopupWindow](https://gitee.com/hihopeorg/RelativePopupWindow) - 相对某个控件位置的PopupWindow
- [BottomSheet](https://gitee.com/hihopeorg/ohos-BottomSheet) - 底部弹出ListView、GridView形式的带有分割线的弹框
- [TDialog](https://gitee.com/hihopeorg/TDialog) - 提供封装好的dialog框架使用工具库
- [OhosRateThisApp](https://gitee.com/chinasoft4_ohos/OhosRateThisApp) - ohos库显示“为该应用评分”对话框
- [FlyoutMenus](https://gitee.com/chinasoft5_ohos/FlyoutMenus) - 一个简单的弹出式菜单
- [PopupList](https://gitee.com/chinasoft5_ohos/PopupList) - PopupList基于PopupDialog的扩展
- [MaterialDialog-Ohos](https://gitee.com/chinasoft4_ohos/MaterialDialog-Ohos) - 轻松创建丰富、动画、漂亮的对话框
- [BubbleView](https://gitee.com/chinasoft4_ohos/BubbleView) - 气泡样式，可运用到文本、图片、布局
- [TimePickerDialog](https://gitee.com/chinasoft5_ohos/TimePickerDialog) - 一个Ohos版时间选择器库

[返回目录](#目录)

#### <a name="layout-JAVA"></a>Layout
- [bottomNavigationF](https://gitee.com/blueskyliu/BottomNavigationF) - 这个组件提供类似flutter的scaffold小部件的解决方案解决具有生命周期的底部导航
- [vlayout](https://gitee.com/harmonyos-tpc/vlayout) - 可以嵌套列表布局
- [flexbox-layout](https://gitee.com/harmonyos-tpc/flexbox-layout) - 按照百分比控制的布局
- [ohosAutoLayout](https://gitee.com/harmonyos-tpc/ohosAutoLayout) - 可根据设计尺寸按比例缩放的屏幕适配框架
- [yoga](https://gitee.com/harmonyos-tpc/yoga) - facebook基于flexbox的布局引擎
- [TextLayoutBuilder](https://gitee.com/hihopeorg/TextLayoutBuilder) - facebook的一款textlayout组件，支持文本的创建、文本字体、大小、颜色设置等
- [FlowLayout](https://gitee.com/hihopeorg/FlowLayout) - 流式布局实现
- [ShadowLayout](https://gitee.com/chinasoft_ohos/ShadowLayout) - 带阴影效果的自定义layout
- [ExpandableLayout](https://gitee.com/chinasoft_ohos/ExpandableLayout) - 可折叠展开的layout
- [LayoutManagerGroup](https://gitee.com/harmonyos-tpc/LayoutManagerGroup) - 负责测量和放置RecyclerView中的项目视图
- [Flipboard/bottomsheet](https://gitee.com/harmonyos-tpc/bottomsheet) - 从屏幕底部显示可忽略的View
- [ohos-flowlayout](https://gitee.com/harmonyos-tpc/ohos-flowlayout) - 流布局
- [ExpandableLayout](https://gitee.com/harmonyos-tpc/ExpandableLayout) - 可动画扩展折叠子view布局
- [CarouselLayoutManager](https://gitee.com/chinasoft_ohos/CarouselLayoutManager) - 支持点击快速定位，快速滑动，点击显示当前item的下标
- [shadow-layout](https://gitee.com/chinasoft2_ohos/shadow-layout) - 可以设置图片以及按钮的阴影效果
- [loadinglayout](https://gitee.com/chinasoft_ohos/loadinglayout) - 简单实用的页面多状态布局(content,loading,empty,error)
- [material-about-library](https://gitee.com/chinasoft_ohos/material-about-library) - material-about-library库包含了多种样式的选项条，多用于“关于”页面
- [MultiViewAdapter](https://gitee.com/chinasoft2_ohos/MultiViewAdapter) - 所有的布局用一个适配器去实现
- [DiagonalLayout](https://gitee.com/chinasoft3_ohos/DiagonalLayout) - 利用对角线布局，实现新的设计风格
- [ohos-card-form](https://gitee.com/chinasoft2_ohos/ohos-card-form) - 表单是一个现成的卡形式布局
- [ohos-titlebar](https://gitee.com/chinasoft2_ohos/ohos-titlebar) - 抛弃在开发过程中，因页面过多，需要构建大量重复的标题栏布局。本项目总结了几种常用的使用场景，将标题栏封装成控件，Java代码实现，对当前主流的沉浸式提供了支持
- [HtmlNative](https://gitee.com/hihopeorg/HtmlNative) - 使用HTML / CSS渲染ohos View，使用Lua来控制其逻辑（不是Webview）
- [SwipeBackLayout](https://gitee.com/baijuncheng-open-source/SwipeBackLayout) - 侧滑返回上一页
- [ToggleButtonGroup](https://gitee.com/baijuncheng-open-source/ToggleButtonGroup) - 一组简易的单选和多选按钮工具
- [FlexLayout](https://gitee.com/baijuncheng-open-source/flexlayout) - 百分比布局
- [KugouLayout](https://gitee.com/baijuncheng-open-source/kugou-layout) - 页面滑动控制
- [ScalableLayout](https://gitee.com/baijuncheng-open-source/ScalableLayout) - 可拓展布局
- [RearrangeableLayout](https://gitee.com/baijuncheng-open-source/rearrangeable-layout) - 子控件任意拖动
- [ZoomLayout](https://gitee.com/ts_ohos/zoom-layout) - 可滑动的效果
- [FoldableLayout](https://gitee.com/harmonyos-tpc/FoldableLayout) - 3D翻转
- [ohos-drag-FlowLayout](https://gitee.com/harmonyos-tpc/ohos-drag-FlowLayout) - 可拖拽layout
- [ohosTagGroup](https://gitee.com/harmonyos-tpc/ohosTagGroup) - 自定义标签组件
- [FoldableLayout](https://gitee.com/harmonyos-tpc/worldline_FoldableLayout) - 3D翻转列表菜单选项
- [ohosChangeSkin](https://gitee.com/harmonyos-tpc/ohosChangeSkin) - 皮肤背景色修改控件
- [MessageView](https://gitee.com/harmonyos-tpc/MessageView) - 自定义聊天对话框布局
- [LondonEyeLayoutManager](https://gitee.com/harmonyos-tpc/LondonEyeLayoutManager) - 与RecyclerComponent一起使用的LayoutManager。滚动列表时，组件按圆形轨迹移动
- [ohosFlowLayout](https://gitee.com/harmonyos-tpc/ohosFlowLayout) - 一个实用的流式布局
- [Search_Layout](https://gitee.com/ts_ohos/Search_Layout) - 一个标准的搜索布局
- [TitleBar](https://gitee.com/ts_ohos/TitleBar) - 一个标题栏框架。
- [ohos-FlipView](https://gitee.com/ts_ohos/ohos-FlipView) - 带翻转动画的自定义布局切换。
- [Shuffle](https://gitee.com/ts_ohos/Shuffle) - 自定义横扫布局。
- [FantasySlide](https://gitee.com/ts_ohos/fantasyslide) - 侧边划出菜单栏。
- [cult](https://gitee.com/ts_ohos/cult) - Cult将为你的工具栏提供一个新的布局。这允许您使用自定义的SearchView动画。
- [ImageLayout](https://gitee.com/ts_ohos/image-layout) - 一个自定义图片标点组件。
- [SwipeCardRecyclerView](https://gitee.com/ts_ohos/swipe-card-recycler-view) - 卡片手势划出布局。
- [RevealLayout](https://gitee.com/ts_ohos/revealLayout) - 揭示动画切换。
- [ShadowLayout](https://gitee.com/ts_ohos/shadowLayout) - 带阴影的布局。
- [ohos-sliding-layer-lib](https://gitee.com/ts_ohos/ohos-sliding-layer-lib) - 高度自定义的侧滑布局。
- [ProgressLayout](https://gitee.com/archermind-ti/progresslayout) - 具有进度条效果的自定义布局
- [CreditCardView](https://gitee.com/archermind-ti/credit-card-view) - 信用卡样式组件布局
- [ParallaxLayerLayout](https://gitee.com/archermind-ti/parallax-layer-layout) - 视图分层视差效果库，允许您根据设备旋转向视图或图像添加分层视差效果
- [RaiflatButton](https://gitee.com/archermind-ti/raiflat-button) - 提供一个立体或者扁平风格样式，并实现带有点击水波纹效果的组件
- [DragSlopLayout](https://gitee.com/archermind-ti/dragsloplayout) - 一个提供固定范围拖拽、动画、模糊等效果的布局
- [OhosPileLayout](https://gitee.com/chinasoft4_ohos/OhosPileLayout) - 水平列表叠放的布局
- [EmojiRain](https://gitee.com/chinasoft5_ohos/EmojiRain) - 仿微信表情雨的自定义布局
- [ELinkageScroll](https://gitee.com/chinasoft5_ohos/ELinkageScroll) - 多子控件嵌套滚动通用解决方案
- [BubbleAnimationLayout](https://gitee.com/archermind-ti/bubbleanimationlayout) - 泡动画布局，该组件非常实用，适用于各种应用程序。让您的应用程序的 UI 脱颖而出
- [TimeLine](https://gitee.com/chinasoft4_ohos/TimeLine) - 提供了左右滑动功能，并支持回弹效果和自定义设置滑动距离和是否开启滑动的自定义布局
- [3dTagCloudOhos](https://gitee.com/chinasoft4_ohos/threedTagCloudOhos) - 支持将一组View展示为一个3D球形集合，并支持全方向滚动的自定义布局
- [FlowTag](https://gitee.com/ts_ohos/FlowTag) - 流式布局，支持点击、单选、多选等，适合用于产品标签等，用法采用Adapter模式，和ListContainer用法一样。新添加初始化标签功能
- [TwinklingRefreshLayout](https://gitee.com/chinasoft4_ohos/TwinklingRefreshLayout) - TwinklingRefreshLayout延伸了SwipeRefreshLayout的思想,不在列表控件上动刀,而是使用一个ViewGroup来包含列表控件,以保持其较低的耦合性和较高的通用性
- [EasyRefreshLayout](https://gitee.com/chinasoft5_ohos/EasyRefreshLayout) - 列表界面的下拉刷新和上拉加载更多
- [SwipyRefreshLayout](https://gitee.com/chinasoft5_ohos/SwipyRefreshLayout) - 允许双向滑动的 SwipeRefreshLayout 扩展
- [Smart-HeaderFooter-RecyclerView](https://gitee.com/chinasoft5_ohos/Smart-HeaderFooter-RecyclerView) - SmartHeaderFooterRecyclerView非常方便的实现Recyclerview添加HeaderView和FooterView
- [XRefreshView](https://gitee.com/hihopeorg/XRefreshView) - 刷新加载框架
- [Transferee](https://gitee.com/hihopeorg/transferee) - transferee 可以帮助你完成从缩略视图到原视图的无缝过渡转变, 优雅的浏览普通图片、长图、gif图、视频等不同格式的多媒体
- [ArcLayout](https://gitee.com/hihopeorg/ArcLayout) - 弧形容器
- [ohos-percent-support-extend](https://gitee.com/hihopeorg/ohos-percent-support-extend) - 百分比布局库的扩展
- [FlowLayout](https://gitee.com/hihopeorg/blazsolar-FlowLayout) - 提供一套流式布局实现
- [FlowingDrawer](https://gitee.com/hihopeorg/FlowingDrawer) - 可左右滑动的抽屉容器
- [AlphabetIndex-Fast-Scroll-RecyclerView](https://gitee.com/hihopeorg/AlphabetIndex-Fast-Scroll-RecyclerView) - 用于快速滚动的字母检索表
- [folding-cell](https://gitee.com/hihopeorg/ohos-folding-cell) - 提供一套自定义ComponentContainer，可支持子控件的折叠与展开效果
- [ResideMenu](https://gitee.com/hihopeorg/ohos-ResideMenu) - 可手势滑动开启/关闭菜单
- [JellyRefreshLayout](https://gitee.com/hihopeorg/JellyRefreshLayout) - 一个下拉刷新布局效果
- [SwipePanel](https://gitee.com/hihopeorg/SwipePanel) - 支持界面侧滑返回的视图控件
- [CompoundLayout](https://gitee.com/hihopeorg/CompoundLayout) - 使用布局作为RadioButton或CheckBox
- [PullZoomRecyclerView](https://gitee.com/hihopeorg/PullZoomRecyclerView) -  支持headview自定义、headview缩放
- [LoadDataLayout](https://gitee.com/archermind-ti/loaddatalayout) -  App公共组件：加载数据Layout，高效开发必备、
- [WaveSwipeRefreshLayout](https://gitee.com/archermind-ti/WaveSwipeRefreshLayout) - 提供可重复使用的水波纹下拉刷新小部件
- [Slice](https://gitee.com/chinasoft3_ohos/Slice) - 类似CardView效果的自定义控件
- [Ohos-ActionItemBadge](https://gitee.com/chinasoft5_ohos/Ohos-ActionItemBadge) - 简便易用的徽标
- [ohos-tagview](https://gitee.com/chinasoft4_ohos/ohos-tagview) - 标签小部件， 您可以编辑标签的样式，并设置选择或删除标签的监听器
- [ohos-palette](https://gitee.com/chinasoft4_ohos/ohos-palette) - 简易的画板
- [swipe-maker](https://gitee.com/chinasoft4_ohos/swipe-maker) - 一款可以帮助你的控件快速实现各种滑动事件的View类库
- [ShapeView](https://gitee.com/chinasoft4_ohos/ShapeView) - View剪裁不同形状
- [DropDownLayout](https://gitee.com/chinasoft4_ohos/DropDownLayout) - 下拉导航菜单
- [SortableTableView](https://gitee.com/chinasoft3_ohos/SortableTableView) - 支持按列排序的列表组件
- [MaterialScrollBar](https://gitee.com/chinasoft4_ohos/MaterialScrollBar) - 易于使用的ohos库，可轻松实现滚动跟随效果
- [chips-input-layout](https://gitee.com/chinasoft4_ohos/chips-input-layout) - 允许用户在输入的时候过滤chip
- [ShadowLayout](https://gitee.com/chinasoft4_ohos/ShadowLayout) - 此库允许您基于子对象为布局创建阴影效果
- [ScalingLayout](https://gitee.com/chinasoft4_ohos/ScalingLayout) - 使用缩放布局在用户交互中缩放您的布局

[返回目录](#目录)

#### <a name="tab-菜单切换JAVA"></a>Tab-菜单切换

- [FlycoTabLayout](https://gitee.com/harmonyos-tpc/FlycoTabLayout) - 自定义TabLayout组件，支持三种模式多种状态设置。
- [NavigationTabBar](https://gitee.com/harmonyos-tpc/NavigationTabBar) - 各种样式TabBar合集
- [BottomBar](https://gitee.com/harmonyos-tpc/BottomBar) - 自定义底部菜单栏
- [BottomNavigation](https://gitee.com/harmonyos-tpc/BottomNavigation) - 支持多种样式自定义底部菜单栏，此库可帮助用户轻松使用底部导航栏（来自 google 的新模式）并允许进行大量自定义
- [ahbottomnavigation](https://gitee.com/harmonyos-tpc/ahbottomnavigation) - 一个从 Material Design 中重现底部导航的库。
- [SHSegmentControl](https://gitee.com/chinasoft_ohos/SHSegmentControl) - 自定义菜单控件
- [BottomNavigationViewEx](https://gitee.com/harmonyos-tpc/BottomNavigationViewEx) - 自定义底部导航栏
- [SHSegmentControl](https://gitee.com/chinasoft_ohos/SHSegmentControl) - 分段器自定义UI组件
- [AdvancedPagerSlidingTabStrip](https://gitee.com/chinasoft2_ohos/AdvancedPagerSlidingTabStrip) - 漂亮的自定义导航控件
- [MaterialNavigationDrawer](https://gitee.com/chinasoft2_ohos/MaterialNavigationDrawer) - 具有材料设计风格和简化方法的导航抽屉栏
- [ChromeLikeTabSwitcher](https://gitee.com/chinasoft3_ohos/ChromeLikeTabSwitcher) - ChromeLikeTabSwitcher是一个仿Chrome浏览器中Tab切换的库
- [bubble-navigation](https://gitee.com/chinasoft3_ohos/bubble-navigation) - 轻量级的自定义导航栏组件
- [FABRevealMenu](https://gitee.com/chinasoft3_ohos/FABRevealMenu) - 悬浮按钮自定义弹出菜单
- [Floating-Navigation-View](https://gitee.com/chinasoft2_ohos/Floating-Navigation-View) - 一个简单的浮动操作按钮，显示锚定导航视图
- [BoomMenu](https://gitee.com/openneusoft/boom-menu) - BoomMenu是一个爆炸式显示类component，可定制显示个数，位置等，可独自显示，也可以添加到component容器中(List等)使用
- [segmented_control](https://gitee.com/baijuncheng-open-source/segmented_control) - 鸿蒙版本的分段控制器
- [WearMenu](https://gitee.com/baijuncheng-open-source/WearMenu) - 手表的菜单
- [PagerBottomTabStrip](https://gitee.com/harmonyos-tpc/PagerBottomTabStrip) - 底部和侧边的导航栏
- [GuillotineMenu-ohos](https://gitee.com/harmonyos-tpc/GuillotineMenu-ohos) - 自定义类似于闸刀样式的菜单
- [SmileyRating](https://gitee.com/harmonyos-tpc/SmileyRating) - 5种表情的菜单选项
- [TapBarMenu](https://gitee.com/ts_ohos/TapBarMenu) - 底部可展开式菜单样式
- [LuseenBottomNavigation](https://gitee.com/ts_ohos/LuseenBottomNavigation) - 根据谷歌指南设计的底部菜单
- [Ohostoggleswitch](https://gitee.com/ts_ohos/ohostoggleswitch) - 支持多个选项的开关按钮
- [SegmentedButton](https://gitee.com/hihopeorg/SegmentedButton) - 多菜单选项选择
- [FloatingToolbar](https://gitee.com/hihopeorg/FloatingToolbar) - 从浮动操作按钮中变出的工具栏
- [MaterialNavigationDrawer](https://gitee.com/archermind-ti/materialnavigationdrawer) - 具有材料设计风格和简化方法的导航抽屉栏
- [snake-menu](https://gitee.com/archermind-ti/snake-menu) - 具有跟随手势拖动的“贪吃蛇”动画的浮动菜单
- [OHOSIndicator](https://gitee.com/archermind-ti/ohos-indicators) - 具有动画效果的Tab标题栏
- [OhosNiceTab](https://gitee.com/archermind-ti/ohosnicetab) - 自定义table选择器控件
- [Folder-ResideMenu](https://gitee.com/archermind-ti/folder-residemenu) - 侧滑菜单抽屉库
- [material-drawer](https://gitee.com/archermind-ti/material-drawer) - Material design模式下的自定义抽屉实现
- [FABToolbar](https://gitee.com/chinasoft5_ohos/FABToolbar) - 浮动按钮可点开为菜单
- [ Ohos-CircleMenu](https://gitee.com/chinasoft5_ohos/Ohos-CircleMenu) - 圆形旋转菜单，支持跟随手指旋转以及快速旋转
- [NavigationDrawer-MaterialDesign](https://gitee.com/chinasoft5_ohos/NavigationDrawer-MaterialDesign) - 自定义抽屉布局
- [ButtonMenu](https://gitee.com/archermind-ti/buttonmenu) - 底部导航菜单
- [RecyclerTabLayout](https://gitee.com/chinasoft4_ohos/RecyclerTabLayout) - 使用ListContainer实现的高效的Tab菜单库
- [SpaceTabLayout](https://gitee.com/chinasoft5_ohos/SpaceTabLayout) - 一个Tab菜单库
- [MagicIndicator](https://gitee.com/chinasoft4_ohos/MagicIndicator) - 可定制的和可扩展的Tab菜单库
- [BottomNavigation](https://gitee.com/hihopeorg/BottomNavigation) - 底部导航栏，动态添底部导航栏选项，两种不同的切换风格，支持两种类型item角标，文本类型，图形类型
- [LBehavior](https://gitee.com/hihopeorg/Lbehavior) - 标题栏，底部栏等控件滑动动画的简单实现
- [ThumbnailMenu](https://gitee.com/hihopeorg/thumbnailmenu) - 一个简单而精致的 Fraction 菜单控件，它可以让你切换 Fraction 的时候不再单调、死板
- [FloatMenuSample](https://gitee.com/hihopeorg/FloatMenuSample) - 提供支持浮动显示的菜单栏控件
- [BubbleTab](https://gitee.com/chinasoft_ohos/BubbleTab) - 提供一个气泡风格的tab栏
- [Ohos-MultiBackStack](https://gitee.com/chinasoft4_ohos/Ohos-MultiBackStack) - 底部状态栏切换效果
- [LottieBottomNav](https://gitee.com/chinasoft4_ohos/LottieBottomNav) - 创建使用 Lottie 视图的底部导航视图的库

[返回目录](#目录)

#### <a name="toast-JAVA"></a>Toast

- [Toasty](https://gitee.com/harmonyos-tpc/Toasty) - 简单好用的Toast调用工具
- [FancyToast-ohos](https://gitee.com/harmonyos-tpc/FancyToast-ohos) - Toast常用样式的简单封装
- [TastyToast](https://gitee.com/hihopeorg/TastyToast) - 自定义Toast控件
- [StyleableToast](https://gitee.com/chinasoft_ohos/StyleableToast) - 通过代码或使用中的样式来样式化toasts
- [GlideToast](https://gitee.com/harmonyos-tpc/GlideToast) - 用于构建飞行动画toast消息
- [Toastbar](https://gitee.com/archermind-ti/toastbar) - 一个Material 风格的顶部和底部弹出的Toast

[返回目录](#目录)

#### <a name="time-date-JAVA"></a>Time-Date

- [ohos-times-square](https://gitee.com/harmonyos-tpc/ohos-times-square) - 简单的日历组件
- [CountdownView](https://gitee.com/harmonyos-tpc/CountdownView) - 多种效果的时间计时器
- [MaterialDateRangePicker](https://gitee.com/baijuncheng-open-source/material-date-range-picker) - Material风格的时间选择
- [circleTimer](https://gitee.com/baijuncheng-open-source/circle-timer) - 一个简单的带动画效果的钟表样式的倒计时器
- [SublimePicker](https://gitee.com/baijuncheng-open-source/sublime-picker) - 用于时间选择的自定义控件：提供选择日期(年、月、日)，提供选择时间(时、分、秒)，提供可选择日期的重复选项等功能
- [ohos-betterpickers](https://gitee.com/hihopeorg/ohos-betterpickers) - 日历、时间、市区等UI Dialog 弹框框架，提供可定制主题的日历选择器
- [CalendarView](https://gitee.com/archermind-ti/calendarview) - 自定义日历组件
- [henry-Calendarview](https://gitee.com/archermind-ti/henry-calendarview) - 一个高度定制的日期选择器，可以满足多选日期的需求
- [linear-time-picker](https://gitee.com/archermind-ti/linear-time-picker) - 一个时间和日期选择器组件，拥有独特而直观的时间和日期选择器，自动为用户提供动画教程
- [mCalendarView](https://gitee.com/chinasoft5_ohos/mCalendarView) - 一个简单的日历组件
- [CountDownView](https://gitee.com/chinasoft4_ohos/CountDownView) - 倒计时计时器控件
- [CalendarListView](https://gitee.com/chinasoft4_ohos/CountDownView) - 日历组件，可伸缩扩展，列表可上拉下沉， 日历的选择会让ListContainer滑动到指定的位置，ListContainer的滑动同时也会带动日历滑到指定位置并能同时自动切换月份
- [PickView](https://gitee.com/chinasoft5_ohos/PickView) - 一个展示日期及时间滚轮显示效果的功能库
- [NumberPickerView](https://gitee.com/chinasoft5_ohos/NumberPickerView) - 上下滚动数字选择器
- [CalendarView](https://gitee.com/hihopeorg/CalendarView) - 一个优雅、万能自定义UI、支持垂直、水平方向切换、支持周视图、自定义周起始、性能高效的日历控件
- [CountdownView](https://gitee.com/hihopeorg/CountdownView) - 倒计时控件，使用Canvas绘制，支持多种样式
- [ohos-collapse-calendar-view](https://gitee.com/hihopeorg/ohos-collapse-calendar-view) - 日历月视图和周视图的切换UI效果
- [ohos-AlarmManagerClock](https://gitee.com/hihopeorg/ohos-AlarmManagerClock) - 闹钟功能，支持选择响铃和震动
- [datepicker-timeline](https://gitee.com/hihopeorg/datepicker-timeline) -  datepicker-timeline是一个可以月日联动的无限滚动时间轴
- [CircleAlarmTimerView](https://gitee.com/hihopeorg/CircleAlarmTimerView) -  圆形闹钟定时器
- [TimeRangePicker](https://gitee.com/hihopeorg/TimeRangePicker) -  时间范围选择,提供了一个带有两个选项卡的对话框，第一个
- [Ohos-SwitchDateTimePicker](https://gitee.com/chinasoft4_ohos/Ohos-SwitchDateTimePicker) - 用于在同一 UI 中使用 DatePicker（日历）和 TimePicker（时钟）在对话框中选择日期对象
- [DatePickerTimeline](https://gitee.com/chinasoft4_ohos/DatePickerTimeline) - 一个可水平滑动的日期选择控件
- [CompactCalendarView](https://gitee.com/chinasoft5_ohos/CompactCalendarView) - CompactCalendarView是一个简单的日历视图
- [LazyDatePicker](https://gitee.com/chinasoft4_ohos/LazyDatePicker) - 日期选择器
- [Custom-Calendar-View](https://github.com/applibgroup/Custom-Calendar-View) - Custom-Calendar-View for Harmony OS
- [TimetableView](https://gitee.com/chinasoft_ohos/TimetableView) - 是一款开源的、完善、高效的课程表控件
- :tw-1f195: [FlexibleCalendar](https://github.com/applibgroup/FlexibleCalendar) - A customizable calendar with customizable events.
- :tw-1f195: [MaterialCalendar](https://github.com/applibgroup/MaterialCalendar) - A Material design calendar inspired by the CalendarView of School Diary

[返回目录](#目录)

#### <a name="其他ui-自定义控件JAVA"></a>其他UI-自定义控件
- [BGARefreshLayout-ohos](https://gitee.com/harmonyos-tpc/BGARefreshLayout-ohos) - 基于多个场景的下拉刷新
- [FunGameRefresh](https://gitee.com/harmonyos-tpc/FunGameRefresh) - 一款可以打游戏的下拉刷新控件
- [ohos-Bootstrap](https://gitee.com/harmonyos-tpc/ohos-Bootstrap) - 多种自定义控件合集
- [ohosSlidingUpPanel](https://gitee.com/harmonyos-tpc/ohosSlidingUpPanel) - 底部上滑布局
- [Fragmentation](https://gitee.com/harmonyos-tpc/Fragmentation) - 侧边菜单
- [triangle-view](https://gitee.com/harmonyos-tpc/triangle-view) - 三角图
- [MaterialDesignLibrary](https://gitee.com/harmonyos-tpc/MaterialDesignLibrary) - 一系列包含ProgressBar,CheckBox,Button等基础组件的materiaDesign风格的自定义集合框架
- [cardslib](https://gitee.com/harmonyos-tpc/cardslib) - 卡片式布局库
- [Swipecards](https://gitee.com/harmonyos-tpc/Swipecards) - 滑动卡片组件
- [SlideUp-ohos](https://gitee.com/harmonyos-tpc/SlideUp-ohos) - 从下方滑动出来的布局控件
- [EazeGraph](https://gitee.com/harmonyos-tpc/EazeGraph) - 柱状图圆形图山峰图
- [WheelView](https://gitee.com/harmonyos-tpc/WheelView) - 轮盘选择
- [RulerView](https://gitee.com/harmonyos-tpc/RulerView) - 卷尺控件
- [MultiCardMenu](https://gitee.com/harmonyos-tpc/MultiCardMenu) - 底部弹出的自定义菜单集合
- [DividerDrawable](https://gitee.com/harmonyos-tpc/DividerDrawable) - 分割线绘制
- [ProtractorView](https://gitee.com/harmonyos-tpc/ProtractorView) - 量角器控件
- [ohos-ExpandIcon](https://gitee.com/harmonyos-tpc/ohos-ExpandIcon) - 箭头控件
- [GestureLock](https://gitee.com/harmonyos-tpc/GestureLock) - 可自定义配置的手势动画解锁的库，支持多种样式大小自由设置
- [williamchart](https://gitee.com/harmonyos-tpc/williamchart) - 柱状图圆形图进度图山峰图
- [labelview](https://gitee.com/harmonyos-tpc/labelview) - 自定义角标图
- [PatternLockView](https://gitee.com/harmonyos-tpc/PatternLockView) - 自定义屏幕图案手势解锁控件
- [BadgeView](https://gitee.com/harmonyos-tpc/BadgeView) - 图标的标签图
- [MaterialBadgeTextView](https://gitee.com/harmonyos-tpc/MaterialBadgeTextView) - 自定义Text实现带有插入数字的彩色圆圈，该圆圈显示在图标的右上角，通常在IM应用程序中显示新消息或新功能的作用
- [SlantedTextView](https://gitee.com/harmonyos-tpc/SlantedTextView) - 一个倾斜的text,适用于标签效果
- [TriangleLabelView](https://gitee.com/harmonyos-tpc/TriangleLabelView) - 三角形角标图
- [GoodView](https://gitee.com/harmonyos-tpc/GoodView) - 点赞+1效果的按钮，支持文本和图像
- [StateViews](https://gitee.com/harmonyos-tpc/StateViews) - 展示加载中，加载成功，加载失败以及支持自定义状态的控件
- [WaveView](https://gitee.com/harmonyos-tpc/WaveView) - 可自定义振幅，频率，颜色等属性的波浪进度条控件
- [CircleRefreshLayout](https://gitee.com/harmonyos-tpc/CircleRefreshLayout) - 自定义下拉刷新组件，包含有趣的动画
- [TextDrawable](https://gitee.com/harmonyos-tpc/TextDrawable) - 带有字母/文字的drawable
- [OhosMaterialViews](https://gitee.com/hihopeorg/ohos-material-views) - Material风格控件
- [baseAdapter](https://gitee.com/hihopeorg/baseAdapter) - ListView,RecyclerView,GridView适配器
- [Materialize](https://gitee.com/hihopeorg/Materialize) - Materia Design风格的主题库
- [FastAdapter](https://gitee.com/hihopeorg/FastAdapter) - 快速简化适配器
- [GestureViews](https://gitee.com/hihopeorg/GestureViews) - 带有手势控制和位置动画的ImageView和FrameLayout
- [GroupedRecyclerViewAdapter](https://gitee.com/hihopeorg/GroupedRecyclerViewAdapter) - RecyclerView适配器
- [ImmersionBar](https://gitee.com/hihopeorg/ImmersionBar) - 沉浸式状态栏导航栏实现
- [material](https://gitee.com/hihopeorg/material) - Material风格的UI控件库
- [MaterialDateTimePicker](https://gitee.com/hihopeorg/MaterialDateTimePicker) - Material风格的时间选择器
- [material-design-icons](https://gitee.com/hihopeorg/material-design-icons) - 提供material-design-icons图片资源
- [PanelSwitchHelper](https://gitee.com/hihopeorg/PanelSwitchHelper) - 输入法与面板流畅切换
- [SwipeBackLayout](https://gitee.com/hihopeorg/SwipeBackLayout) - 帮助构建带有向后滑动手势的应用程序
- [SwipeRevealLayout](https://gitee.com/hihopeorg/SwipeRevealLayout) - 上下左右滑动布局
- [EasyFlipView](https://gitee.com/hihopeorg/EasyFlipView) - 可以设定反转动画的自定义控件
- [JKeyboardPanelSwitch](https://gitee.com/hihopeorg/JKeyboardPanelSwitch) - 键盘面板冲突 布局闪动处理方案
- [MarqueeViewLibrary](https://gitee.com/hihopeorg/MarqueeViewLibrary) - 一个方便使用和扩展的跑马灯库
- [nice-spinner](https://gitee.com/hihopeorg/nice-spinner) - 简单好用的下拉框组件
- [PullZoomView](https://gitee.com/hihopeorg/PullZoomView) - 支持下拉顶部图片放大
- [WaveView](https://gitee.com/hihopeorg/WaveView) - 水波纹动画
- [search](https://gitee.com/hihopeorg/Search) - Material Design风格的搜索组件
- [ohos-hellocharts](https://gitee.com/hihopeorg/ohos-hellocharts) - 各种表格数据统计UI控件
- [TicketView](https://gitee.com/hihopeorg/TicketView) - 类似于观影二维码的票据视图
- [ohos-StepsView](https://gitee.com/hihopeorg/ohos-StepsView) - 显示步骤执行的自定义控件
- [OXChart](https://gitee.com/hihopeorg/OXChart) - 自定义图表库
- [Captcha](https://gitee.com/chinasoft_ohos/Captcha) - 图片滑块解锁控件
- [LeafChart](https://gitee.com/chinasoft_ohos/LeafChart) - 支持折现、柱状的图表库
- [MessageBubbleView](https://gitee.com/chinasoft_ohos/MessageBubbleView) - 仿QQ未读消息气泡，可拖动删除
- [SuperLike](https://gitee.com/chinasoft_ohos/SuperLike) - 表情点赞功能
- [ohos_maskable_layout](https://gitee.com/chinasoft_ohos/ohos_maskable_layout) - 自定义component遮罩动画
- [Lighter](https://gitee.com/chinasoft_ohos/Lighter) - Lighter是一个首次进入页面的按钮提示功能库
- [E-signature](https://gitee.com/archermind-ti/E-signature) - 电子签名控件，支持签名边缘裁剪,根据速度进行了插值改变宽度
- [RippleView](https://gitee.com/chinasoft_ohos/RippleView) - 点击拥有水波涟漪效果动画的控件
- [StickyScrollView](https://gitee.com/chinasoft_ohos/StickyScrollView) - 支持多种样式的ScrollView控件
- [SlidingMenu_ohos](https://gitee.com/isrc_ohos/sliding-menu_ohos) - 滑动菜单
- [Ultra-Pull-To-Refresh_ohos](https://gitee.com/isrc_ohos/ultra-pull-to-refresh_ohos) - 通用下拉刷新组件
- [MPChart_ohos](https://gitee.com/isrc_ohos/mp-chart_ohos) - 图表绘制组件
- [lock-screen](https://gitee.com/harmonyos-tpc/lock-screen) - 简单漂亮的锁屏库
- [Graphview](https://gitee.com/harmonyos-tpc/Graphview) - ohos图表库,用于创建可视化分析的线图和条形图
- [Gloading](https://gitee.com/harmonyos-tpc/Gloading) - 将应用中全局的Loading控件与页面解耦，默认提供5种加载状态（加载中、加载失败、空数据、加载成功，无网络），支持自定义其它状态
- [TimetableView](https://gitee.com/harmonyos-tpc/TimetableView) - 一款开源、完善、高效的课程表控件，支持添加广告、课程重叠自动处理、透明背景设置、空白格子点击事件处理等丰富的功能
- [ohos-shapeLoadingView](https://gitee.com/harmonyos-tpc/ohos-shapeLoadingView) - 仿58同城的Loading控件和Loading弹窗
- [polygonsview](https://gitee.com/harmonyos-tpc/polygonsview) - 五边形蜘蛛网百分比库
- [MultipleStatusView](https://gitee.com/harmonyos-tpc/MultipleStatusView) - 一个支持多种状态的自定义View,可以方便的切换到：加载中视图、错误视图、空数据视图、网络异常视图、内容视图
- [SlideshowToolbar](https://gitee.com/harmonyos-tpc/SlideshowToolbar) -一款支持状态栏联动动画效果，用于播放幻灯片图片的加载组件
- [ShowcaseView](https://gitee.com/harmonyos-tpc/ShowcaseView) - 引导页
- [SlidingLayout](https://gitee.com/harmonyos-tpc/SlidingLayout) - 下拉上拉弹跳的果冻效果
- [AnimatedCircleLoadingView](https://gitee.com/harmonyos-tpc/AnimatedCircleLoadingView) - 确定/不确定的加载视图动画
- [SwipeBack](https://gitee.com/harmonyos-tpc/SwipeBack) - 手势关闭页面
- [DiscreteSlider](https://gitee.com/harmonyos-tpc/DiscreteSlider) - 自定义标签滑块
- [CustomWaterView](https://gitee.com/harmonyos-tpc/CustomWaterView) - 自定义仿支付宝蚂蚁森林能量控件
- [WheelPicker](https://gitee.com/harmonyos-tpc/WheelPicker) - 滚轮选择器
- [EasySwipeMenuLayout](https://gitee.com/harmonyos-tpc/EasySwipeMenuLayout) - 滑动菜单库
- [floatingsearchview](https://gitee.com/harmonyos-tpc/floatingsearchview) - 浮动搜索View
- [FlycoRoundView](https://gitee.com/harmonyos-tpc/FlycoRoundView) - 设置圆形矩形背景
- [Ratingbar](https://gitee.com/harmonyos-tpc/Ratingbar) - 自定义星级/等级
- [ohos-validation-komensky](https://gitee.com/harmonyos-tpc/ohos-validation-komensky) - 使用批注验证表单中的用户输入
- [SystemBarTint](https://gitee.com/harmonyos-tpc/SystemBarTint) - 将背景色应用于系统
- [Leonids](https://gitee.com/harmonyos-tpc/Leonids) - 粒子效果库
- [CircleView](https://gitee.com/harmonyos-tpc/CircleView) - 包含标题和副标题的圆形View
- [PercentageChartView](https://gitee.com/harmonyos-tpc/PercentageChartView) - 自定义百分比ChartView
- [DatePicker](https://gitee.com/harmonyos-tpc/DatePicker) - 日期选择器
- [SwipeCardView](https://gitee.com/harmonyos-tpc/SwipeCardView) - 自定义滑动操作卡片
- [ValueCounter](https://gitee.com/harmonyos-tpc/ValueCounter) - 自定义组件计数器
- [MyLittleCanvas](https://gitee.com/harmonyos-tpc/MyLittleCanvas) - 辅助作画工具集合，并且已经预设多种自定义控件
- [DragScaleCircleView](https://gitee.com/harmonyos-tpc/DragScaleCircleView) - 剪裁圆形图片的控件，支持多种自定义样式属性设置
- [CircularFillableLoaders](https://gitee.com/harmonyos-tpc/CircularFillableLoaders) - 水波纹浸漫式LoadingView
- [SpinMenu](https://gitee.com/harmonyos-tpc/SpinMenu) - 轮盘式菜单选择控件
- [BubbleLayout](https://gitee.com/harmonyos-tpc/BubbleLayout) - 自定义气泡组件
- [ohos-slidr](https://gitee.com/harmonyos-tpc/ohos-slidr) - 自定义滑动条
- [ohos-SwitchView](https://gitee.com/harmonyos-tpc/ohos-SwitchView) - 自定义开关按钮
- [material-intro-screen](https://gitee.com/harmonyos-tpc/material-intro-screen) - Material风格的引导页组件库
- [DraggableView](https://gitee.com/harmonyos-tpc/DraggableView) - 拥有3D拖拽功能浏览图片的自定义表格控件。其中拥有2种算法，通过canvas实现3D效果。
- [GridPasswordView](https://gitee.com/chinasoft_ohos/GridPasswordView) - 支付密码视图
- [material-ripple](https://gitee.com/chinasoft_ohos/material-ripple) - 为组件添加点击水波纹效果，水波纹效果已经全部实现
- [vehicle-keyboard-ohos](https://gitee.com/chinasoft_ohos/vehicle-keyboard-ohos) - 快速输入车牌号
- [GuideView](https://gitee.com/chinasoft_ohos/GuideView) - 可添加局部高亮和动画效果的遮罩式导航页
- [RWidgetHelper](https://gitee.com/chinasoft_ohos/RWidgetHelper) - 实现多种UI:圆角、边框、渐变、图形的角度、背景色，字体颜色、渐变、水波纹、阴影、自定义类型的单选和多选
- [ohos-otpview-pinview](https://gitee.com/chinasoft_ohos/ohos-otpview-pinview) - 用于在身份验证时输入验证码视图
- [WidgetCase](https://gitee.com/chinasoft2_ohos/WidgetCase) - WidgetCase是一个自定义控件库
- [WaveLoadingView](https://gitee.com/chinasoft_ohos/WaveLoadingView) - 一个提供实时波纹加载特效的控件
- [Doodle](https://gitee.com/chinasoft_ohos/Doodle) - 图片涂鸦，具有撤消、缩放、移动、添加文字，贴图等功能
- [XUI](https://gitee.com/chinasoft2_ohos/XUI) - 一个简洁而又优雅的ohos原生UI框架，解放你的双手！
- [ScrollNumber](https://gitee.com/chinasoft2_ohos/ScrollNumber) - 一个 简单、优雅、易用 的滚动数字控件
- [WheelPicker](https://gitee.com/chinasoft_ohos/WheelPicker) - 自定义滚轮选择器
- [ohos-expression](https://gitee.com/chinasoft_ohos/ohos-expression) - 自定义表情包的库
- [StateView](https://gitee.com/chinasoft_ohos/StateView) - 状态视图
- [ShadowDrawable](https://gitee.com/chinasoft_ohos/ShadowDrawable) - 带阴影效果的组件库
- [labelview](https://gitee.com/chinasoft_ohos/labelview) - 在按钮 文字 图片上添加角标
- [MaterialSearchBar](https://gitee.com/chinasoft_ohos/MaterialSearchBar) - 实现搜索和侧滑
- [MaterialStepperView](https://gitee.com/chinasoft_ohos/MaterialStepperView) - 竖直样式的 Stepper 组件,未来将会加入更多的样式。你可以自定义正常/激活的圆点颜色、完成图标、动画时长、是否启用动画、线条颜色以及错误高亮颜色之类的参数
- [SuperNova-Emoji](https://gitee.com/chinasoft_ohos/SuperNova-Emoji) - SuperNova-Emoji是一个用于实现和渲染表情符号的库
- [ikvStockChart](https://gitee.com/chinasoft2_ohos/ikvStockChart) - ikvStockChart一个简单的openharmony图表库，支持时间线，k线，macd，kdj，rsi，boll索引和交互式手势操作，包括左右滑动刷新，缩放，突出显示
- [Genius-ohos](https://gitee.com/chinasoft2_ohos/Genius-ohos) - 是 Material Design 控件和一些常用类库组合而成
- [material-code-input](https://gitee.com/chinasoft2_ohos/material-code-input) - Material样式的输入框
- [OhosTreeView](https://gitee.com/chinasoft2_ohos/ohos-tree-view) - 实现可以展开/折叠的树型菜单
- [UIWidget](https://gitee.com/chinasoft2_ohos/UIWidget) - 一个集成UIAlertDialog、UIActionSheetDialog、UIProgressDialog、RadiusView、TitleBarView、 CollapsingTitleBarLayout、StatusViewHelper、NavigationViewHelper 等项目常用UI库
- [mua](https://gitee.com/chinasoft2_ohos/mua) - 支持多语言 支持GFM Markdown 语法说明 工具栏，用于插入Markdown代码、图片、加粗、斜体等等 菜单操作，用于保存、重命名、删除等 文件搜索 MIT协议
- [Codeview](https://gitee.com/chinasoft2_ohos/Codeview) - 代码块高亮显示
- [Conductor](https://gitee.com/chinasoft_ohos/Conductor) - 基于component (而非Fraction) 的HAP框架
- [SimpleSearchView](https://gitee.com/chinasoft_ohos/SimpleSearchView) - 一款简单的基于鸿蒙风格的搜索控件
- [datetimepicker](https://gitee.com/chinasoft_ohos/datetimepicker) - 漂亮的时间和日期选择器控件
- [ShapeOfView](https://gitee.com/chinasoft_ohos/ShapeOfView) - 可将子控件设为多种形状的库
- [SingleDateAndTimePicker](https://gitee.com/chinasoft_ohos/SingleDateAndTimePicker) - 一个可以同时选择日期与时间的控件
- [material-sheet-fab](https://gitee.com/chinasoft2_ohos/material-sheet-fab) - 实现浮动操作按钮到工作表的转换
- [MaterialShadows](https://gitee.com/chinasoft3_ohos/MaterialShadows) - 实现阴影效果的组件
- [CountryCodePickerProject](https://gitee.com/chinasoft3_ohos/CountryCodePickerProject) - 国家城市编码选择器
- [Alligator](https://gitee.com/chinasoft3_ohos/Alligator) - 通过注解处理器实现一套绑定ability和fraction页面切换的三方库
- [Ohos-Week-View](https://gitee.com/chinasoft3_ohos/Ohos-Week-View) - 用于在应用程序中显示日历（周视图或日视图），它支持自定义样式
- [Virtualview-ohos](https://gitee.com/chinasoft2_ohos/virtualview-ohos) - 通过自定义的XML文件及对应的页面展示控件,来组成一套区别于原生系统的控件展示方式
- [CookieBar2](https://gitee.com/chinasoft2_ohos/CookieBar2) - CookieBar2是一个底部和顶部可弹出Bar的控件，且可以自动弹回或者侧滑删除
- [CalendarExaple](https://gitee.com/chinasoft2_ohos/CalendarExaple) - 高仿钉钉和小米的日历控件，支持快速滑动，界面缓存
- [BGATransformersTip-ohos](https://gitee.com/chinasoft2_ohos/BGATransformersTip-ohos) - 实现浮窗展示在锚点控件的任意位置，支持配置浮窗背景色，支持配置指示箭头（是否展示、展示在浮窗的任意位置、高度、圆角、颜色）
- [blurkit-ohos](https://gitee.com/chinasoft2_ohos/blurkit-ohos) - BlurKit是一个非常易于使用和高性能的实用程序，可渲染实时模糊效果
- [PinView](https://gitee.com/chinasoft3_ohos/PinView) - 输入框的背景颜色的动态变化，基线的显示与隐藏，明文密文的切换
- [tooltips](https://gitee.com/chinasoft2_ohos/tooltips) - 易于使用的ohos库，可轻松在任何视图附近添加工具提示
- [JustWeTools](https://gitee.com/hihopeorg/JustWeTools) - JustWeTools是一个方便使用的工具集，集合了众多工具类和自定义组件
- [HoloGraphLibrary](https://gitee.com/hihopeorg/holographlibrary) - 一款集成了绘制现状图、柱状图、饼状图的工具
- [ColorPickerView](https://gitee.com/hihopeorg/color-picker-view) - 颜色选择器
- [arcView](https://gitee.com/hihopeorg/arcView) - 提供一套自定义搜索框控件
- [Simple-Calendar](https://gitee.com/hihopeorg/Simple-Calendar) - 提供事件设置，日历显示
- [saripaar](https://gitee.com/archermind-ti/saripaar) - Saripaar 是一个简单、功能丰富且功能强大的基于规则的 openharmony UI 表单验证库
- [material-icon-lib](https://gitee.com/archermind-ti/material-icon-lib) - 一个包含 2000 多个材料矢量图标的库，可轻松用作 PixelMap和独立控件
- [CosmoCalendar](https://gitee.com/archermind-ti/CosmoCalendar) - 高度自定义的日历库，UI精美，支持多种模式
- [RemoteControlView](https://gitee.com/archermind-ti/remote-control-view) - 万能遥控器
- [cache-web-view](https://gitee.com/openneusoft/CacheWebView) - 定制实现WebView缓存，离线网站，让cache配置更加简单灵活
- [Barber](https://gitee.com/baijuncheng-open-source/barber) - 一个自定义视图样式库。提供了一个简单的基于自定义注释@StyledAttr的style接口来定义视图样式
- [ShadowLayout](https://gitee.com/baijuncheng-open-source/ShadowLayout) - 绘制阴影的库
- [PatternLock](https://gitee.com/baijuncheng-open-source/PatternLock) - 一个实现 Material Design 模式的图案锁库。
- [StatusStories](https://gitee.com/baijuncheng-open-source/statusstories) - 一个高度可定制化的故事视图
- [CurveGraphView](https://gitee.com/baijuncheng-open-source/curve-graph-view) - CurveGraphView组件为图形视图，是一种高度可定制和高性能的自定义视图，用于渲染曲线图
- [CountryPicker](https://gitee.com/baijuncheng-open-source/country-picker) - 国家/地区选择器
- [PinView](https://gitee.com/baijuncheng-open-source/pin-view) - PIN 码专用输入控件，支持任意长度和输入任意数据
- [Carbon](https://gitee.com/ts_ohos/Carbon) - 一个适用于鸿蒙的自定义组件框架，帮助快速实现各种需要的效果
- [ohos-AdvancedWebView](https://gitee.com/hihopeorg/ohos-AdvancedWebView) - 高级的webview
- [ohos-ui](https://gitee.com/harmonyos-tpc/ohos-ui) - ui库
- [FogView_Library](https://gitee.com/harmonyos-tpc/FogView_Library) - 雾化视图
- [ohosWheelView](https://gitee.com/harmonyos-tpc/ohosWheelView) - 滚轮视图
- [ohos-PickerView](https://gitee.com/harmonyos-tpc/ohos-PickerView) - 自定义滚轮控件，支持时间日期城市等选择
- [Isometric](https://gitee.com/harmonyos-tpc/Isometric) - 支持多种形状的仿3D作画控件
- [actual-number-picker](https://gitee.com/harmonyos-tpc/actual-number-picker) - 自定义数字选择器控件
- [PanningView](https://gitee.com/harmonyos-tpc/PanningView) - 可以设置自定义横向或者纵向动画的控件
- [UltimateBreadcrumbsView](https://gitee.com/harmonyos-tpc/UltimateBreadcrumbsView) - 一种定制的视图，具有许多附加的灵活功能，可让您控制内容路径，以便在应用程序中轻松导航
- [SquareMenu](https://gitee.com/harmonyos-tpc/SquareMenu) - 悬浮式矩形菜单
- [AnimateCheckBox](https://gitee.com/harmonyos-tpc/AnimateCheckBox) - 带有动画的多选项选择。
- [ohos-DecoView-charting](https://gitee.com/harmonyos-tpc/ohos-DecoView-charting) - 圆形柱状图。
- [FlightSeat](https://gitee.com/harmonyos-tpc/FlightSeat) - 类似于飞机选择作为的自定义控件。
- [DashboardView](https://gitee.com/harmonyos-tpc/DashboardView) - 速度仪表控件。
- [circleTimer](https://gitee.com/harmonyos-tpc/circleTimer) - 圆形时间秒表。
- [MaterialSpinner](https://gitee.com/harmonyos-tpc/MaterialSpinner) - 下拉式菜单。
- [voice-recording-visualizer](https://gitee.com/harmonyos-tpc/voice-recording-visualizer) - 声音采集频率显示控件
- [snake](https://gitee.com/harmonyos-tpc/snake) - 曲线走势图。
- [trianglify](https://gitee.com/harmonyos-tpc/trianglify) - 生产三角形状玻璃图控件。
- [DropDownView](https://gitee.com/ts_ohos/DropDownView) - 下拉式列表菜单
- [SmallBang](https://gitee.com/ts_ohos/SmallBang) - 爱心动画控件
- [GADownloading](https://gitee.com/ts_ohos/GADownloading) - 一个绚丽的加载布局。
- [ohos-pickers](https://gitee.com/ts_ohos/ohos-pickers) - 自定义转轮选择器
- [Ohos-Charts](https://gitee.com/ts_ohos/Ohos-Charts) - 各种样式的图表控件
- [TriangleRectangleLabelView](https://gitee.com/ts_ohos/triangle-rectangle-label-view) - 自定义角标控件
- [CreditSesameRingView](https://gitee.com/ts_ohos/credit-sesame-ring-view) - 仿芝麻信用分UI刻度和评价
- [UIUtil](https://gitee.com/ts_ohos/UIUtil) - UI工具集合
- [GraphView](https://gitee.com/hihopeorg/GraphView) - 各种图表控件集合
- [KalmanRx](https://gitee.com/hihopeorg/KalmanRx) - 利用卡尔曼滤波算法对数据流进行平滑化处理，适用于过滤各种传感器输入中的噪波
- [ColorPickerPreference](https://gitee.com/hihopeorg/ohos-colorpickerpreference) - 颜色选择器
- [ohos-circle-menu](https://gitee.com/hihopeorg/ohos-circle-menu) - 支持自定义图片和背景
- [CarouselView](https://gitee.com/hihopeorg/CarouselView) - 3D旋转木马效果选项
- [bubbles-for-ohos](https://gitee.com/hihopeorg/bubbles-for-ohos) - 一个可拖动的气泡弹框显示在系统窗口
- [ohosCharts](https://gitee.com/hihopeorg/ohos-Charts) - 带有动画的易于使用的ohos图表库
- [CouponView](https://gitee.com/hihopeorg/CouponView) - 优惠券效果控件
- [Animated-Icons](https://gitee.com/hihopeorg/Animated-Icons) - 动画图标控件
- [FloatingActionMenu-Animation](https://gitee.com/archermind-ti/floatingactionmenu-animation) - 具有滚动处理程序和动画的折叠浮动操作按钮
- [SpeedView](https://gitee.com/archermind-ti/SpeedView) - 动态车速表、仪表
- [PatternLock](https://gitee.com/archermind-ti/pattern-lock) - 自定义屏幕图案手势解锁组件
- [fab](https://gitee.com/archermind-ti/fab) - 具有与变形、启动和转移锚点相关的特殊运动行为浮动操作按钮
- [ShowCaseView](https://gitee.com/archermind-ti/showcaseview) - 一个轻量级向导/引导式遮罩
- [DatePicker](https://gitee.com/archermind-ti/DatePicker) - 日期选择器
- [tutorial-view](https://gitee.com/archermind-ti/tutorial-view) - 授权引导组件
- [Flowing-Gradient](https://gitee.com/archermind-ti/flowing-gradient_ohos) - 可以给图片和颜色背景设置平滑切换效果的自定义组件
- [SlidingCard](https://gitee.com/archermind-ti/sliding-card) - 具有漂亮画廊效果的滑动卡片
- [SlidingTutorial-OHOS](https://gitee.com/archermind-ti/sliding-tutorial-ohos) - 带视差滚动动画引导页组件
- [StackCardsView](https://gitee.com/archermind-ti/stackcardsview) - 堆叠滑动控件，类似于社交软件探探的效果，支持滑动方向控制、消失方向控制，支持嵌入到PageSlider中和内嵌ListContainer等滑动组件
- [LyricViewDemo](https://gitee.com/archermind-ti/lyricviewdemo) - 歌词显示组件
- [BreadcrumbsView](https://gitee.com/archermind-ti/breadcrumbsview) - Material Design 设计面包屑 (Breadcrumbs) 导航组件
- [SmartChart](https://gitee.com/archermind-ti/smartchart) - 一个图表框架，支持线性图（折线，曲线，散点）柱状图、面积图、饼图、3D柱状图、雷达图、风向玫瑰图,支持图表多样化配置。支持轴方向，双轴，图示，水平线，十字轴，MarkView自定义，空白，标题，网格等，支持丰富的样式，包括字体样式（字体大小，颜色），图形样式（正方形，长方形，圆形），线（大小，颜色，DashPathEffect）,增加了图表移动和缩放功能以及动画
- [FloatingKeyboard](https://gitee.com/archermind-ti/floatingkeyboard) - 悬浮的可移动的键盘，可以绑定TextField
- [TokenAutoComplete](https://gitee.com/archermind-ti/tokenautocomplete) - Gmail风格的令牌自动完成文本字段和过滤器
- [DragPointView](https://gitee.com/archermind-ti/dragpointview) - 可以快速的实现拖放未读信息， 还可以自定义效果的自定义组件
- [Muti-Barrage](https://gitee.com/archermind-ti/muti-barrage) - 一个轻量级、自定义多视图、触摸事件处理的弹幕库
- [ohos-loading-dialog](https://gitee.com/archermind-ti/ohos-loading-dialog) - 具有多种样式的且可自定义的Loading弹窗
- [Autocomplete](https://gitee.com/archermind-ti/autocomplete) - 给TextField增加自动提示的组件
- [BubbleActions](https://gitee.com/archermind-ti/bubbleactions) - 长按弹出菜单，并通过手指滑动高亮选择目标以及手指抬起执行回调函数的组件
- [PersistentSearchView](https://gitee.com/archermind-ti/persistentsearchview) - 搜索框自定义组件
- [FadingActionBar](https://gitee.com/archermind-ti/fadingactionbar) - 可以随着上滑而逐渐隐藏的抬头栏
- [reflow-animator](https://gitee.com/archermind-ti/reflow-animator) - 用于在同级Text之间轻松转换的库
- [XDanmuku](https://gitee.com/archermind-ti/reflow-animator) - 支持多种弹幕样式的弹幕视图组件
- [RangeSliderView](https://gitee.com/archermind-ti/range-slider-view) - 具有节点的滑块组件
- [PoppyView](https://gitee.com/archermind-ti/poppyview) - 一个位于ListContainer或者ScrollView的头部或者底部，可滑动隐藏的组件
- [ProperRatingBar](https://gitee.com/archermind-ti/properratingbar) - 简单的打分控件
- [CircleLoadingView](https://gitee.com/archermind-ti/circleloadingview) - 类似ios更新桌面应用效果的loading组件
- [AnimCheckBox](https://gitee.com/archermind-ti/animcheckbox) - 具有动画效果的复选框组件
- [PopupBubble](https://gitee.com/archermind-ti/popupbubble) - 配合ListContainer使用，弹出显示在ListContainer上；点击组件跳转置顶位置；非置顶位置向下滑动显示，向上滑动隐藏
- [CacheWebView](https://gitee.com/openneusoft/CacheWebView) - 具有缓存效果的WebView
- [KChartView](https://gitee.com/hihopeorg/KChartView) - 股票K线图组件
- [Vertical-Intro](https://gitee.com/chinasoft4_ohos/Vertical-Intro) - 应用引导页组件
- [MaterialShowcaseView](https://gitee.com/chinasoft4_ohos/MaterialShowcaseView) - 引导高亮显示遮罩层组件
- [Music-Cover-View](https://gitee.com/chinasoft4_ohos/Music-Cover-View) - 音乐播放器封面视图的组件
- [MusicBobber](https://gitee.com/chinasoft5_ohos/MusicBobber) - 音频播放小部件
- [RadarScanView](https://gitee.com/chinasoft5_ohos/RadarScanView) - 雷达扫描自定义组件
- [PinEntryEditText](https://gitee.com/chinasoft5_ohos/PinEntryEditText) - 带掩码及动画效果的输入框
- [ohos-kline](https://gitee.com/chinasoft5_ohos/ohos-kline) - 金融图表库，包括分时图和K线图
- [TSnackbar](https://gitee.com/chinasoft5_ohos/TSnackBar) - 顶部可以隐藏/显示的信息栏
- [ohos-swipe-to-dismiss-undo](https://gitee.com/chinasoft5_ohos/ohos-swipe-to-dismiss-undo) - 在ListContainer中使用，让使其条目可以关闭或撤销
- [MaterialTabs](https://gitee.com/chinasoft5_ohos/MaterialTabs) - 一个易于集成的Ohos选项卡栏
- [virtual-joystick-ohos](https://gitee.com/archermind-ti/virtual-joystick-ohos) - 一个非常简单且随时可用的操纵杆组件
- [ZDepthShadowLayout](https://gitee.com/archermind-ti/zdepthshadow) - 绘制 MaterialDesign 的 z-depth 阴影的自定义布局
- [PullZoomView](https://gitee.com/archermind-ti/pullzoomview) - 类似QQ空间，新浪微博个人主页下拉头部放大的布局效果，支持ListContainer，ScrollView，WebView，以及其他的任意Component和ComponentContaier。支持头部视差动画和阻尼下拉放大
- [ycshopdetaillayout](https://gitee.com/archermind-ti/ycshopdetaillayout) - 模仿淘宝、京东、考拉等商品详情页分页加载的UI效果。可以嵌套ListContainer、WebView、PageSlider、ScrollView等等
- [WheelView](https://gitee.com/archermind-ti/wheelview) - 轮盘选择组件
- [AvatarLabelView](https://gitee.com/archermind-ti/avatarlabelview) - 一个可以配置角标文字的自定义组件
- [HintCase](https://gitee.com/archermind-ti/hintcase) - 可以给你的应用提供蒙层或者弹窗来进行提示的库
- [DiscreteSlider](https://gitee.com/archermind-ti/hintcase) - 具有刻度节点的滑块组件
- [StickyHeaders](https://gitee.com/chinasoft4_ohos/StickyHeaders) - 粘性头部装饰器，支持粘性标题定位
- [floatlabelededittext](https://gitee.com/chinasoft4_ohos/floatlabelededittext) - 当输入文本时，hint提示将显示在TextField上方的自定义TextField组件
- [desCharts](https://gitee.com/chinasoft5_ohos/desCharts) - 一个图表生成类库，支持的包括XY图表、折线/曲线图、柱状图、堆叠柱状图、堆叠折线/曲线图、Styled XY图表
- [StatusLayoutManager](https://gitee.com/chinasoft5_ohos/StatusLayoutManager) - 切换不同的数据状态布局，包含加载中、空数据和出错状态
- [SmartSwipe](https://gitee.com/chinasoft5_ohos/SmartSwipe) - 具有智能侧滑，弹性侧滑、滑动抽屉、下拉刷新、侧滑返回以及侧滑删除功能的组件库
- [LiveGiftLayout](https://gitee.com/chinasoft4_ohos/LiveGiftLayout) - 一个带礼物面板的直播礼物连击效果组件
- [InteractivePlayerView](https://gitee.com/ts_ohos/interactive-player-view) - 一个炫酷的自定义UI播放器界面
- [MusicPlayerView](https://gitee.com/ts_ohos/music-player-view) - 一个炫酷的自定义UI播放器界面
- [EasySegmentedBarView](https://gitee.com/chinasoft4_ohos/EasySegmentedBarView) - 一个简单易用的自定义分段控件，支持xml配置、代码配置、分段规则按均分/比例分、数字分段、文本分段、渐变分段、bar条样式正常/圆形/三角形，segment文字样式、进度设置、进度标记类型设置、分段描述设置、其它更多自定义设置等功能
- [SimpleRatingBar](https://gitee.com/chinasoft5_ohos/SimpleRatingBar) - 最简单的RatingBar库，几步创建属于自己的动画RatingBar
- [MeiWidgetView](https://gitee.com/chinasoft5_ohos/MeiWidgetView) - 一款汇总了郭霖，鸿洋，以及自己平时收集的自定义控件的集合库。主旨帮助大家学习自定义控件中的一些技巧，分析问题解决问题的一种思路。
- [StikkyHeader](https://gitee.com/chinasoft5_ohos/StikkyHeader) - 能够简单实现header和ListContainer或ScrollView进行滑动关联
- [WheelView](https://gitee.com/chinasoft5_ohos/WheelView) - 滚轮控件，基于滑动列表实现，可以自定义样式
- [TextDrawable](https://gitee.com/hihopeorg/TextDrawable) - 支持圆形,矩形,圆角矩形,加边框，字体样式多种组合背景与文字并列展示
- [BackgroundLibrary](https://gitee.com/hihopeorg/BackgroundLibrary) - 用户开发过程中，不需要自定义View，直接添加标签即可自动生成shape和selector效果
- [overscroll-decor](https://gitee.com/hihopeorg/overscroll-decor) - 支持几乎所有可滚动视图。实现过度滚动效果
- [signaturepad](https://gitee.com/hihopeorg/ohos-signaturepad) - 一个用于绘制平滑签名的库
- [SuperTextView](https://gitee.com/hihopeorg/chenBingX_SuperTextView) - SuperTextView 使您免于复杂的渲染逻辑。简单的API方法调用，炫丽的渲染效果立竿见影
- [TableView](https://gitee.com/hihopeorg/TableView) - TableView是一个用于显示数据结构和呈现由行、列和单元格组成的表格数据的库
- [XhsEmoticonsKeyboard](https://gitee.com/hihopeorg/XhsEmoticonsKeyboard) - 开源表情键盘
- [PdfViewPager](https://gitee.com/hihopeorg/PdfViewPager) - pdf 文件的展示，缩放，翻页功能
- [LabelsView](https://gitee.com/hihopeorg/LabelsView) - LabelsView标签列表控件，可以设置标签的选中效果。 可以设置标签的选中类型：不可选中、单选、限数量多选和不限数量多选等， 并支持设置必选项、单行显示、最大显示行数等功能
- [MaterialIntroView](https://gitee.com/hihopeorg/MaterialIntroView) - MaterialIntroView是展示性动画的库，辅助UI完成特定的动画效果
- [SmartTable](https://gitee.com/hihopeorg/smartTable) - 一款丰富的自动生成表格框架
- [SimpleRatingBar](https://gitee.com/hihopeorg/SimpleRatingBar) - 具有简单而强大的 RatingBar功能
- [HoloColorPicker](https://gitee.com/hihopeorg/HoloColorPicker) - 全息主题选色器，可设置颜色饱和度，亮度，不透明度
- [ohosTagView](https://gitee.com/hihopeorg/ohosTagView) - 支持自定义标签样式
- [BubbleView](https://gitee.com/hihopeorg/BubbleView#https://github.com/cpiz/BubbleView) - 带有箭头的控件/容器,可自定义的气泡属性,气泡可以是纯文本视图或布局容器
- [StickyScrollViewItems](https://gitee.com/hihopeorg/StickyScrollViewItems) - 提供一套滚动视图，可将指定Item标记为'sticky'，被标记的item在滚动过程中不会被滚动出父控件范围，而是置顶显示，直到另一个标记为sticky的item将其推出显示范围
- [CodeView](https://gitee.com/hihopeorg/ohos-CodeView) - 提供代码预览功能
- [Snacky](https://gitee.com/hihopeorg/Snacky) - 用于在布局中添加Snackbar,提供一些模板设计，如错误、警告、信息和成功以及一些自定义选项
- [ohos-about-page](https://gitee.com/hihopeorg/ohos-about-page) - 创建AboutPage对象，添加一个about页面，自定义其中的item
- [CircularFloatingActionMenu](https://gitee.com/hihopeorg/CircularFloatingActionMenu) - 可定制的圆形浮动菜单
- [SpiderWebScoreView](https://gitee.com/hihopeorg/spider-web-score-view) - 蛛网评分控件
- [ThumbUp](https://gitee.com/hihopeorg/ThumbUp) - 一个漂亮的点赞控件
- [Blockly-ohos](https://gitee.com/hihopeorg/ohos-blockly) - 为 JavaScript 和其他编程语言构建拖放可视化编辑器
- [velocimeter-view](https://gitee.com/hihopeorg/velocimeter-view) - 速度表视图,支持自定义颜色，背景色，旋转角度，圆环角度等
- [ohosFloatLabel](https://gitee.com/hihopeorg/ohos-FloatLabel) - 浮动标签,支持自定义布局、动画等
- [FlipView](https://gitee.com/hihopeorg/FlipView) - 自定义支持翻转动画的视图控件
- [HorizontalScrollSelectedView](https://gitee.com/hihopeorg/HorizontalScrollSelectedView) - 横向滚动的，可以支持大量文本选择
- [fab-speed-dial](https://gitee.com/hihopeorg/fab-speed-dial) - 提供一套自定义拨号菜单控件，可添加事件处理唤起拨号盘等服务
- [StockChart-MPohosChart](https://gitee.com/hihopeorg/ohos-StockChart-MPChart) - 基于MPLibChart的专业股票图，如分时图和K线图KLine
- [EasyTagDragView](https://gitee.com/hihopeorg/EasyTagDragView) - 标签选择菜单，长按拖动排序，点击增删标签控件
- [ColorPicker](https://gitee.com/hihopeorg/ColorPicker) - 颜色选择器
- [GaugeView](https://gitee.com/hihopeorg/GaugeView) - 实现圆形仪表盘
- [GiftCard](https://gitee.com/hihopeorg/GiftCard) - 可以自定义控件的炫酷卡片控件，支持自定义多种属性
- [TagEditView](https://gitee.com/hihopeorg/TagsEditText) - TagEditView可以在Edit上添加标签，并提供可选的标签列表和对标签样式的设置
- [PatternLockView](https://gitee.com/archermind-ti/PatternLockView) - 一个易于使用，可自定义的，Material Design 风格的 图案锁控件
- [RemoteControlView](https://gitee.com/archermind-ti/remote-control-view) - 本项目模仿乐视遥控App添加万能遥控器的交互效果
- [MaterialRatingBar](https://gitee.com/archermind-ti/material-rating-bar) - 提供星型打分条样式
- [ScratchView](https://gitee.com/archermind-ti/scratchview) - OHOS自定义控件，类似刮刮卡效果
- [edittag](https://gitee.com/archermind-ti/edittag) - 编辑文本一样，用于编辑标签的视图
- [PassWordInput](https://gitee.com/archermind-ti/passwordinput) - 为OHOS开发的一个自定义密码输入与动画效果
- [Forsquare-CollectionPicker](https://gitee.com/archermind-ti/foursquarecollectionpicker) - 标签tag选择
- [VercodeEditText](https://gitee.com/n914354/VercodeEditText) - 一个验证码输入框控件
- [CatLoadingView](https://gitee.com/archermind-ti/cat-loading-view) - 一个小猫形状的自定义动画加载控件
- [LollipopAppCompatSkeleton](https://gitee.com/archermind-ti/lollipopappcompatskeleton) - 一个material设计风格的导航抽屉框架组件
- [HorizontalWheelView](https://gitee.com/archermind-ti/horizontalwheelview) - 用于模拟水平轮状控制器的自定义控件
- [WindRoseDiagramView](https://gitee.com/archermind-ti/windrosediagramview) - 一个可调节的风力玫瑰图
- [pickerview](https://gitee.com/chinasoft5_ohos/pickerview) - pickerview是一个时间与地址的选择器
- [discrollview](https://gitee.com/chinasoft_ohos/discrollview) - 一个自定义scrollView控件
- [proteus](https://gitee.com/chinasoft3_ohos/proteus) - 一个可从后台动态变更页面的组件
- [Contacts](https://gitee.com/chinasoft4_ohos/Contacts) - 一个联系人列表，汉字转拼音的实现
- [ExpandableView](https://gitee.com/chinasoft4_ohos/ExpandableView) - 实现可折叠ListContainer
- [ExpandableHeightListView](https://gitee.com/chinasoft5_ohos/ExpandableHeightListView) - ScrollView嵌套ListContainer不折叠
- [ink-ohos](https://gitee.com/chinasoft5_ohos/ink-ohos) - 是一个获取签名或绘图的功能库
- [SearchableSpinner](https://gitee.com/chinasoft4_ohos/SearchableSpinner) - 实现可搜索微调器
- [DrawingView-Ohos](https://gitee.com/chinasoft5_ohos/DrawingView-Ohos) - DrawingView是一个简单的视图，允许您使用手指在屏幕上绘制，并将图形保存为图像
- [FloatingView](https://gitee.com/chinasoft2_ohos/FloatingView) - 可以让Component在别的Component上执行的漂浮动画
- [ohos-sku](https://gitee.com/chinasoft4_ohos/ohos-sku) - Sku选择器,类似于淘宝，天猫，京东，支持多维属性，购物车动画，支持MVVM架构，可以直接使用
- [recyclerview-expandable](https://gitee.com/chinasoft5_ohos/recyclerview-expandable) - 基于NestedScrollView+DirectionalLayout封装的可扩展的列表
- [MaterialTapTargetPrompt](https://gitee.com/chinasoft4_ohos/MaterialTapTargetPrompt) - 基于Material Design风格涉及的Tap Target组件
- [Emoji](https://gitee.com/chinasoft4_ohos/Emoji) - 为你的应用添加Emoji支持
- [MapView](https://gitee.com/chinasoft4_ohos/MapView) - 可以缩放、旋转、标记、导航的室内地图
- [MatchView](https://gitee.com/chinasoft4_ohos/MatchView) - 是一款由进度条来控制文字的组合和拆分的库
- [lrcview](https://gitee.com/chinasoft4_ohos/lrcview) - 歌词控件，支持上下拖动歌词
- [ContourView](https://gitee.com/chinasoft3_ohos/ContourView) - 自定义 View：用贝塞尔曲线绘制酷炫轮廓背景
- [Magnet](https://gitee.com/chinasoft5_ohos/Magnet) - 一个可拖拽并自动靠边的悬浮窗库
- [FreeDrawView](https://gitee.com/chinasoft5_ohos/FreeDrawView) - 根据手势绘制图案，获取绘制的图片
- [spark](https://gitee.com/chinasoft5_ohos/spark) - spark是一个自定义折线控件库
- [MaterialBanner](https://gitee.com/chinasoft5_ohos/MaterialBanner) - 一个带有icon,message,button的小部件
- [FancyWalkthrough-Ohos](https://gitee.com/chinasoft5_ohos/FancyWalkthrough-Ohos) - 为应用创建酷炫的简介屏幕
- [Ohos-Iconics](https://gitee.com/chinasoft5_ohos/Ohos-Iconics) - 可设置颜色，线框，阴影等定制化功能的矢量字体图标库
- [MaterialDrawer](https://gitee.com/chinasoft4_ohos/MaterialDrawer) - 是一个多功能的侧拉抽屉第三方库
- [CalendarView](https://gitee.com/chinasoft5_ohos/CalendarView) - 自定义各种风格日历
- [CircleView](https://gitee.com/chinasoft4_ohos/CircleView) - 包含标题和副标题的圆形视图
- [RippleDrawable](https://gitee.com/chinasoft4_ohos/RippleDrawable) - 好用的水波纹组件
- [StackLabel](https://gitee.com/chinasoft4_ohos/StackLabel) - 堆叠标签组件
- [VerticalStepperForm](https://gitee.com/chinasoft4_ohos/VerticalStepperForm) - 一个高度可定制的垂直步进表单
- [MultiWaveHeader](https://gitee.com/chinasoft4_ohos/MultiWaveHeader) - 一个可以高度定制每个波形的ohos水波控件
- [ohosplot](https://gitee.com/chinasoft4_ohos/ohosplot) - ohos图表库
- [VectorMaster](https://gitee.com/chinasoft4_ohos/VectorMaster) - 本库引入矢量可绘制的动态控制
- [CircleMenu](https://gitee.com/chinasoft4_ohos/CircleMenu) - 一个精美别致支持定制的圆形菜单
- [faboptions](https://gitee.com/chinasoft4_ohos/faboptions) - 具有可定制选项的多功能 FAB 组件
- [FastScroll](https://gitee.com/chinasoft4_ohos/FastScroll) - FastScroll风格的滚动条和section“气泡”视图
- [FortyTwo](https://gitee.com/chinasoft5_ohos/FortyTwo) - 显示多项选择答案的UI库
- [FabulousFilter](https://gitee.com/chinasoft4_ohos/FabulousFilter) - 一个实现了底部弹窗且可向上滑动全屏的筛选面板
- [SimpleRatingBar](https://gitee.com/chinasoft4_ohos/SimpleRatingBar) - 一个简单的星级评分，您可以更轻松地自定义图像和动画
- [SegmentedControl](https://gitee.com/chinasoft5_ohos/SegmentedControl) - 分段控件，有很多自定义属性
- [ohos-multitoggle](https://gitee.com/chinasoft5_ohos/ohos-multitoggle) - 滚轮控件，基于滑动列表实现，可以自定义样式
- [Shotang-App](https://gitee.com/chinasoft5_ohos/Shotang-App) - 是一个实现ScrollView，左右指示器滑动，卡片背景效果的 Demo库
- [VSpot](https://gitee.com/chinasoft4_ohos/VSpot) - 实现划分ui模块的库
- [Badger](https://gitee.com/chinasoft4_ohos/Badger) - 一个自定义图形显示角标
- [ohos-navigation-bar](https://gitee.com/chinasoft4_ohos/ohos-navigation-bar) - 可定制的导航栏，实现导航栏内容自定义显示
- [CustomWaterView](https://gitee.com/chinasoft4_ohos/CustomWaterView) - 自定义仿支付宝蚂蚁森林水滴控件
- [expanding-collection-ohos](https://gitee.com/chinasoft4_ohos/expanding-collection-ohos) - 一个可以展开查看详情的卡片
- [MaterialRatingBar](https://gitee.com/chinasoft5_ohos/MaterialRatingBar) - 这是一个星星评价功能
- [material-calendarview](https://gitee.com/chinasoft5_ohos/material-calendarview) - 区别于原生库的一款多重效果日历控件
- [MultiStateView](https://github.com/applibgroup/MultiStateView) - HMOS library which provide MultiStateView feature.
- [EasyGuideView](https://github.com/applibgroup/EasyGuideView) - HMOS library which provides guide highlighting tips, simple and easy to use
- [PasswordInputView](https://github.com/applibgroup/PasswordInputView) - HMOS library of custom password (verification code) input box that supports multiple styles.
- [VerificationCodeView](https://github.com/applibgroup/VerificationCodeView) - HMOS library which provides Verification code features.
- :tw-1f195: [UnifiedContactPicker](https://github.com/applibgroup/UnifiedContactPicker) - UnifiedContactPicker is an HOS library which allows you to easily integrate contact picking workflow into your application with minimal effort
- :tw-1f195: [EmojiConverter](https://github.com/applibgroup/EmojiConverter) - This is a HMOS library used to convert text to Emoji's from a textfield or from other sources.
- :tw-1f195: [Cosin](https://github.com/applibgroup/Cosin) - HMOS loading view library


[返回目录](#目录)

### <a name="框架类-JAVA"></a>框架类

#### <a name="框架类JAVA"></a>框架类

- [TheMVP](https://gitee.com/harmonyos-tpc/TheMVP) - mvp框架
- [ohos-ZBLibrary](https://gitee.com/harmonyos-tpc/ohos-ZBLibrary) - MVP框架，同时附有OKhttp，glide，zxing等常用工具
- [AutoDispose](https://gitee.com/hihopeorg/AutoDispose) - 基于RxJava进行自动绑定代码流式处理
- [mosby](https://gitee.com/hihopeorg/mosby) - 开源mvi、mvp模式适配项目
- [Hermes](https://gitee.com/chinasoft_ohos/Hermes) - 一套新颖巧妙易用的openHarmony进程间通信IPC框架
- [MVPArt](https://gitee.com/archermind-ti/mvpart) - 含有网络层的完整框架,将 **Retrofit** 作为网络层并使用 **Dagger2** 管理所有对象,成熟强大适合新建的项目
- [VIABUS-Architecture](https://gitee.com/archermind-ti/viabus-architecture) - ViaBus 是一款响应式架构，借助总线转发数据的请求和响应，实现ui、业务的完全解耦
- [Clean-Contacts](https://gitee.com/archermind-ti/clean-contacts) - Clean Architecture implementation on OpenHarmony
- [grouter](https://gitee.com/archermind-ti/grouter) - harmonyos APP页面及服务组件化框架
- [XUpdate](https://gitee.com/openneusoft/xupdate) - 一个轻量级、高可用性的版本更新框架
- [Component](https://gitee.com/openneusoft/component) - 一个强大、灵活的组件化框架
- [magnet](https://gitee.com/ts_ohos/magnet_hos) - 一个适用于鸿蒙的java注解框架
- [ohos_viewmodel_livedata](https://gitee.com/ethan-osc_admin/viewmodel_for_ohos)-鸿蒙可用的ViewModel和LiveData，移植自androidx.life相关组件
- [koin](https://gitee.com/ts_ohos/koin-ohos) -koin是一个实用型轻量级依赖注入框架
- [JsBridge](https://gitee.com/archermind-ti/jsbridge) - 轻量可扩展 OHOS WebView 和 Javascript 双向交互框架
- [Rosie](https://gitee.com/archermind-ti/rosie) - MVP开发框架
- [OhosMvc](https://gitee.com/chinasoft5_ohos/OhosMvc) - MVC开发框架
- [AdaptiveCards](https://gitee.com/hihopeorg/AdaptiveCards) - A new way for developers to exchange card content in a common and consistent way
- [ChatKit](https://gitee.com/hihopeorg/ChatKit) - 提供一种UI聊天框架。具有多种样式，支持可定制化UI以及数据管理
- [ohos-material-remixer](https://gitee.com/hihopeorg/ohos-material-remixer) - Remixer是一个可以快速迭代UI更改的框架，它允许您调整UI变量，而无需重新构建（甚至重新启动）应用程序。您可以调整数字、颜色、布尔值和字符串。
- [router_ohos](https://gitee.com/archermind-ti/router) - 一个简单的路由框架，包含路由功能。
- [ohosannotations](https://gitee.com/chinasoft5_ohos/ohosannotations) - 一款能快速开发Ohos的注解使用工具
- [RxCache](https://gitee.com/chinasoft4_ohos/RxCache) - 使用注解来为Retrofit配置缓存信息
- [reductor](https://gitee.com/chinasoft2_ohos/reductor) - Reductor可以帮助您使状态突变更易于阅读，编写和推理
- [cheesesquare](https://gitee.com/chinasoft3_ohos/cheesesquare) - cheesesquare是一个支持侧滑，主题背景切换的Material Degisn Demo库
- [Shatter](https://gitee.com/chinasoft4_ohos/Shatter) - 实现划分ui模块的库
- [scene](https://gitee.com/chinasoft4_ohos/scene) - 基于Component的轻量级导航和页面切分组件库
- [Ohos-DragDismissAbility](https://gitee.com/chinasoft4_ohos/Ohos-DragDismissAbility) - 从一个活动退出到另外一个活动，滑动到底部或顶部退出当前页面
- [OpeningHoursFragment](https://gitee.com/chinasoft5_ohos/OpeningHoursFragment) - 这是一个可重复使用的 UI 元素，用于编辑涵盖完整规范的开放时间值，适用于非破坏性编辑
- [RxDiffUtil](https://gitee.com/chinasoft4_ohos/RxDiffUtil) - 围绕DiffUtil库的 RecyclerView窗口小部件的Rx包装器

[返回目录](#目录)

### <a name="动画图形类JAVA"></a>动画图形类

#### <a name="动画JAVA"></a>动画

- [ohosViewAnimations](https://gitee.com/harmonyos-tpc/ohosViewAnimations) - 包含旋转，缩放，平移，透明及其组合的常见动画效果的动画库集合框架
- [lottie-ohos](https://gitee.com/harmonyos-tpc/lottie-ohos) - json格式的动画解析渲染库
- [confetti](https://gitee.com/harmonyos-tpc/confetti) - 模仿雪花飘落的动画
- [RippleEffect](https://gitee.com/harmonyos-tpc/RippleEffect) - 水波纹点击动画
- [MetaballLoading](https://gitee.com/harmonyos-tpc/MetaballLoading) - 一个类似圆球进度动画效果
- [ohos-Spinkit](https://gitee.com/harmonyos-tpc/ohos-Spinkit) - 多种基础动画集合
- [LoadingView](https://gitee.com/harmonyos-tpc/LoadingView) - 21种简单的带有动画效果的加载控件
- [desertplaceholder](https://gitee.com/harmonyos-tpc/desertplaceholder) - 沙漠风格的动画占位页
- [Sequent](https://gitee.com/harmonyos-tpc/Sequent) - 为一个页面中的所有子控件提供动画效果，使页面更生动
- [ohos-Views](https://gitee.com/harmonyos-tpc/ohos-Views) - 包含粒子效果，脉冲button效果，progress效果，底部导航栏等自定义组件的集合
- [BezierMaker](https://gitee.com/harmonyos-tpc/BezierMaker) - 简单的贝赛尔曲线绘制
- [ohos-transition](https://gitee.com/hihopeorg/ohos-transition) - 平移动画库
- [Konfetti](https://gitee.com/hihopeorg/Konfetti) - 纸屑粒子效果动画
- [LoadingDrawable](https://gitee.com/hihopeorg/LoadingDrawable) - 提供16种加载动画， 适用于下拉刷新、图片加载的占位符、以及其他耗时操作场景
- [recyclerview-animators](https://gitee.com/hihopeorg/recyclerview-animators) - 实现Item增加和删除的动画效果
- [ViewAnimator](https://gitee.com/hihopeorg/ViewAnimator) - 多种布局的动画集合
- [Ohos-spruce](https://gitee.com/hihopeorg/ohos-spruce) - 轻量级平移转场动画
- [CanAnimation](https://gitee.com/chinasoft_ohos/CanAnimation) - 使用ohos的属性动画写的一个库，可组建动画队列，可实现同时、顺序、重复播放等
- [EasingInterpolator](https://gitee.com/archermind-ti/EasingInterpolator) - 多种动画插值器轨迹展示
- [ohos-svprogress-hud-master](https://gitee.com/baijuncheng-open-source/ohos-svprogress-hud-master) - 一个精仿ios提示的弹窗提示库，包括加载动画，失败与成功提示等
- [circular-anim](https://gitee.com/baijuncheng-open-source/circular-anim) - 圆形转场动画
- [AnimatorValueLoadingIndicatorView_ohos](https://gitee.com/isrc_ohos/avloading-indicator-view_ohos) - 支持加载动画的开关和隐藏，支持多种加载动画效果
- [AZExplosion](https://gitee.com/isrc_ohos/azexplosion_ohos) - 粒子破碎效果
- [SwipeCaptcha_ohos](https://gitee.com/isrc_ohos/swipe-captcha_ohos) - 滑动验证码
- [DanmakuFlameMaster_ohos](https://gitee.com/isrc_ohos/danmaku-flame-master_ohos) - 弹幕解析绘制
- [Transitions-Everywhere](https://gitee.com/harmonyos-tpc/Transitions-Everywhere) - 转场动画
- [AnimationEasingFunctions](https://gitee.com/harmonyos-tpc/AnimationEasingFunctions) - 多种估值器动画运动轨迹的集合
- [MultiWaveHeader](https://gitee.com/harmonyos-tpc/MultiWaveHeader) - 自定义水波控件
- [ohos-animated-menu-items](https://gitee.com/harmonyos-tpc/ohos-animated-menu-items) - 自定义动画菜单条目小控件
- [OhosCarrouselLayout](https://gitee.com/chinasoft2_ohos/OhosCarrouselLayout) - 旋转木马3D版
- [SimpleFingerGestures_Ohos_Library](https://gitee.com/chinasoft2_ohos/simple-finger-gestures-ohos-library) - 一个可轻松实现简单的1或2个或多个手指手势的openharmony库
- [WaveLineView](https://gitee.com/chinasoft_ohos/WaveLineView) - 一款性能内存友好的录音波浪动画
- [DynamicGrid](https://gitee.com/chinasoft2_ohos/DynamicGrid) - 图标拖拽排序组件
- [OhosLoadingAnimation](https://gitee.com/chinasoft2_ohos/ohos-loading-animation) - 实现多种动画加载效果
- [BGABadgeView-ohos](https://gitee.com/chinasoft2_ohos/BGABadgeView-ohos) - 实现消息徽章拖拽出范围后爆炸效果
- [mkloader](https://gitee.com/chinasoft2_ohos/mkloader) - 多个自定义加载动画组件
- [SpringView](https://gitee.com/hihopeorg/SpringView) - 提供了上下拖拽刷新控件的功能组件，能够自定义下拉\上拉动画效果
- [Swipecards](https://gitee.com/archermind-ti/swipecards) - 类似探探，自定义卡片左右滑动删除
- [MaterialPlayPauseDrawble](https://gitee.com/archermind-ti/materialplaypausedrawable) - 带动画的点击控件，可切换播放暂停状态
- [ENViews](https://gitee.com/harmonyos-tpc/ENViews) - 各种加载动画
- [ohos-player](https://gitee.com/harmonyos-tpc/ohos-player) - ability过场动画
- [ohosFillableLoaders](https://gitee.com/harmonyos-tpc/ohosFillableLoaders) - 描边矢量动画
- [Loading](https://gitee.com/harmonyos-tpc/Loading) - 几个有意思的加载动画
- [DownloadProgressBar](https://gitee.com/harmonyos-tpc/DownloadProgressBar) - 一个简单的下载动画
- [loading-dots](https://gitee.com/harmonyos-tpc/loading-dots) - 跳动的点加载动画
- [LikeAnimation](https://gitee.com/ts_ohos/LikeAnimation) - 点击喜欢的动画
- [Loading](https://gitee.com/ts_ohos/Loading) - 几种样式的加载图
- [DepthLIB](https://gitee.com/ts_ohos/ohos-Depth-LIB) - 设置界面跳转过渡动画
- [Flubber](https://gitee.com/hihopeorg/Flubber) -  该库的灵感来自iOS版的Spring库。它支持Spring中存在的所有动画、曲线和属性
- [Pulsator4Droid](https://gitee.com/hihopeorg/Pulsator4Droid) -  自定义脉冲动画
- [ActSwitchAnimTool](https://gitee.com/hihopeorg/ActSwitchAnimTool) -  圆形动画放大缩小转场动画
- [ShapeRipple](https://gitee.com/hihopeorg/ShapeRipple) - Shape Ripple是一个库，它可以模拟涟漪效果，如动画，随时随地都可以进行很酷的调整。此外，您甚至可以通过画布创建自己的形状渲染器，以创建自定义形状波纹。
- [Revealator](https://gitee.com/hihopeorg/Revealator) - 圈形显示/取消显示视图的控件
- [FancyView](https://gitee.com/hihopeorg/FancyView) - 开屏动画
- [MultiStateAnimation](https://gitee.com/hihopeorg/MultiStateAnimation) - 多状态动画
- [SpinnerLoading](https://gitee.com/hihopeorg/SpinnerLoading) - 加载动画
- [ItemAnimators](https://gitee.com/hihopeorg/ItemAnimators) - 为ListContainer提供了一个巨大的预创建动画的集合
- [skeleton](https://gitee.com/hihopeorg/skeleton) - 适用于所有view自定义加载动画
- [PasswordLoadingView](https://gitee.com/hihopeorg/PasswordLoadingView) - 当完成密码输入时提供一个动画
- [CubeGrid](https://gitee.com/hihopeorg/CubeGrid) - 立方体网格动画
- [CannyViewAnimator](https://gitee.com/hihopeorg/CannyViewAnimator) - 多组件动画切换
- [OrionPreview](https://gitee.com/archermind-ti/orionpreview) - 一个给控件添加平移或缩放视图的简单动画
- [MaterialImageLoading](https://gitee.com/archermind-ti/materialimageloading) - Material风格图片载入动画的实现
- [RichPath](https://gitee.com/archermind-ti/rich-path) - 一个可以通过矢量文件所描述的图像来实现矢量动画的库
- [Animatoo](https://gitee.com/archermind-ti/animatoo) - 一个给Ability提供跳转过渡动画的库
- [SpinKit](https://gitee.com/hihopeorg/ohos-SpinKit) - 用于加载动画，支持多种加载动画效果
- [ExpectAnim](https://gitee.com/hihopeorg/ExpectAnim) - 动画支持aplha，位置，旋转和缩放功能
- [Depth](https://gitee.com/hihopeorg/Depth) - 设置界面跳转过渡动画
- [paper_onboarding_ohos](https://gitee.com/hihopeorg/ohos-paper-onboarding) - Material Design UI滑动
- [BookPage](https://gitee.com/hihopeorg/BookPage) - 自定义翻页效果
- [ColorTrackView](https://gitee.com/hihopeorg/ColorTrackView) - 字体或者图片可以逐渐染色和逐渐褪色的动画效果
- [PaperShredder](https://gitee.com/hihopeorg/PaperShredder) - 碎纸机动画
- [EasingInterpolator](https://gitee.com/archermind-ti/EasingInterpolator) - 三十一个不同的缓动动画插值器，绘制运动图，展示动画运动轨迹
- [SquareLoading](https://gitee.com/archermind-ti/squareloading_ohos) - 方块翻转加载动画
- [Persei_ohos](https://gitee.com/archermind-ti/persei-ohos) - 支持ListContainer和ScrollView的顶部动画菜单
- [CurrencyEditText](https://gitee.com/archermind-ti/currencyedittext) - 封装使用 TextField字段从用户那里收集货币信息。支持所有符合 ISO-3166 的语言环境/货币。
- [BezierLoadingView](https://gitee.com/archermind-ti/bezierloadingview) - 带有贝塞尔曲线和平滑圆周运动的酷加载视图
- [WaveLoading](https://gitee.com/archermind-ti/WaveLoading) - 一个波形加载动画
- [SineView](https://gitee.com/chinasoft4_ohos/SineView) - 绘制正弦动画
- [StickerView](https://gitee.com/chinasoft4_ohos/StickerView) - 一个可以添加贴纸和缩放、拖动、翻转、删除它的视图
- [Swipeable-Cards](https://gitee.com/chinasoft4_ohos/Swipeable-Cards) - 一个可拖动，可左滑右滑删除带有动画效果的卡片组件
- [FloatingActionButtonSpeedDial](https://gitee.com/chinasoft4_ohos/FloatingActionButtonSpeedDial) - ohos库提供了Material Design浮动动作按钮快速拨号的实现
- [empty-view](https://gitee.com/chinasoft3_ohos/empty-view) - 显示屏幕状态的视图，如loadingType、error、empty等
- [Custom-Navigation-Drawer](https://gitee.com/chinasoft5_ohos/Custom-Navigation-Drawer) - 抽屉菜单导航,从一个页面切换到另一个页面时，它具有平滑的放大，缩小动画
- [ExplosionField](https://gitee.com/chinasoft2_ohos/ExplosionField) - 自定义的一个炫酷爆炸性粉尘效果的动画视图
- [progress-ability](https://gitee.com/chinasoft3_ohos/progress-ability) - 内容加载时的进度条，空视图，错误视图
- [VoiceRipple](https://gitee.com/chinasoft4_ohos/VoiceRipple) - 一个超级棒的音波语音录制按钮
- [SlidingRootNav](https://gitee.com/chinasoft3_ohos/SlidingRootNav) - 一种类似于抽屉布局
- [MultipleStatusView](https://gitee.com/chinasoft3_ohos/MultipleStatusView) - 一个支持多种状态的自定义View
- [LDrawer](https://gitee.com/chinasoft4_ohos/LDrawer) - 抽屉图标与材质设计动画
- [loaderviewlibrary](https://gitee.com/chinasoft4_ohos/loaderviewlibrary) - 为 TextView 和 ImageView 提供在显示任何文本或图像之前显示微光（动画加载器）的能力
- [JJSearchViewAnim](https://gitee.com/chinasoft4_ohos/JJSearchViewAnim) - 一个炫酷的SearchView搜索动画库,支持8中不同的动画
- [PlaceHolderView](https://gitee.com/chinasoft4_ohos/PlaceHolderView) - 支持拖拽卡片、侧滑菜单、列表加载更多、横向滚动列表以及展开收起列表布局的自定义控件
- [FabDialogMorph](https://gitee.com/chinasoft4_ohos/FabDialogMorph) - Ohos 上的 Fab 和对话框变形动画
- [ohos-snowfall](https://gitee.com/chinasoft4_ohos/ohos-snowfall) - 雪花飘落的效果
- [confetti](https://gitee.com/chinasoft4_ohos/confetti) - 一个高性能，易于配置粒子系统库，可以通过空间动画任何一组物体
- [ohos_additive_animations](https://gitee.com/chinasoft4_ohos/ohos_additive_animations) - 一种简单的方法来添加任何物体的任何属性动画
- [Backboard](https://gitee.com/chinasoft2_ohos/Backboard) - 一个反弹动画框架
- [SVGAPlayer-Ohos](https://gitee.com/chinasoft5_ohos/SVGAPlayer-Ohos) - 一个轻量的动画渲染库
- [ohos-viewpager-transformers](https://gitee.com/chinasoft4_ohos/ohos-viewpager-transformers) -视图页面切换效果的集合
- [SpeechRecognitionView](https://gitee.com/chinasoft4_ohos/SpeechRecognitionView) -一种动画通过语音来控制变化
- [mapbox-gestures-ohos](https://gitee.com/chinasoft5_ohos/mapbox-gestures-ohos) -引入了缩放、旋转、移动、推和点击手势检测器的实现
- [RoadRunner](https://gitee.com/chinasoft4_ohos/RoadRunner) -主要实现使用SVG图像制作自己的加载动画
- [JustBar](https://github.com/applibgroup/JustBar) -A HMOS library to add a Bottom Navigation Bar.
- [Bounceview-Ohos](https://github.com/applibgroup/Bounceview-Ohos) -Customizable bounce animation for any view.
- [RainyView](https://github.com/applibgroup/RainyView) -HMOS library which provides rainy view animation


[返回目录](#目录)

#### <a name="图片处理JAVA"></a>图片处理

- [SimpleCropView](https://gitee.com/harmonyos-tpc/SimpleCropView) - 适用于ohos的图像裁剪库，简化了裁剪的代码，并提供了易于自定义的UI
- [Luban](https://gitee.com/harmonyos-tpc/Luban) - 图片压缩工具
- [TakePhoto](https://gitee.com/harmonyos-tpc/TakePhoto) - 拍照图片旋转剪裁
- [Compressor](https://gitee.com/harmonyos-tpc/Compressor) - 一个轻量级且功能强大的图像压缩库。通过Compressor，您可以将大照片压缩为较小尺寸的照片，而图像质量的损失则很小或可以忽略不计，不支持WebP
- [PloyFun](https://gitee.com/harmonyos-tpc/PloyFun) - 用来生成三角玻璃图片工具
- [CompressHelper](https://gitee.com/harmonyos-tpc/CompressHelper) - 图片压缩，压缩Pixelmap，主要通过尺寸压缩和质量压缩，以达到清晰度最优
- [SimpleCropView](https://gitee.com/hihopeorg/SimpleCropView) - 图片裁剪工具
- [cropper](https://gitee.com/hihopeorg/cropper) - 图像裁剪工具
- [cropper2](https://gitee.com/harmonyos-tpc/cropper) - 图片裁剪
- [boxing](https://gitee.com/hihopeorg/boxing) - 支持图片旋转裁剪多图选择等功能
- [Ohos-stackblur](https://gitee.com/hihopeorg/ohos-stackblur) - 图片模糊效果
- [ImageCropper_ohos](https://gitee.com/isrc_ohos/image-cropper_ohos) - 图片裁剪
- [uCrop_ohos](https://gitee.com/isrc_ohos/u-crop_ohos) - 图像裁剪
- [ Crop_ohos](https://gitee.com/isrc_ohos/crop_ohos) - 图片裁剪
- [crop_image_layout_ohos](https://gitee.com/isrc_ohos/crop_image_layout_ohos) - 图片裁剪
- [Lichenwei-Dev_ImagePicker](https://gitee.com/harmonyos-tpc/Lichenwei-Dev_ImagePicker) - 图片选择预览加载器
- [wallpaperboard](https://gitee.com/chinasoft2_ohos/wallpaperboard) - 可查看、下载、设置壁纸和锁屏的库
- [Image-Steganography-Library-ohos](https://gitee.com/chinasoft3_ohos/Image-Steganography-Library-ohos) - 使用LSB将加密信息编码嵌入到图片中，实现隐写
- [photo-editor-ohos](https://gitee.com/chinasoft_ohos/photo-editor-ohos) - 易于操作图片文件的oho库
- [Ohos-CutOut](https://gitee.com/chinasoft3_ohos/Ohos-CutOut) - 对图片进行裁剪,旋转,涂鸦,渲染等效果
- [ShadowImageView](https://gitee.com/chinasoft3_ohos/ShadowImageView) - 设置图片、设置图片半径、设置图片阴影颜色、根据图片内容获取阴影颜色
- [touch-gallery](https://gitee.com/archermind-ti/touch-gallery) - 库用于图片浏览, 基于PageSlider，实现图片的切换、缩放、拖拽等
- [SiliCompressor](https://gitee.com/ts_ohos/silicompressor-for-ohos) - 图片压缩
- [ohossvg](https://gitee.com/ts_ohos/ohossvg) - svg图片
- [VectorMaster](https://gitee.com/harmonyos-tpc/VectorMaster) - 矢量图加载
- [ohos-anyshape](https://gitee.com/harmonyos-tpc/ohos-anyshape) - 实现各种滤镜图片
- [BlurredGridMenu](https://gitee.com/ts_ohos/BlurredGridMenu) - 表格菜单，可以使背景模糊化
- [ImageBlurring](https://gitee.com/ts_ohos/ImageBlurring) - 图片模糊处理
- [PhotoEditor](https://gitee.com/hihopeorg/PhotoEditor) - 照片编辑器库，简单的支持图像编辑使用油漆，文本，过滤器，表情符号和贴纸类
- [Resizer](https://gitee.com/hihopeorg/Resizer) - 图片压缩，修改图片尺寸
- [ohos-WM](https://gitee.com/hihopeorg/ohos-WM) - 一个轻量级的图片水印框架，支持隐形数字水印。
- [QuickShot](https://gitee.com/hihopeorg/QuickShot) - 快速截图工具
- [SVG Drawable plugin](https://gitee.com/hihopeorg/ohos-svgdrawable-plugin) - 一套编译期插件，可在编译时将SVG矢量图文件缩放渲染输出为各个分辨率下的图片资源文件
- [scissors](https://gitee.com/hihopeorg/scissors) - 提供图片裁剪
- [RubberStamp](https://gitee.com/archermind-ti/rubberstamp) - 一个轻松地为图像添加水印的库
- [ohos-imagecropview](https://gitee.com/archermind-ti/ohos-imagecropview) - 从具有图库或相机中选择图像进行裁剪保存
- [HokoBlur](https://gitee.com/chinasoft5_ohos/HokoBlur) - 给图片添加模糊效果；动态模糊，对背景的实时模糊
- [ OhosPhotoFilter](https://gitee.com/ts_ohos/ohos-photo-filters) - 一个图片渲染工具
- [CombineBitmap](https://gitee.com/hihopeorg/CombineBitmap) - 图片拼接绘制，图片下载，生成多张图片
- [TileView](https://gitee.com/hihopeorg/TileView) - 异步地显示、平移和缩放基于平铺的图像
- [AdvancedLuban](https://gitee.com/hihopeorg/AdvancedLuban) -  是一个方便简约的图片压缩工具库，提供多种压缩策略（包括Luban原有的压缩策略），多种调用方式，自定义压缩，多图压缩，专注更好的图片压缩使用体验
- [PicassoPalette](https://gitee.com/hihopeorg/PicassoPalette) -  PicassoPalette是一个基于图片的调色板，可以通过图片，获取相应属性的颜色值
- [Ohos-Image-Picker-and-Cropping](https://gitee.com/archermind-ti/Ohos-Image-Picker-and-Cropping) -  从具有裁剪功能的图库或相机中选择图像
- [Ananas](https://gitee.com/chinasoft5_ohos/Ananas) - 一个简单的照片编辑器
- [cv4j](https://gitee.com/chinasoft5_ohos/cv4j) - CV in Java，纯 java 实时图像与处理库
- [GaussianBlur](https://gitee.com/chinasoft4_ohos/GaussianBlur) - 图片的高斯模糊效果展示
- [PhotoView](https://gitee.com/chinasoft5_ohos/PhotoView) - 一个流畅的photoview图片缩放浏览控件
- [view-effects](https://github.com/applibgroup/view-effects) - Apply custom effects on view backgrounds.


[返回目录](#目录)

### <a name="音视频JAVA"></a>音视频

- [jcodec java](https://gitee.com/harmonyos-tpc/jcodec) - 纯java实现的音视频编解码器的库
- [VideoCache_ohos](https://gitee.com/isrc_ohos/video-cache_ohos) - 开源视频缓存项目，支持自动缓存视频并在断网状态下播放视频
- [soundtouch](https://gitee.com/harmonyos-tpc/soundtouch) - 开源音频处理库，可更改音频流或音频文件的速度、音高和播放速率
- [ohosMP3Recorder](https://gitee.com/harmonyos-tpc/ohosMP3Recorder) - 提供MP3录音功能
- [ijkplayer](https://gitee.com/harmonyos-tpc/ijkplayer) - 基于FFmpeg的ohos视频播放器，除了常规的播放器功能外，多用于直播流场景，支持常见的各种流媒体协议和音视频格式
- [YcVideoPlayer](https://gitee.com/chinasoft3_ohos/YcVideoPlayer) - 基础封装视频播放器player，使用简单，代码拓展性强，封装性好，主要是和业务彻底解耦，暴露接口监听给开发者处理业务具体逻辑
- [ohos-AudioRecorder](https://gitee.com/chinasoft3_ohos/ohos-AudioRecorder) - 主要实现录音功能、暂停，播放。根据声音大小振幅有水波纹冒泡效果
- [ChatVoicePlayer](https://gitee.com/chinasoft2_ohos/ChatVoicePlayer) - 简单音乐播放器功能，实现播放、暂停功能
- [speechutils](https://gitee.com/chinasoft2_ohos/speechutils) - 语音转文字，文字转语音库
- [youtube-jextractor](https://gitee.com/chinasoft2_ohos/youtube-jextractor) - 从任何youtube视频中提取视频和音频以及其他一些数据，例如视频标题，说明，作者，缩略图等
- [audio-visualizer-ohos](https://gitee.com/chinasoft3_ohos/audio-visualizer-ohos) - 音频播放及背景联动
- [ohos-audio-visualizer](https://gitee.com/chinasoft3_ohos/ohos-audio-visualizer) - 实现音频可视化
- [auto-play-video](https://gitee.com/archermind-ti/auto-play-video) - 轻松实现带有视频的ListContainer
- [fenster](https://gitee.com/ts_ohos/fenster) - 1.简易视频播放器功能 支持暂停和播放，播放进度显示，快进和快退功能 2.标准播放器功能 支持 暂停播放，播放进度显示，快进和快退，音量调节，亮度调节等功能 3.视频缩放 支持不同size的缩放 4.开发者可以扩展Next和Pre键，实现自己想要的功能
- [VideoPlayerManager](https://gitee.com/ts_ohos/VideoPlayerManager) - openharmony实现的VideoPlayerManager功能
- [RxOhosAudio](https://gitee.com/ts_ohos/RxOhosAudio) - 音频的录制和播放
- [mp4parser](https://gitee.com/ts_ohos/mp4parser_hos) - 用于读取、写入和创建MP4容器的JavaAPI。操纵容器不同于对视频和音频进行编码和解码。openharmony移植组件
- [QSVideoPlayer](https://gitee.com/ts_ohos/qsvideo-player_hos) - 支持设置视频比例，支持两种悬浮窗，支持拓展解码器，支持本地缓存，支持倍速静音等，只需100行不到的java代码即可打造自己的播放器，提供DemoQSVideoView成品播放器,支持手势,清晰度，一句代码集成弹幕。openharmony移植组件
- [lingorecorder](https://gitee.com/ts_ohos/lingorecorder-for-ohos) - 音频处理
- [FFmpeg](https://gitee.com/harmonyos-tpc/FFmpeg) - 音视频库，一套可以用来记录、转换数字音频、视频，并能将其转化为流的开源组件
- [openh264](https://gitee.com/harmonyos-tpc/openh264) - 支持H.264格式编解码
- [ohos-ffmpeg-java](https://gitee.com/harmonyos-tpc/ohos-ffmpeg-java) - java版本的ffmpeg库
- [OmRecorder](https://gitee.com/harmonyos-tpc/OmRecorder) - 音频记录工具
- [IjkPlayerView](https://gitee.com/ts_ohos/IjkPlayerView) - IjkPlayerView是一个基于ijkplayer的视屏播放库，可以用于播放本地和网络视频
- [VideoEnabledWebView](https://gitee.com/ts_ohos/VideoEnabledWebView) - webview扩展，支持 HTML5 视频播放。
- [ohos-speech](https://gitee.com/hihopeorg/ohos-speech) - 将语音识别和文本语音转换变得简单的库。
- [LandscapeVideoCamera](https://gitee.com/hihopeorg/LandscapeVideoCamera) - 视频录制，设置视频分辨率，帧率，播放等。
- [JZVideo](https://gitee.com/hihopeorg/JZVideo) - 高度自定义的视频框架
- [PlayPicdio](https://gitee.com/ts_ohos/play-picdio) - ohos平台下，视频转ascii码视频、图片转ascii码图片、图片转低多边形风格图片、图片emoji-masaic化
- [OmRecorder](https://gitee.com/chinasoft5_ohos/OmRecorder) - 一个简单的Pcm/Wav录音机封装库
- [ audio-wife](https://gitee.com/ts_ohos/audio-wife) - 一个简单的主题和可集成的音频播放器库
- [ohos-ffmpeg-java](https://gitee.com/hihopeorg/ohos-ffmpeg-java) - 简化ffmpeg在手机上的任务处理
- [Voice-Overlay-ohos](https://gitee.com/archermind-ti/voice-overlay-ohos) - Voice overlay 帮助您将用户的语音转化为文本，在为您处理必要权限的同时提供了一个成熟的用户体验。
- [ZlwAudioRecorder](https://gitee.com/chinasoft2_ohos/ZlwAudioRecorder) - 多格式音频录制与可视化
- [UniversalVideoView](https://gitee.com/chinasoft5_ohos/UniversalVideoView) - 一个更好用的ohos VideoView

[返回目录](#目录)

### <a name="游戏JAVA"></a>游戏

- [JustWeEngine](https://gitee.com/openneusoft/just-we-engine) - 原生游戏框架，可以基于这个框架开发一些简单的小游戏，比如打飞机，骨骼精灵打怪等

[返回目录](#目录)


## 学习材料

### 组件介绍

- [鸿蒙开源三方组件-第三方图片加载组件Fresco使用指南](https://os.51cto.com/art/202104/659992.htm)
- [鸿蒙开源三方组件-强大的弹窗库Xpopup组件](https://harmonyos.51cto.com/posts/4198)
- [鸿蒙开源三方组件-跨平台自适应布局yoga组件](https://harmonyos.51cto.com/posts/4153)
- [鸿蒙开源三方组件-MD风格应用引导页组件material-intro-screen](https://harmonyos.51cto.com/posts/4231)
- [HarmonyOS三方开源组件——鸿蒙JS实现仿蚂蚁森林](https://harmonyos.51cto.com/posts/7418)
- [HarmonyOS 鸿蒙开源第三方组件-表情雨EmojiRain](https://harmonyos.51cto.com/posts/7854)
- [HarmonyOS自定义控件之Material风格的下拉刷新](https://harmonyos.51cto.com/posts/7974)
- [HarmonyOS 自定义控件之JS进度条](https://harmonyos.51cto.com/posts/8041)

[返回目录](#目录)

### 技术分享

- [鸿蒙开源三方组件Maven&HAPM发布流程指导](https://harmonyos.51cto.com/posts/6934)
- [HarmonyOS 非UI单元测试在DevEco Studio上的应用](https://harmonyos.51cto.com/posts/7417)
- [HarmonyOS 日程提醒卡片应用](https://harmonyos.51cto.com/posts/7546)
- [HarmonyOS 自定义控件之触摸事件与事件分发](https://harmonyos.51cto.com/posts/7555)
- [HarmonyOS IDL跨进程通信实现](https://harmonyos.51cto.com/posts/7553)
- [HarmonyOS列表组件-ListContainer](https://harmonyos.51cto.com/posts/7552)
- [HarmonyOS自定义控件之测量与布局](https://harmonyos.51cto.com/posts/7686)
- [HarmonyOS 基于Java开发的服务卡片](https://harmonyos.51cto.com/posts/7687)
- [ActiveData在HarmonyOS中的原理分析和运用](https://harmonyos.51cto.com/posts/7557)
- [HarmonyOS 非侵入式事件分发设计](https://harmonyos.51cto.com/posts/7735)
- [HarmonyOS 用 Matrix 实现各种图片 ScaleType 缩放](https://harmonyos.51cto.com/posts/7736)
- [HarmonyOS自定义常用通知栏](https://harmonyos.51cto.com/posts/7730)
- [HarmonyOS自定义控件之速度检测VelocityDetector](https://harmonyos.51cto.com/posts/7829)
- [HarmonyOS 注解的使用](https://harmonyos.51cto.com/posts/7849)
- [HarmonyOS 通用的应用引导功能](https://harmonyos.51cto.com/posts/7850)
- [HarmonyOS 数据库系列之关系型数据库](https://harmonyos.51cto.com/posts/8013)
- [HarmonyOS 数据库系列之对象关系映射数据库](https://harmonyos.51cto.com/posts/8004)
- [HarmonyOS JS FA 调用 JAVA PA 机制](https://harmonyos.51cto.com/posts/8187)
- [HarmonyOS JS卡片之“彩票开奖查询”及避坑指北](https://harmonyos.51cto.com/posts/8204)
- [HarmonyOS 自定义UI之水波纹效果](https://harmonyos.51cto.com/posts/8212)

[返回目录](#目录)

### 教学专栏

- [HarmonyOS 开发者学堂](https://developer.huawei.com/consumer/cn/training/result?type1=101603094347460003)
- [HUAWEI Codelabs](https://developer.huawei.com/consumer/cn/codelabsPortal/index)
- [HarmonyOS 博客](https://developer.huawei.com/consumer/cn/blog/technicalfield/harmony-os)
- [51CTO鸿蒙课堂](https://harmonyos.51cto.com/study)
- [51CTOHarmonyOS专栏](https://harmonyos.51cto.com/column)

[返回目录](#目录) 